$("document").ready(function () {
    var mind = {
        "meta": {
            "name": "jsMind remote",
            "author": "hizzgdev@163.com",
            "version": "0.2"
        },
        "format": "node_array",
        "data": $data
    };
    var options = {
        container: 'jsmind_container',
        editable: true,
        theme: 'orange'
    };
    var jm = new jsMind(options);
    // show it
    jm.show(mind);


    $("button.btn-delete").click(function () {
        var name = $(this).attr("data-name");
        $("#confirm-name").html(name);
    });
    $('#confirm-delete').on('show.bs.modal', function (e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });
});