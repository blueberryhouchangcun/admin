<?php
$scm_config = require(__DIR__ . '/scm_config.php');

return [
	'admin.email' => 'manjinxin@baidu.com',
	'title' => 'What2eat 管理后台',
	'sign.in.cookie.name' => '7c92461b16d2d6938c7f6e6abb0f6261',
	'sign.in.name' => $scm_config['sign.in.name'],
	'sign.in.password' => $scm_config['sign.in.password'],
	'color.level' => [
		0 => 'primary',
		1 => 'default',
		2 => 'secondary',
		3 => 'success',
		4 => 'warning',
		5 => 'danger',
		6 => 'info',
	]
];
