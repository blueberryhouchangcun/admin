<?php
include(__DIR__ . '/scm_config.php');
$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');
$config = [
	'id' => 'basic',
	'basePath' => dirname(__DIR__),
	'bootstrap' => ['log'],
	'components' => [
		'errorHandler' => [
			'errorAction' => 'admin/error',
		],
		'view' => [
			'theme' => [
				'pathMap' => [
					'@dektrium/user/views' => '@app/views/user'
				],
			],
		],
		'authManager' => [
			'class' => 'dektrium\rbac\components\DbManager',
		],
		'assetManager' => [
			'bundles' => [
//		        'yii\web\JqueryAsset' => [
//			        'js'=>[]
//		        ],
//		        'yii\bootstrap\BootstrapPluginAsset' => [
//			        'js'=>[]
//		        ],
//		        'yii\bootstrap\BootstrapAsset' => [
//			        'css' => [],
//		        ],
			],
		],
		'request' => [
			// !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
			'cookieValidationKey' => '87gjkVeK9Y1oRlIoV9kt76XQo_qw488J',
			'enableCsrfValidation' => false,
		],
		'cache' => [
			'class' => 'yii\caching\FileCache',
		],
		'user' => [
			'identityClass' => 'dektrium\user\models\User',
			'enableAutoLogin' => true,
		],
//		'user' => [
//			'class' => 'dektrium\user\Module',
//			'enableUnconfirmedLogin' => true,
//			'confirmWithin' => 21600,
//			'cost' => 12,
//			'admins' => ['admin']
//		],
		'mailer' => [
			'class' => 'yii\swiftmailer\Mailer',
			// send all mails to a file by default. You have to set
			// 'useFileTransport' to false and configure a transport
			// for the mailer to send real emails.
			'useFileTransport' => true,
		],
		'log' => [
			'traceLevel' => YII_DEBUG ? 3 : 0,
			'targets' => [
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['error', 'warning'],
				],
			],
		],
		'what2eat' => $db['what2eat'],
		'db' => $db['what2eat'],
		'account' => $db['account'],
		'scrapy' => $db['scrapy'],
		'urlManager' => [
			'enablePrettyUrl' => true,
			'showScriptName' => false,
			'rules' => [
			],
		],
	],
	'params' => $params,
	'on beforeRequest' => function () {
		Yii::$app->setTimeZone('Asia/Shanghai');
	},
	'modules' => [
		'user' => [
			'class' => 'dektrium\user\Module',
			'admins' => ['manjinxin'],
			'controllerMap' => [
				'security' => [
					'class' => 'dektrium\user\controllers\SecurityController',
					'layout' => '//auth',
				],
			],
		],
		'admin' => [
			'class' => 'app\modules\admin\Module',
		],
		'api' => [
			'class' => 'app\modules\api\Module',
		],
		'rbac' => [
			'class' => 'dektrium\rbac\RbacWebModule'
		]
	],
];

if (YII_ENV_DEV) {
	// configuration adjustments for 'dev' environment
	$config['bootstrap'][] = 'debug';
	$config['modules']['debug'] = [
		'class' => 'yii\debug\Module',
		// uncomment the following to add your IP if you are not connecting from localhost.
		//'allowedIPs' => ['127.0.0.1', '::1'],
	];

	$config['bootstrap'][] = 'gii';
	$config['modules']['gii'] = [
		'class' => 'yii\gii\Module',
		// uncomment the following to add your IP if you are not connecting from localhost.
		//'allowedIPs' => ['127.0.0.1', '::1'],
	];
}

return $config;
