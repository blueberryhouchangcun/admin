<?php
$scm_config = require(__DIR__ . '/scm_config.php');

return [
	'what2eat' => [
		'class' => 'yii\db\Connection',
		'dsn' => 'mysql:host=' . $scm_config['db.mysql.host']
			. ';port=' . $scm_config['db.mysql.port']
			. ';dbname=' . $scm_config['db.mysql.dbname.what2eat'],
		'username' => $scm_config['db.mysql.username'],
		'password' => $scm_config['db.mysql.password'],
		'charset' => 'utf8'
	],
	'account' => [
		'class' => 'yii\db\Connection',
		'dsn' => 'mysql:host=' . $scm_config['db.mysql.host']
			. ';port=' . $scm_config['db.mysql.port']
			. ';dbname=' . $scm_config['db.mysql.dbname.account'],
		'username' => $scm_config['db.mysql.username'],
		'password' => $scm_config['db.mysql.password'],
		'charset' => 'utf8'
	],
	'scrapy' => [
		'class' => 'yii\db\Connection',
		'dsn' => 'mysql:host=' . $scm_config['db.mysql.host']
			. ';port=' . $scm_config['db.mysql.port']
			. ';dbname=' . $scm_config['db.mysql.dbname.scrapy'],
		'username' => $scm_config['db.mysql.username'],
		'password' => $scm_config['db.mysql.password'],
		'charset' => 'utf8'
	]
];
