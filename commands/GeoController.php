<?php

namespace app\commands;

use app\models\what2eat\ForHereShop;
use app\services\Curl;
use app\services\GeoHash;
use yii\console\Controller;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 */
class GeoController extends Controller
{
	const APP_CODE = '046e32c399e54517bdc69d5d15039943';


	public function actionAddressToGeoHash()
	{
		$shops = ForHereShop::find()->all();
		$url = 'http://geo.market.alicloudapi.com/v3/geocode/geo';
		$headers = [
			"Authorization: APPCODE " . self::APP_CODE
		];
		foreach ($shops as $index => $shop) {
			if ($shop instanceof ForHereShop) {
				$address = ($shop->getDistrict() . $shop->getAddress());
				$city = $shop->getCity();
				$params = [
					'address' => $address,
					'city' => $city,
					'output' => "JSON",
				];
				$result = Curl::get($url, 80, $params, $headers);
				if ($result['status'] == 1) {
					$location = $result['geocodes'][0]['location'];
					$location = explode(",", $location);
					$shop->setGeoLongitude($location[0]);
					$shop->setGeoLatitude($location[1]);
					$geo_hash = GeoHash::encode($location[0], $location[1], 0.000001);
					$shop->setGeoHash($geo_hash);
					$shop->setGeoType($result['geocodes'][0]['level']);
					$shop->setGeoAdcode($result['geocodes'][0]['adcode']);
					$street = $result['geocodes'][0]['street'];
					$street = (is_array($street)) ? implode("#", $street) : $street;
					$shop->setStreet($street);
					$shop->setDistrict($result['geocodes'][0]['district']);
					$shop->setFormattedAddress($result['geocodes'][0]['formatted_address']);
					$shop->save(false);
					echo $index . "#" . $shop->getName() . "#" . $shop->getGeoLatitude() . " DONE\n";
				}
			}
		}
	}
}
