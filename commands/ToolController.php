<?php

namespace app\commands;

use app\models\Label;
use app\models\SubLabels;
use app\models\what2eat\ForHereShop;
use app\services\Curl;
use app\services\GeoHash;
use yii\console\Controller;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 */
class ToolController extends Controller
{
	public function actionTest()
	{
		$test = explode("#","dd#ee");
		var_dump($test);
	}

	public function actionReplaceCategory()
	{
		$shops = ForHereShop::find()->where(['status' => ForHereShop::STATUS_MANUAL_EDITED, 'region' => '清河'])->all();
		foreach ($shops as $index => $shop) {
			if ($shop instanceof ForHereShop) {
				$labelsArray = explode("#", $shop->getLabels());
				if (!empty($labelsArray)) {
					$category = [];
					foreach ($labelsArray as $labelId) {
						if (in_array($labelId, array_keys(SubLabels::$combination_attribution))) {
							$category[] = $labelId;
						}
					}
					$shop->setCategory(implode("#", $category));
					$shop->save(false);
					echo $index."\n";
				}
			}
		}
	}
}
