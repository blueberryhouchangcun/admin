<?php

namespace app\commands;

use app\models\scrapy\ForHereCourse;
use app\models\scrapy\ForHereShop;
use JonnyW\PhantomJs\Http\RequestInterface;
use QL\Ext\PhantomJs;
use QL\QueryList;
use Ramsey\Uuid\Uuid;
use yii\console\Controller;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 */
class DianpingController extends Controller
{
	public $offset;
	public $limit = 1000;

	public function options($actionID)
	{
		return ['limit', 'offset'];
	}

	public function optionAliases()
	{
		return ['l' => 'limit', 'o' => 'offset'];
	}

	public function actionShop()
	{
		$categories = QueryList::get('http://www.dianping.com/beijing/ch10')
			// 设置采集规则
			->rules([
				'link' => array('#classfy > a', 'href'),
				'name' => array('#classfy > a span', 'text'),
			])->query()->getData();
		$links = $categories->all();
		foreach ($links as $index => $link) {
			echo $index . "#" . $link['link'] . "\n";
			echo $index . "#" . $link['name'] . "\n";
			for ($i = 1; $i <= 50; $i++) {
				$url = $link['link'] . 'p' . $i;
				echo $url . "\n";
				$data = QueryList::get($url)
					// 设置采集规则
					->rules([
						'name' => array('div.tit h4', 'text'),
						'link' => array('div.tit > a:nth-child(1)', 'href'),
						'category' => array('div.tag-addr > a:nth-child(1) > span.tag', 'text'),
						'region' => array('div.tag-addr > a:nth-child(3) > span.tag', 'text'),
						'address' => array('div.tag-addr > span.addr', 'text'),
					])->query()->getData();
				foreach ($data->all() as $item) {
					if ($item['name']) {
						$shop = new ForHereShop();
						$shop->setId(Uuid::uuid4());
						$shop->setName($item['name']);
						$shop->setLink($item['link']);
						$shop->setCategory($item['category']);
						$shop->setRegion($item['region']);
						$shop->setAddress($item['address']);
						$shop->setCity("北京");
						$shop->save(false);
					}
				}
			}
		}
	}

	/**
	 *
	 */
	public function actionCourse()
	{

		$shops = ForHereShop::find()->where(['region' => '清河'])->orderBy('id')->limit($this->limit)->offset($this->offset)->all();
		foreach ($shops as $index => $shop) {
			if ($shop instanceof ForHereShop) {
				$url = $shop->getLink();
				$shopData = QueryList::get($url)
					// 设置采集规则
					->rules([
						'open_hours' => array('#basic-info > div.other.J-other > p:nth-child(1) > span.item', 'text'),
						'taste_rate' => array('#comment_score > span:nth-child(1)', 'text'),
						'environment_rate' => array('#comment_score > span:nth-child(2)', 'text'),
						'service_rate' => array('#comment_score > span:nth-child(3)', 'text'),
						'avg_price' => array('#avgPriceTitle', 'text'),
						'district' => array('#body > div > div.breadcrumb > a:nth-child(3)', 'text'),
						'comment_quantity' => array('#reviewCount', 'text'),
						'telephone' => array('#basic-info > p > span.item', 'text'),
					])->query()->getData();
				$item = $shopData->all()[0];
				$shop->setOpenHours($item['open_hours']);
				$shop->setTasteRate((float)filter_var($item['taste_rate'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION));
				$shop->setEnvironmentRate((float)filter_var($item['environment_rate'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION));
				$shop->setServiceRate((float)filter_var($item['service_rate'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION));
				$shop->setAvgPrice((int)filter_var($item['avg_price'], FILTER_SANITIZE_NUMBER_INT));
				$shop->setDistrict($item['district']);
				$shop->setCommentQuantity((int)filter_var($item['comment_quantity'], FILTER_SANITIZE_NUMBER_INT));
				$shop->setTelephone($item['telephone']);
				$shop->setDianpingId((int)filter_var($url, FILTER_SANITIZE_NUMBER_INT));
				$shop->save(false);
				echo $index . "#" . $shop->getId() . "\n";
				$dishUrl = $url . "/dishlist";
				$pageQl = QueryList::use (PhantomJs::class, '/Users/baidu/Documents/workspace/blueberry/services/admin/bin/phantomjs');
				$pageData = $pageQl->browser($dishUrl)->rules([
					'number' => array('#index-main > div > div > div.shop-food-list-page > a', 'text'),
				])->query()->getData();
				$page = $pageData->all();
				$cnt = count($page);
				if ($page[$cnt - 1]['number'] == '下一页') {
					$total = $page[$cnt - 2]['number'];
				} else {
					$total = $page[$cnt - 1]['number'];
				}
				if ($total === null) {
					$total = 1;
				}
				for ($j = 1; $j <= $total; $j++) {
					$ql = QueryList::use (PhantomJs::class, '/Users/baidu/Documents/workspace/blueberry/services/admin/bin/phantomjs');
					$dishData = $ql->browser($dishUrl . "/p" . $j)->rules([
						'name' => array('div.shop-food-name', 'text'),
						'price' => array('div.shop-food-money', 'text'),
						'recommend_count' => array('div.recommend-count', 'text'),
						'link' => array('#index-main > div > div > div.list-desc > ul > a', 'href'),
						'image_url' => array('#index-main > div > div > div.list-desc > ul > a > div.shop-food-img > img', 'src'),
					])->query()->getData();
					$dishes = $dishData->all();
					if (empty($dishes)) {
						echo $dishUrl . "/p" . $j . "\n";
					} else {
						foreach ($dishes as $dish) {
							$course = new ForHereCourse();
							$course->setId(Uuid::uuid4());
							$course->setShopId($shop->getId());
							$course->setName($dish['name']);
							$course->setPrice((int)filter_var($dish['price'], FILTER_SANITIZE_NUMBER_INT));
							$course->setLink("http://www.dianping.com" . $dish['link']);
							$course->setRecommendCount((int)filter_var($dish['recommend_count'], FILTER_SANITIZE_NUMBER_INT));
							$course->setImageUrl($dish['image_url']);
							$course->save(false);
						}
					}
				}
				echo $index . "#" . $shop->getName() . "\n";
			}
		}

	}

	public function actionTest()
	{
		$url = "http://www.dianping.com/shop/67978385/dishlist";
		$ql = QueryList::use (PhantomJs::class, '/Users/baidu/Documents/workspace/blueberry/services/admin/bin/phantomjs');
		$data = $ql->browser($url)->rules([
			'name' => array('div.shop-food-name', 'text'),
			'price' => array('div.shop-food-money', 'text'),
			'recommend_count' => array('div.recommend-count', 'text'),
			'link' => array('#index-main > div > div > div.list-desc > ul > a', 'href'),
			'image_url' => array('#index-main > div > div > div.list-desc > ul > a > div.shop-food-img > img', 'src'),
		])->query()->getData();
		print_r($data->all());
	}

	public function actionShopExtra()
	{

		$shops = ForHereShop::find()->where(['region' => '清河', 'telephone' => null])->orderBy('id')->limit($this->limit)->offset($this->offset)->all();
		foreach ($shops as $index => $shop) {
			if ($shop instanceof ForHereShop) {
				$url = $shop->getLink();
				$ql = QueryList::use (PhantomJs::class, '/Users/baidu/Documents/workspace/blueberry/services/admin/bin/phantomjs');
				$shopData = $ql->browser($url)
					// 设置采集规则
					->rules([
						'open_hours' => array('#basic-info > div.other.J-other > p:nth-child(1) > span.item', 'text'),
						'taste_rate' => array('#comment_score > span:nth-child(1)', 'text'),
						'environment_rate' => array('#comment_score > span:nth-child(2)', 'text'),
						'service_rate' => array('#comment_score > span:nth-child(3)', 'text'),
						'avg_price' => array('#avgPriceTitle', 'text'),
						'district' => array('#body > div > div.breadcrumb > a:nth-child(3)', 'text'),
						'comment_quantity' => array('#reviewCount', 'text'),
						'telephone' => array('#basic-info > p > span.item', 'text'),
					])->query()->getData();
				$item = $shopData->all()[0];
				if ($item) {
					$shop->setOpenHours($item['open_hours']);
					$shop->setTasteRate((float)filter_var($item['taste_rate'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION));
					$shop->setEnvironmentRate((float)filter_var($item['environment_rate'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION));
					$shop->setServiceRate((float)filter_var($item['service_rate'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION));
					$shop->setAvgPrice((int)filter_var($item['avg_price'], FILTER_SANITIZE_NUMBER_INT));
					$shop->setDistrict($item['district']);
					$shop->setCommentQuantity((int)filter_var($item['comment_quantity'], FILTER_SANITIZE_NUMBER_INT));
					$shop->setTelephone($item['telephone']);
					$shop->setDianpingId((int)filter_var($url, FILTER_SANITIZE_NUMBER_INT));
					$shop->save(false);
				}
				echo $index . "#" . $shop->getId() . "\n";
				echo $index . "#" . $shop->getName() . "\n";
			}
		}

	}

}
