<?php

namespace app\commands;

use app\models\Courses;
use app\models\Ingredients;
use app\models\Label;
use app\models\MealCombination;
use app\models\OauthUsers;
use app\models\what2eat\ForHereShop;
use app\services\Profile;
use yii\console\Controller;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 */
class ProfileController extends Controller
{
	/**
	 *
	 */
	public function actionCourse()
	{
		$courses = Courses::find()
			->where(['status' => Courses::STATUS_MANUAL_EDITED])
			->andWhere('update_time > course_profile_update_time or course_profile_update_time is null')
			->all();
		foreach ($courses as $index => $course) {
			if ($course instanceof Courses) {
				$labelsString = $course->getLabels();
				if ($labelsString) {
					$labelsArray = explode("#", $labelsString);
					$json = Profile::constructLabelTree($labelsArray);
				}
				$pIngredientsId = explode("#", $course->getPrincipalIngredients());
				if (count($pIngredientsId)) {
					foreach ($pIngredientsId as $ingredientId) {
						$ingredient = Ingredients::findOne(['id' => $ingredientId]);
						if ($ingredient instanceof Ingredients) {
							$json['主食材'][] = [
								'名称' => $ingredient->getName(),
								'别名' => $ingredient->getAliasArray(),
								'功效' => $ingredient->getFunctionArray()
							];
						}
					}
				}
				$cIngredientsId = explode("#", $course->getComplementaryIngredients());
				if (count($cIngredientsId)) {
					foreach ($cIngredientsId as $ingredientId) {
						$ingredient = Ingredients::findOne(['id' => $ingredientId]);
						if ($ingredient instanceof Ingredients) {
							$json['辅食材'][] = [
								'名称' => $ingredient->getName(),
								'别名' => $ingredient->getAliasArray(),
								'功效' => $ingredient->getFunctionArray()
							];
						}
					}
				}
				$course->setCourseProfile(json_encode($json, JSON_UNESCAPED_UNICODE));
				$course->setCourseProfileUpdateTime($course->getUpdateTime());
				$course->save(false);
				echo $index . "\n";
			}
		}
	}


	public function actionShop()
	{
		$shops = ForHereShop::find()
			->where(['status' => ForHereShop::STATUS_MANUAL_EDITED, 'region' => '清河'])
//			->andWhere('update_time > shop_profile_update_time or shop_profile_update_time is null')
			->all();
		foreach ($shops as $index => $shop) {
			if ($shop instanceof ForHereShop) {
				$labelsString = $shop->getLabels();
				if ($labelsString) {
					$labelsArray = explode("#", $labelsString);
					$json = Profile::constructLabelTree($labelsArray);
				}
				if ($shop->getCategory()) {
					$categories = explode("#", $shop->getCategory());
					foreach ($categories as $category) {
						$combinations = MealCombination::find()->where(['category' => $category])->all();
						foreach ($combinations as $combination) {
							if ($combination instanceof MealCombination) {
								$json['meal_combination'][] = [
									'number_of_dinners' => $combination->getNumberOfDinners(),
									'combination' => json_decode($combination->getDisplayCombinationJson(), true)
								];
							}
						}
					}
				}
				$shop->setShopProfile(json_encode($json, JSON_UNESCAPED_UNICODE));
				$shop->setShopProfileUpdateTime($shop->getUpdateTime());
				$shop->save(false);
				echo $index . "\n";
			}
		}
	}

	public function actionUser()
	{
		$users = OauthUsers::find()->all();
		foreach ($users as $user) {
			if ($user instanceof OauthUsers) {
				Profile::appendUserProfile($user->getId());
				echo $user->getId();
			}
		}
	}

	public function actionCheckCourse()
	{
		$courses = Courses::find()->where(['status' => Courses::STATUS_MANUAL_EDITED])->all();
		$cnt = 0;
		foreach ($courses as $index => $course) {
			if ($course instanceof Courses) {
				$labelsString = $course->getLabels();
				$labelsArray = explode("#", $labelsString);
				if ($labelsArray) {
					foreach ($labelsArray as $labelId) {
						$label = Label::findOne(['id' => $labelId]);
						if ($label instanceof Label) {
							if ($label->hasChildren()) {
								echo $course->getId() . "\n";
								$cnt++;
								break;
							}
						}
					}
				}
			}
		}
		echo "number:$cnt";
	}

	public function actionCheckShop()
	{
		$shops = ForHereShop::find()->where(['status' => ForHereShop::STATUS_MANUAL_EDITED, 'region' => '清河'])->all();
		$cnt = 0;
		foreach ($shops as $index => $shop) {
			if ($shop instanceof ForHereShop) {
				$labelsString = $shop->getLabels();
				$labelsArray = explode("#", $labelsString);
				if ($labelsArray) {
					foreach ($labelsArray as $labelId) {
						$label = Label::findOne(['id' => $labelId]);
						if ($label instanceof Label) {
							if ($label->hasChildren()) {
								echo $shop->getId() . "\n";
								$cnt++;
								break;
							}
						}
					}
				}
			}
		}
		echo "number:$cnt";
	}

}
