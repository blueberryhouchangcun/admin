<?php

namespace app\commands;

use app\models\Ingredients;
use app\models\IngredientsAlias;
use app\models\IngredientsFunction;
use yii\console\Controller;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 */
class IngredientsController extends Controller
{
	public function actionExtractAlias()
	{
		$ingredients = Ingredients::find()->all();
		foreach ($ingredients as $ingredient) {
			$ids = [];
			if ($ingredient instanceof Ingredients) {
				$alias = $ingredient->getAlias();
				if ($alias) {
					$alias = explode("#", $alias);
					foreach ($alias as $item) {
						$exist = IngredientsAlias::findOne(['alias' => $item]);
						if ($exist) {
							$ids[] = $exist->getId();
						} else {
							$exist = new IngredientsAlias();
							$exist->setAlias($item);
							$exist->save(false);
							$ids[] = $exist->getId();
						}
					}
				}
				if ($ids) {
					$idsString = implode("#", $ids);
					$ingredient->setAlias($idsString);
					$ingredient->save(false);
				}
			}
		}
	}

	public function actionExtractFunction()
	{
		$ingredients = Ingredients::find()->all();
		foreach ($ingredients as $ingredient) {
			$ids = [];
			if ($ingredient instanceof Ingredients) {
				$function = $ingredient->getFunction();
				if ($function) {
					$function = explode("#", $function);
					foreach ($function as $item) {
						$exist = IngredientsFunction::findOne(['function' => $item]);
						if ($exist) {
							$ids[] = $exist->getId();
						} else {
							$exist = new IngredientsFunction();
							$exist->setFunction($item);
							$exist->save(false);
							$ids[] = $exist->getId();
						}
					}
				}
				if ($ids) {
					$idsString = implode("#", $ids);
					$ingredient->setFunction($idsString);
					$ingredient->save(false);
				}
			}
		}
	}

	public function actionAddIngredientIdToAlias()
	{
		$ingredients = Ingredients::find()->all();
		foreach ($ingredients as $index => $item) {
			if ($item instanceof Ingredients) {
				$aliasIds = $item->getAlias();
				if ($aliasIds) {
					$aliasIds = explode("#", $aliasIds);
					foreach ($aliasIds as $aliasId) {
						$alias = IngredientsAlias::findOne(['id' => $aliasId]);
						if ($alias instanceof IngredientsAlias) {
							if ($alias->getIngredientId() === null) {
								$alias->setIngredientId($item->getId());
								$alias->save(false);
							}
						}
					}
				}
			}
			echo $index . "#DONE\n";
		}
	}

}
