<?php

namespace app\commands;

use app\models\Courses;
use app\models\scrapy\ForHereCourse;
use app\models\scrapy\ForHereShop;
use JonnyW\PhantomJs\Http\RequestInterface;
use QL\Ext\PhantomJs;
use QL\QueryList;
use Ramsey\Uuid\Uuid;
use yii\console\Controller;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 */
class StreamController extends Controller
{
	const LIMIT = 5000;

	public function actionShop()
	{
		$shopsFromScrapy = ForHereShop::find()->where(['region' => '清河'])->all();
		foreach ($shopsFromScrapy as $shop) {
			if ($shop instanceof ForHereShop) {
				$shopWhat2eat = \app\models\what2eat\ForHereShop::findOne(['id' => $shop->getId()]);
				if ($shopWhat2eat === null) {
					$shopWhat2eat = new \app\models\what2eat\ForHereShop();
				}
				$shopWhat2eat->setId($shop->getId());
				$shopWhat2eat->setCategory($shop->getCategory());
				$shopWhat2eat->setName($shop->getName());
				$shopWhat2eat->setOpenHours($shop->getOpenHours());
				$shopWhat2eat->setTasteRate($shop->getTasteRate());
				$shopWhat2eat->setEnvironmentRate($shop->getEnvironmentRate());
				$shopWhat2eat->setServiceRate($shop->getServiceRate());
				$shopWhat2eat->setLink($shop->getLink());
				$shopWhat2eat->setAddress($shop->getAddress());
				$shopWhat2eat->setRegion($shop->getRegion());
				$shopWhat2eat->setDistrict($shop->getDistrict());
				$shopWhat2eat->setCity($shop->getCity());
				$shopWhat2eat->setTelephone($shop->getTelephone());
				$shopWhat2eat->setAvgPrice($shop->getAvgPrice());
				$shopWhat2eat->setCommentQuantity($shop->getCommentQuantity());
				$shopWhat2eat->setDianpingId($shop->getDianpingId());
				$shopWhat2eat->setStatus(\app\models\what2eat\ForHereShop::STATUS_IMPORTED);
				$shopWhat2eat->save(false);
			}
		}
	}

	public function actionCourse()
	{
		$cnt = ForHereCourse::find()->count();
		for ($j = 0; $j < $cnt; $j++) {
			if ($j % self::LIMIT == 0) {
				$coursesFromScrapy = ForHereCourse::find()->offset($j)->limit(self::LIMIT)->all();
				foreach ($coursesFromScrapy as $index => $course) {
					if ($course instanceof ForHereCourse) {
						$courseWhat2eat = \app\models\what2eat\ForHereCourse::findOne(['id' => $course->getId()]);
						if ($courseWhat2eat === null) {
							$courseWhat2eat = new \app\models\what2eat\ForHereCourse();
						}
						$courseWhat2eat->setId($course->getId());
						$courseWhat2eat->setName($course->getName());
						$courseWhat2eat->setImageUrl($course->getImageUrl());
						$courseWhat2eat->setPrice($course->getPrice());
						$courseWhat2eat->setLink($course->getLink());
						$courseWhat2eat->setRecommendCount($course->getRecommendCount());
						$courseWhat2eat->setShopId($course->getShopId());
						$courseWhat2eat->setSource("dianping");
						$courseWhat2eat->setStatus(\app\models\what2eat\ForHereCourse::STATUS_IMPORTED);
						$courseWhat2eat->save(false);
						echo $index . "#" . $course->getName() . "\n";
					}
				}
			}
		}


	}

	public function actionRemoveDuplicateCourse()
	{
		$cnt = \app\models\what2eat\ForHereCourse::find()->count();
		for ($j = 0; $j < $cnt; $j++) {
			if ($j % self::LIMIT == 0) {
				$coursesScrapy = \app\models\what2eat\ForHereCourse::find()->offset($j)->limit(self::LIMIT)->all();
				foreach ($coursesScrapy as $index => $item) {
					if ($item instanceof \app\models\what2eat\ForHereCourse) {
						$course = Courses::findOne(['name' => $item->getName()]);
						if ($course === null) {
							$course = new Courses();
							$course->setId(Uuid::uuid4());
							$course->setName($item->getName());
							if ($item->getRecommendCount() > $course->getAvgRecommendCount()) {
								$course->setAvgRecommendCount($item->getRecommendCount());
							}
							$course->setStatus(Courses::STATUS_IMPORTED);
							$course->setCreateTime(time());
							$course->save(false);
						}
						$item->setCourseId($course->getId());
						$item->save(false);
						echo ($j + $index) . "#DONE\n";
					}
				}
			}
		}
	}
}
