<?php
/**
 * Created by PhpStorm.
 * User: manjinxin
 * Date: 25/04/2018
 * Time: 17:31
 */

namespace app\services;


class Curl
{

	/**
	 * @param $url
	 * @param $port
	 * @param $params
	 * @param $headers
	 *
	 * @return bool|mixed
	 */
	static public function get($url, $port, $params, $headers = [])
	{
		$curl = curl_init();
		$query = http_build_query($params);
		$url = $url . "?" . $query;
		curl_setopt_array($curl, array(
			CURLOPT_PORT => $port,
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => $headers,
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);


		if ($response !== false) {
			$response = json_decode($response, true);
		}
		if ($err) {
			$response['status'] = 500;
			$response['message'] = $err;
		}
		return $response;
	}

}