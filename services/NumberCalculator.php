<?php
/**
 * Created by PhpStorm.
 * User: manjinxin
 * Date: 06/12/2017
 * Time: 15:58
 */

namespace app\services;


class NumberCalculator
{
	/**
	 * @param $current
	 * @param $last
	 *
	 * @return float|int|string
	 */

	static public function getPureChainRatio($current, $last)
	{
		$return = 0;
		if ($last != 0) {
			$return = ($current - $last) / $last;
			$return = floatval($return);
		}
		return $return;
	}

	/**
	 * @param $current
	 * @param $last
	 *
	 * @return float|int|string
	 */

	static public function getPureChainRatioForShare($current, $last)
	{
		$return = $current - $last;
		$return = floatval($return);
		return $return;
	}

	static public function getChainRatioForTable($ratio)
	{
		$ratio = number_format($ratio * 100, 2);
		if ($ratio > 0) {
			$return = '<span class="text-success">' . $ratio . '%' . '</span>';
		} elseif ($ratio < 0) {
			$return = '<span class="text-danger">' . $ratio . '%' . '</span>';
		} else {
			$return = '<span class="text-success">0.00%</span>';
		}
		return $return;
	}


	static public function getChainRatioForChart($current, $last)
	{
		return self::getPureChainRatio($current, $last);
	}


	static public function getPureShare($se, $dapan)
	{
		$return = 0;
		if ($dapan != 0) {
			$return = $se / $dapan;
			$return = number_format($return, 4);
			$return = floatval($return);
		}
		return $return;
	}

	static public function getShareForChart($se, $dapan)
	{
		return self::getPureShare($se, $dapan) * 100;
	}

	static public function getShareForTable($share)
	{
		return number_format($share, 4) * 100 . '%';
	}


	static function getPureAvg($numbers = [])
	{
		$return = 0;
		if (count($numbers) != 0) {
			$return = array_sum($numbers) / count($numbers);
			$return = intval($return);
		}
		return $return;
	}

	static function getAvgForChart($numbers = [])
	{
		return self::getPureAvg($numbers);
	}

	static function getAvgForTable($number)
	{
		return number_format($number);
	}

	/**
	 * @param $numbers
	 *
	 * @return array|null|string|string[]
	 */
	static function filterNumber($numbers)
	{
		$pattern = '/<.*?>(.*?)<\/.*?>/';
		if (is_array($numbers)) {
			$return = [];
			foreach ($numbers as $number) {
				$return[] = preg_replace($pattern, '$1', $number);
			}
		} else {
			$return = preg_replace($pattern, '$1', $numbers);
		}
		return $return;
	}

	static function filterPureNumber($numbers)
	{
		$pattern = '/<.*?>(.*?)<\/.*?>/';
		if (is_array($numbers)) {
			$return = [];
			foreach ($numbers as $number) {
				$return[] = str_replace([',', '%'], '', preg_replace($pattern, '$1', $number));
			}
		} else {
			$return = str_replace([',', '%'], '', preg_replace($pattern, '$1', $numbers));
		}
		return $return;
	}


}