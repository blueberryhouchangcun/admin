<?php
/**
 * Created by PhpStorm.
 * User: manjinxin
 * Date: 06/12/2017
 * Time: 15:58
 */

namespace app\services;


class DatetimeGetter
{
	static public function lastDayOf($period, \DateTime $date = null)
	{
		$period = strtolower($period);
		$validPeriods = array('year', 'quarter', 'month', 'week', 'day');

		if (!in_array($period, $validPeriods))
			die('Period must be one of: ' . implode(', ', $validPeriods));

		$newDate = ($date === null) ? new \DateTime() : clone $date;

		switch ($period) {
			case 'year':
				$newDate->modify('last day of december ' . $newDate->format('Y'));
				break;
			case 'quarter':
				$month = $newDate->format('n');
				if ($month < 4) {
					$newDate->modify('last day of march ' . $newDate->format('Y'));
				} elseif ($month > 3 && $month < 7) {
					$newDate->modify('last day of june ' . $newDate->format('Y'));
				} elseif ($month > 6 && $month < 10) {
					$newDate->modify('last day of september ' . $newDate->format('Y'));
				} elseif ($month > 9) {
					$newDate->modify('last day of december ' . $newDate->format('Y'));
				}
				break;
			case 'month':
				$newDate->modify('last day of this month');
				break;
			case 'week':
				$newDate->modify(($newDate->format('w') === '0') ? 'now' : 'sunday this week');
				break;
			case 'day':
				break;

		}

		return $newDate;
	}

	static public function firstDayOf($period, \DateTime $date = null)
	{
		$period = strtolower($period);
		$validPeriods = array('year', 'quarter', 'month', 'week', 'day');

		if (!in_array($period, $validPeriods))
			die('Period must be one of: ' . implode(', ', $validPeriods));

		$newDate = ($date === null) ? new \DateTime() : clone $date;

		switch ($period) {
			case 'year':
				$newDate->modify('first day of january ' . $newDate->format('Y'));
				break;
			case 'quarter':
				$month = $newDate->format('n');

				if ($month < 4) {
					$newDate->modify('first day of january ' . $newDate->format('Y'));
				} elseif ($month > 3 && $month < 7) {
					$newDate->modify('first day of april ' . $newDate->format('Y'));
				} elseif ($month > 6 && $month < 10) {
					$newDate->modify('first day of july ' . $newDate->format('Y'));
				} elseif ($month > 9) {
					$newDate->modify('first day of october ' . $newDate->format('Y'));
				}
				break;
			case 'month':
				$newDate->modify('first day of this month');
				break;
			case 'week':
				$newDate->modify(($newDate->format('w') === '0') ? 'monday last week' : 'monday this week');
				break;
			case 'day':
				break;
		}
		return $newDate;
	}

}