<?php
/**
 * Created by PhpStorm.
 * User: manjinxin
 * Date: 2018/6/8
 * Time: 23:08
 */

namespace app\services;


use app\models\account\UserProfile;
use app\models\AnswerOption;
use app\models\Ingredients;
use app\models\Label;
use app\models\OauthUsers;
use app\models\UserAnswers;

class Profile
{
	static public function appendUserProfile($userId)
	{
		$json = [];
		$user = OauthUsers::findOne(['id' => $userId]);
		if ($user instanceof OauthUsers) {
			$json['gender'] = $user->getGender();
			$json['birth_year'] = $user->getBirthYear();
			$json['height'] = $user->getHeight();
			$json['weight'] = $user->getWeight();
			$json['course_labels'] = [];
			$json['shop_labels'] = [];
			$json['avoid_ingredients'] = [];
			$course_labels = [];
			$shop_labels = [];
			$avoid_ingredients = [];
			$userAnswers = UserAnswers::find()->where(['user_id' => $userId])->all();
			foreach ($userAnswers as $userAnswer) {
				if ($userAnswer instanceof UserAnswers) {
					$answerOptionIds = $userAnswer->getAnswerOptionIds();
					if ($answerOptionIds) {
						$answerOptionIds = explode("#", $answerOptionIds);
						foreach ($answerOptionIds as $answerOptionId) {
							$answerOption = AnswerOption::findOne(['id' => $answerOptionId]);
							if ($answerOption instanceof AnswerOption) {
								if (empty($course_labels)) {
									$course_labels = explode("#", $answerOption->getCourseLabels());
								} else {
									$course_labels = array_merge($course_labels, explode("#", $answerOption->getCourseLabels()));
								}
								$course_labels = array_unique($course_labels);

								if (empty($shop_labels)) {
									$shop_labels = explode("#", $answerOption->getShopLabels());
								} else {
									$shop_labels = array_merge($shop_labels, explode("#", $answerOption->getShopLabels()));
								}
								$shop_labels = array_unique($shop_labels);

								if (empty($avoid_ingredients)) {
									$avoid_ingredients = explode("#", $answerOption->getAvoidIngredients());
								} else {
									$avoid_ingredients = array_merge($avoid_ingredients, explode("#", $answerOption->getAvoidIngredients()));
								}
								$avoid_ingredients = array_unique($avoid_ingredients);
							}

						}
					}
				}
			}
			if ($course_labels) {
				$json['course_labels'] = self::constructLabelTree($course_labels);
			}
			if ($shop_labels) {
				$json['shop_labels'] = self::constructLabelTree($shop_labels);
			}
			if ($avoid_ingredients){
				foreach ($avoid_ingredients as $ingredientId) {
					$ingredient = Ingredients::findOne(['id' => $ingredientId]);
					if ($ingredient instanceof Ingredients) {
						$json['avoid_ingredients'][] = [
							'名称' => $ingredient->getName(),
							'别名' => $ingredient->getAliasArray(),
							'功效' => $ingredient->getFunctionArray()
						];
					}
				}
			}

			$userProfile = new UserProfile();
			$userProfile->setCreateTime(time());
			$userProfile->setUserProfile(json_encode($json, JSON_UNESCAPED_UNICODE));
			$userProfile->setUserId($userId);
			$userProfile->save(false);
			return true;
		}
		return false;
	}

	static public function constructLabelTree($labelIds)
	{
		$return = [];
		foreach ($labelIds as $labelId) {
			$label = Label::findOne(['id' => $labelId]);
			if ($label instanceof Label) {
				if (!$label->hasChildren()) {
					$tmp = [];
					while ($label->getParent() !== null) {
						$parent = $label->getParent();
						if (!$label->hasChildren()) {
							$tmp[$parent->getName()][] = $label->getName();
						} else {
							$tmp[$parent->getName()] = $tmp;
							unset($tmp[$label->getName()]);
						}
						$label = $parent;
					}
					if (empty($return)) {
						$return = $tmp;
					} else {
						$return = array_merge_recursive($return, $tmp);
					}
				}
			}
		}
		return isset($return['Home']) ? $return['Home'] : [];
	}
}