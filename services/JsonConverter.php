<?php
/**
 * Created by PhpStorm.
 * User: manjinxin
 * Date: 16/04/2018
 * Time: 16:02
 */

namespace app\services;


class JsonConverter
{
	private $platArray = [];

	static public function array2json($objects)
	{
		$itemsByReference = [];
		foreach ($objects as $key => &$item) {
			$itemsByReference[$item['id']] = &$item;
			$itemsByReference[$item['id']]['children'] = array();
		}
		foreach ($objects as $key => &$item)
			if ($item['parent_id'] && isset($itemsByReference[$item['parent_id']]))
				$itemsByReference [$item['parent_id']]['children'][] = &$item;
		foreach ($objects as $key => &$item) {
			if ($item['parent_id'] && isset($itemsByReference[$item['parent_id']]))
				unset($objects[$key]);
		}
		$json = trim((json_encode($objects)), '[]');
		return $json;
	}

	public function recursiveArray2array($array)
	{
		$this->platArray[] = [
			'id' => $array['id'],
			'name' => $array['content'],
			'parent_id' => $array['parent']
		];
		if (is_array($array['children'])) {
			foreach ($array['children'] as $child) {
				$this->recursiveArray2array($child);
			}
		}
		return $this->platArray;
	}
}