<?php

namespace app\modules\api\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;

class BaseController extends Controller
{

	/**
	 * @param $action
	 *
	 * @return bool|Response
	 * @throws \yii\web\BadRequestHttpException
	 */
	public function beforeAction($action)
	{
		Yii::$app->response->format = Response::FORMAT_JSON;
		return parent::beforeAction($action);
	}

}