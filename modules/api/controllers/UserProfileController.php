<?php

namespace app\modules\api\controllers;

use app\services\Profile;
use Yii;

class UserProfileController extends BaseController
{
	/**
	 * @return array
	 */
	public function actionAppend()
	{
		$request = Yii::$app->getRequest();
		$userId = $request->post('id', "");
		if (Profile::appendUserProfile($userId)) {
			$status = 200;
			$message = "success";
		} else {
			$status = 500;
			$message = "[ERROR] Append User Profile Failed";
		}
		$return = [
			'status' => $status,
			'message' => $message,
		];
		return $return;
	}
}