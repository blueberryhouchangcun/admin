<?php
namespace app\modules\admin;

class Module extends \yii\base\Module
{
    /**
     *
     */
    public function init()
    {
        $this->layout = 'main';
        parent::init();
    }
}