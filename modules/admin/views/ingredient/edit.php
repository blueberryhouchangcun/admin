<?php
/* @var $ingredient \app\models\Ingredients */
?>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="/admin/index">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="#">基础数据</a>
    </li>
    <li class="breadcrumb-item"><a href="/admin/ingredient">食材</a></li>
    <li class="breadcrumb-item active">编辑食材</li>
</ol>
<section style="margin-top: 50px">
    <div class="row">
        <div class="col-sm-3">
        </div>
        <div class="col-sm-6">
            <form method="post" action="/admin/ingredient/edit?id=<?= $ingredient->getId(); ?>">
                <div class="card">
                    <div class="card-body">
                        <div class="form-header default-color">
                            <h3>
                                <i class="fa fa-pencil"></i> 新建/编辑 <?= $ingredient->getName() ?></h3>
                            <p class="dancing-script">Edited by: <?= $ingredient->getEditor() ?></p>
                            <p class="dancing-script">Created: <?= $ingredient->getDisplayCreateTime() ?>
                                Updated: <?= $ingredient->getDisplayUpdateTime() ?></p>
                        </div>
						<?php if ($error): ?>
                            <div class="md-form">
                                <p style="text-align: center" class="text-danger"><?= $error ?></p>
                            </div>
						<?php endif; ?>
                        <div class="md-form">
                            <input type="text" id="ingredient_name" name="ingredient_name" class="form-control"
                                   value="<?= $ingredient->getName(); ?>">
                            <label for="ingredient_name">名称</label>
                        </div>
                        <div class="md-form">
                            <input type="text" id="alias" name="alias" class="form-control"
                                   value="<?= $ingredient->getDisplayAlias(); ?>">
                            <label for="alias">别名(如有多个,请用 # 分隔)</label>
                        </div>
                        <div class="md-form">
                            <input type="text" id="function" name="function" class="form-control"
                                   value="<?= $ingredient->getDisplayFunction(); ?>">
                            <label for="function">功效(如有多个,请用 # 分隔)</label>
                        </div>
                        <div class="text-center">
                            <button class="btn btn-default waves-effect waves-light">保存</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
