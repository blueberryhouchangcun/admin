<?php
/* @var $user \app\models\OauthUsers */

/* @var $this yii\web\View */

use yii\web\View as View;

$script = <<< JS
$("document").ready(function() {
     $("button.btn-delete").click(function() {
       var name = $(this).attr("data-name");
       $("#confirm-name").html(name);
     });
     $('#confirm-delete').on('show.bs.modal', function(e) {
         $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
     });
});
JS;

$this->registerJs($script, View::POS_END);
?>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="/admin/index">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="#">用户管理</a>
    </li>
    <li class="breadcrumb-item active">内测用户</li>
</ol>
<section>
    <div class="row">
        <div class="col-sm-12">
            <table class="table table-sm table-striped table-bordered">
                <thead class="thead-light">
                <tr>
                    <th scope="col" style="text-align: center">
                        <button type="button" class="btn btn-sm btn-outline-info waves-effect">Username</button>
                    </th>
                    <th scope="col" style="text-align: center">
                        <button type="button" class="btn btn-sm btn-outline-info waves-effect">Status</button>
                    </th>
                    <th scope="col" style="text-align: center">
                        <button type="button" class="btn btn-sm btn-outline-info waves-effect">注册时间</button>
                    </th>
                    <th scope="col" style="text-align: center">
                        <button type="button" class="btn btn-sm btn-outline-info waves-effect">年龄</button>
                    </th>
                    <th scope="col" style="text-align: center">
                        <button type="button" class="btn btn-sm btn-outline-info waves-effect">身高(cm)</button>
                    </th>
                    <th scope="col" style="text-align: center">
                        <button type="button" class="btn btn-sm btn-outline-info waves-effect">体重(kg)</button>
                    </th>
                    <th scope="col" style="text-align: center">
                        <button type="button" class="btn btn-sm btn-outline-info waves-effect">备注</button>
                    </th>
                    <th scope="col" style="text-align: right">
                        <button onclick="location.href='/admin/user/edit'" type="button"
                                class="btn btn-success btn-sm waves-effect waves-light">新建
                        </button>
                    </th>
                </tr>
                </thead>
                <tbody>
				<?php foreach ($users as $user): ?>
                    <tr>
                        <td style="text-align: center">
							<?= $user->getLabelUsername() ?>
                        </td>
                        <td style="text-align: center">
							<?= $user->getLabelStatus() ?>
                        </td>
                        <td style="text-align: center">
							<?= $user->getDisplayCreateTime() ?>
                        </td>
                        <td style="text-align: center">
							<?= $user->getAge() ?>
                        </td>
                        <td style="text-align: center">
							<?= $user->getHeight() ?>
                        </td>
                        <td style="text-align: center">
							<?= $user->getWeight() ?>
                        </td>
                        <td style="text-align: center">
							<?= $user->getTip() ?>
                        </td>
                        <td style="text-align: right">
                            <button onclick="location.href='/admin/user/edit?id=<?= $user->getId() ?>'" type="button"
                                    class="btn btn-default btn-sm waves-effect waves-light">Edit
                            </button>
                            <button type="button" class="btn btn-sm btn-delete btn-danger waves-effect waves-light"
                                    href="#" data-toggle="modal"
                                    data-target="#confirm-delete"
                                    data-name="<?= $user->getUsername() ?>"
                                    data-href="/admin/user/delete?id=<?= $user->getId(); ?>"
                            >Delete
                            </button>
                        </td>
                    </tr>
				<?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

</section>
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <i class="fa fa-exclamation-triangle"></i>&nbsp;删除确认
            </div>
            <div class="modal-body">
                是否确认删除用户 <span id="confirm-name" style="color: red;"></span> ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <a class="btn btn-danger btn-ok">删除</a>
            </div>
        </div>
    </div>
</div>

<style>
    table.table-sm td, table.table-sm th {
        padding-top: .1rem;
        padding-bottom: .1rem
    }

    li {
        text-align: left;
    }
</style>