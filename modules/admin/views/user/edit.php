<?php
/* @var $user \app\models\OauthUsers */
/* @var $question \app\models\Question */
?>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="/admin/index">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="#">用户管理</a>
    </li>
    <li class="breadcrumb-item"><a href="/admin/user/index">内测用户</a></li>
    <li class="breadcrumb-item active">编辑用户</li>
</ol>
<section style="margin-top: 50px">
    <div class="row">

        <div class="col-sm-6">
            <form method="post" action="/admin/user/edit?id=<?= $user->getId(); ?>">
                <div class="card">
                    <div class="card-body">
                        <div class="form-header default-color">
							<?= $user->getLabelUsername() ?>
                            <p class="dancing-script">Edited by: <?= $user->getEditor() ?></p>
                            <p class="dancing-script">Created: <?= $user->getDisplayCreateTime() ?>
                                Updated: <?= $user->getDisplayUpdateTime() ?></p>
                        </div>
                        <div class="text-center">
                            <button class="btn btn-default waves-effect waves-light">保存</button>
                        </div>
                        <div class="md-form">
                            <input type="text" id="username" name="username" class="form-control"
                                   value="<?= $user->getUsername(); ?>">
                            <label for="username">Username (登录用户名)</label>
                        </div>
                        <div class="md-form">
                            <input type="text" id="password" name="password" class="form-control"
                                   value="<?= $user->getPassword(); ?>">
                            <label for="password">Password</label>
                        </div>
                        <div class="md-form">
                            <input type="text" id="last_name" name="last_name" class="form-control"
                                   value="<?= $user->getLastName(); ?>">
                            <label for="last_name">姓</label>
                        </div>
                        <div class="md-form">
                            <input type="text" id="first_name" name="first_name" class="form-control"
                                   value="<?= $user->getFirstName(); ?>">
                            <label for="first_name">名</label>
                        </div>
                        <div class="md-form">
                            <select name="gender" id="gender" class="mdb-select colorful-select dropdown-primary">
								<?= $user->getSelectOptions($user->getGender()) ?>
                            </select>
                            <label for="gender">性别</label>
                        </div>
                        <div class="md-form">
                            <input type="text" id="birth_year" name="birth_year" class="form-control"
                                   value="<?= $user->getBirthYear(); ?>">
                            <label for="birth_year">出生年份(例如:1989)</label>
                        </div>
                        <div class="md-form">
                            <input type="text" id="height" name="height" class="form-control"
                                   value="<?= $user->getHeight(); ?>">
                            <label for="height">身高(单位:cm)</label>
                        </div>
                        <div class="md-form">
                            <input type="text" id="weight" name="weight" class="form-control"
                                   value="<?= $user->getWeight(); ?>">
                            <label for="weight">体重(单位:kg)</label>
                        </div>
                        <div class="md-form">
                        <textarea type="text" id="tip" name="tip" class="form-control md-textarea"
                        ><?= $user->getTip(); ?></textarea>
                            <label for="tip">备注</label>
                        </div>
                        <div class="text-center">
                            <button class="btn btn-default waves-effect waves-light">保存</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-sm-6">
			<?php foreach ($questions as $question): ?>
                <div class="card">
                    <h6 class="card-header info-color white-text"><?= $question->getContent() ?></h6>
                    <div class="card-body">
						<?php foreach ($user->getUserAnswerByQuestionId($question->getId()) as $userAnswer): ?>
							<?php if ($userAnswer instanceof \app\models\UserAnswers): ?>
                                <h6 class="card-title dancing-script">Answered at: <?= $userAnswer->getDisplayCreateTime() ?></h6>
								<?php foreach ($userAnswer->getAnswersStringArray() as $answer): ?>
                                        <h6 class="card-title"><?= $answer ?></h6>
								<?php endforeach; ?>
							<?php endif; ?>
						<?php
						endforeach; ?>
                    </div>
                </div><br>
			<?php endforeach; ?>
        </div>
    </div>
</section>
