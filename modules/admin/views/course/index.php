<?php
/* @var $course \app\models\Courses */

/* @var $this yii\web\View */

use yii\web\View as View;

$script = <<< JS
$("document").ready(function() {
     $("button.btn-delete").click(function() {
       var name = $(this).attr("data-name");
       $("#confirm-name").html(name);
     });
     $('#confirm-delete').on('show.bs.modal', function(e) {
         $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
     });
});
JS;

$this->registerJs($script, View::POS_END);
?>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="/admin/index">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="#">基础数据</a>
    </li>
    <li class="breadcrumb-item active">核心菜品 (<?= $status?>)</li>
</ol>
<section>
    <div class="row">
        <div class="col-sm-12">
            <nav aria-label="pagination">
		        <?php
		        echo \yii\widgets\LinkPager::widget([
			        'pagination' => $pages,
			        'pageCssClass' => 'page-link waves-effect',
			        'nextPageCssClass' => 'page-link waves-effect',
			        'prevPageCssClass' => 'page-link waves-effect',
			        'activePageCssClass' => 'page-link active waves-effect',
		        ]);
		        ?>
            </nav>
            <table class="table table-sm table-striped table-bordered">
                <thead class="thead-light">
                <tr>
                    <th scope="col" style="text-align: left">
                        <button type="button" class="btn btn-sm btn-outline-info waves-effect">Course 名称</button>
                    </th>
                    <th scope="col" style="text-align: left">
                        <button type="button" class="btn btn-sm btn-outline-info waves-effect">店铺</button>
                    </th>
                    <th scope="col" style="text-align: left">
                        <button type="button" class="btn btn-sm btn-outline-info waves-effect">主食材</button>
                    </th>
                    <th scope="col" style="text-align: left">
                        <button type="button" class="btn btn-sm btn-outline-info waves-effect">Editor</button>
                    </th>
                    <th scope="col" style="text-align: left">
                        <button type="button" class="btn btn-sm btn-outline-info waves-effect">标签</button>
                    </th>
                    <th scope="col" style="text-align: left">
                        <button type="button" class="btn btn-sm btn-outline-info waves-effect">评价数</button>
                    </th>
                    <th scope="col" style="text-align: right">
                        <button onclick="location.href='/admin/course/edit'" type="button"
                                class="btn btn-success btn-sm waves-effect waves-light">新建
                        </button>
                    </th>
                </tr>
                </thead>
                <tbody>
				<?php foreach ($courses as $course): ?>
					<?php if ($course instanceof \app\models\Courses): ?>
                        <tr>
                            <td style="text-align: left">
                                <?= $course->getName() ?>
                            </td>
                            <td style="text-align: left">
		                        <?php foreach ($course->getForHereShops() as $shop): ?>
                                <?php if ($shop instanceof \app\models\what2eat\ForHereShop):?>
                                    <?= $shop->getName()?><br>
                                <?php endif;?>
                                <?php endforeach;?>
                            </td>
                            <td style="text-align: left">
								<?= $course->getIngredientsName('principal') ?></td>
                            <td style="text-align: center;width: 20%">
		                        <?= $course->getEditor() ?><br>
		                        <code style="background-color: transparent;font-size: smaller"><?= $course->getDisplayUpdateTime() ?></code>
                            </td>
                            <td style="text-align: left;;width: 10%">
								<?= $course->getLabelsName() ?></td>
                            <td style="text-align: left">
								<?= $course->getAvgRecommendCount() ?></td>
                            <td style="text-align: right;width: 20%">
                                <a target="_blank" href="/admin/course/edit?status=<?= $status; ?>&id=<?= $course->getId() ?>"
                                        class="btn btn-default btn-sm waves-effect waves-light">Edit
                                </a>
                                <?php if ($userRole == 'admin'):?>
                                <button type="button" class="btn btn-sm btn-delete btn-danger waves-effect waves-light"
                                        href="#" data-toggle="modal"
                                        data-target="#confirm-delete"
                                        data-name="<?= $course->getName() ?>"
                                        data-href="/admin/course/delete?id=<?= $course->getId(); ?>"
                                >Delete
                                </button>
                                <?php endif;?>
                            </td>
                        </tr>
					<?php endif; ?>
				<?php endforeach; ?>
                </tbody>
            </table>
            <nav aria-label="pagination">
		        <?php
		        echo \yii\widgets\LinkPager::widget([
			        'pagination' => $pages,
			        'pageCssClass' => 'page-link waves-effect',
			        'nextPageCssClass' => 'page-link waves-effect',
			        'prevPageCssClass' => 'page-link waves-effect',
			        'activePageCssClass' => 'page-link active waves-effect',
		        ]);
		        ?>
            </nav>
        </div>
    </div>

</section>
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <i class="fa fa-exclamation-triangle"></i>&nbsp;删除确认
            </div>
            <div class="modal-body">
                是否确认删除菜品 <span id="confirm-name" style="color: red;"></span> ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <a class="btn btn-danger btn-ok">删除</a>
            </div>
        </div>
    </div>
</div>

<style>
    table.table-sm td, table.table-sm th {
        padding-top: .1rem;
        padding-bottom: .1rem
    }

    li {
        text-align: left;
    }
</style>