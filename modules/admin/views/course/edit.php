<?php
/* @var $course \app\models\Courses */

/* @var $this yii\web\View */
$this->registerJsFile("/js/seek-and-wrap.js");
$this->registerJsFile("/js/jquery-ui.min.js");
$this->registerJsFile("/js/jquery.tagsinput.min.js");
$this->registerCssFile('/css/jquery-ui.min.css');
$this->registerCssFile('/css/jquery.tagsinput.min.css');


use yii\web\View as View;

$script = <<< JS
$("document").ready(function () {
    $("label[for=489]").before("<div id='p_ingredients_function'></div><div id='c_ingredients_function'></div>");
    $('#principal_ingredients').tagsInput({
        autocomplete_url:'/admin/ingredient/api',
        onChange: function() {
          showPIngredientFunction($(this).val());
        }
    });
    
    $('#complementary_ingredients').tagsInput({
        autocomplete_url:'/admin/ingredient/api',
        onChange: function() {
          showCIngredientFunction($(this).val());
        }
    });
    var words = $("#course_title").text();
    words = words.split("");
    $.each(words, function( key, value ) {
        if(value !== '.'){
            $("body").seekAndWrap({
            "tag":"span",
            "search":value,
            "class":"highlight_1"
            });
        }
            
    }); 
    var categories = $("#categories").text();
    categories = categories.split("#");
    $.each(categories, function( key, value ) {
        if(value){
            $("body").seekAndWrap({
                "tag":"span",
                "search":value + '_',
                "class":"highlight_2"
            });
                $("body").seekAndWrap({
                "tag":"span",
                "search":value,
                "class":"highlight_2"
            }); 
        }
          
    }); 
    $.each($("hr"),function(index,value) {
        $(this).addClass("hr_"+index);
    })
    $("label[for=104]").nextUntil('hr.hr_0').addClass("checkbox_weidao"); //味道
    $("hr.hr_0").nextUntil('hr.hr_1').addClass("checkbox_kougan"); //口感
    $("hr.hr_1").nextUntil('hr.hr_2').addClass("checkbox_zuofa"); //做法
    $("hr.hr_2").nextUntil('hr.hr_3').addClass("checkbox_dapei"); //搭配方式
    $("hr.hr_3").nextUntil('hr.hr_4').addClass("checkbox_gongxiao"); //强功效
    $("hr.hr_4").nextUntil('div').addClass("checkbox_jinji"); //禁忌人群
    var checkboxArray = {
        'weidao':'味道',
        'kougan':'口感',
        'zuofa':'做法',
        'dapei':'搭配方式',
        'gongxiao':'强功效',
        'jinji':'禁忌人群'
    };
    $(document).on('click', 'form input[type=submit][name=save]', function(e) {
        $.each(checkboxArray,function(key,value) {
            var isValid = 0;
            var checkbox = $("input.checkbox_"+key);
            $.each(checkbox,function() {
                if ($(this).is(':checked')){
                    isValid = 1 ;
                    return false;
                }
            })
            if(!isValid) {
              e.preventDefault();
              toastr["warning"]("请勾选"+value+"标签");
              return false;
            }  
            if($("#principal_ingredients").val() === "") {
              e.preventDefault();
              toastr["warning"]("请填写主食材");
              return false;
            }  
        })
    });

});


function showPIngredientFunction(ingredientsString) {
  $.ajax({
         url:"/admin/ingredient/get-function?term="+ ingredientsString,
         success:function(result) {
             $('#p_ingredients_function').html("");
                $.each(result, function( key, value ) {
                    $('#p_ingredients_function').append("<p>"+value.ingredient+"    "+value.function+"</p>");
                });  
         }});
}

function showCIngredientFunction(ingredientsString) {
  $.ajax({
         url:"/admin/ingredient/get-function?term="+ ingredientsString,
         success:function(result) {
             $('#c_ingredients_function').html("");
                $.each(result, function( key, value ) {
                    $('#c_ingredients_function').append("<p>"+value.ingredient+"    "+value.function+"</p>");
                });  
         }});
}


JS;

$this->registerJs($script, View::POS_END);

?>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="/admin/index">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="#">基础数据</a>
    </li>
    <li class="breadcrumb-item"><a href="/admin/course?status=<?= $status; ?>">核心菜品</a></li>
    <li class="breadcrumb-item active">编辑菜品</li>
</ol>
<section style="margin-top: 50px">
    <form method="post" action="/admin/course/edit?status=<?= $status; ?>&id=<?= $course->getId(); ?>" class="row">
        <div class="col-sm-4">
            <div class="card">
                <div class="card-body">
                    <div class="form-header default-color">
                        <h5><a target="_blank" href="https://baike.baidu.com/item/<?= $course->getName() ?>"><span
                                        id="course_title"><?= $course->getName() ?></span></a></h5>
                        <p>搭配属性分类 : <span id="categories"><?= implode("#", $categories); ?></span></p>
						<?php foreach ($course->getForHereShops() as $forHereShop): ?>
							<?php if ($forHereShop instanceof \app\models\what2eat\ForHereShop): ?>

                                <a target="_blank"
                                   href="/admin/for-here-shop/edit?status=manual_edited&id=<?= $forHereShop->getId() ?>"><?= $forHereShop->getName() ?></a>
                                <br><a target="_blank"
                                       href="<?= $forHereShop->getLink() ?>"><?= $forHereShop->getLink() ?></a>

							<?php endif; ?>
						<?php endforeach; ?>
                        <p class="dancing-script">Edited by: <?= $course->getEditor() ?></p>
                        <p class="dancing-script">Created: <?= $course->getDisplayCreateTime() ?>
                            Updated: <?= $course->getDisplayUpdateTime() ?></p>
                    </div>
                    <div class="md-form">
                        <input type="text" id="course_name" name="course_name" class="form-control"
                               value="<?= $course->getName(); ?>">
                        <label for="course_name">菜品名称</label>
                    </div>
                    <div class="md-form">
                        <p style="font-size: small;color: grey">主食材</p>
                        <input type="text" id="principal_ingredients" name="principal_ingredients" class="form-control"
                               value="<?= $course->getDisplayPrincipalIngredients(); ?>">
                    </div>
                    <div class="md-form">
                        <p style="font-size: small;color: grey">辅食材</p>
                        <input type="text" id="complementary_ingredients" name="complementary_ingredients"
                               class="form-control"
                               value="<?= $course->getDisplayComplementaryIngredients(); ?>">
                    </div>
                    <div class="md-form">
						<?php foreach ($course->getForHereCourses() as $forHereCourse): ?>
							<?php if ($forHereCourse instanceof \app\models\what2eat\ForHereCourse): ?>
                                <img src="<?= $forHereCourse->getImageUrl() ?>" alt="placeholder" class="img-thumbnail">
							<?php endif; ?>
						<?php endforeach; ?>
                    </div>
                    <div class="text-center">
                        <input class="btn btn-info waves-effect waves-light" type="submit" value="临时保存(待优化)"
                               name="review">
                        <input class="btn btn-default waves-effect waves-light" type="submit" value="保存" name="save">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="card">
                <div class="card-body">
					<?php if ($course->getCheck() == \app\models\Courses::CHECK_INVALID): ?>
                        <div class="alert alert-warning" role="alert">
                            <span class="text-danger text-center alert"><i class="fas fa-exclamation-triangle"></i>该菜品需要重新编辑: <?= $course->getCheckReason() ?></span>
                        </div>
					<?php endif; ?>
                    <div>
						<?= \app\models\Label::getCourseCheckBoxOptions($course->getLabels()) ?>
                    </div>
                    <div class="text-center">
                        <input class="btn btn-info waves-effect waves-light" type="submit" value="临时保存(待优化)"
                               name="review">
                        <input class="btn btn-default waves-effect waves-light" type="submit" value="保存" name="save">
                        <hr>
	                    <?php if ($course->getCheck() == \app\models\Courses::CHECK_INVALID): ?>
                            <div class="alert alert-warning" role="alert">
                                <span class="text-danger text-center alert"><i class="fas fa-exclamation-triangle"></i>该菜品需要重新编辑: <?= $course->getCheckReason() ?></span>
                            </div>
	                    <?php endif; ?>
						<?php if ($userRole == 'admin'): ?>
                            <hr>
							<?php if ($course->getCheck() == \app\models\Courses::CHECK_VALID): ?>
                                <div class="md-form">
                        <textarea type="text" id="check_reason" name="check_reason"
                                  class="form-control md-textarea"
                        ><?= $course->getCheckReason(); ?></textarea>
                                    <label for="check_reason">审核不通过理由</label>
                                </div>
                                <input class="btn btn-danger waves-effect waves-light" type="submit" value="审核不通过"
                                       name="check_invalid">
							<?php elseif ($course->getCheck() == \app\models\Courses::CHECK_REVIEW): ?>
                                <div class="md-form">
                        <textarea type="text" id="check_reason" name="check_reason"
                                  class="form-control md-textarea"
                        ><?= $course->getCheckReason(); ?></textarea>
                                    <label for="check_reason">审核不通过理由</label>
                                </div>
                                <input class="btn btn-danger waves-effect waves-light" type="submit" value="重新审核不通过"
                                       name="check_invalid">
                                <input class="btn btn-success waves-effect waves-light" type="submit" value="重新审核通过"
                                       name="check_valid">
							<?php endif; ?>
						<?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>
<style>
    [type=checkbox] + label, [type=radio] + label {
        padding-left: 22px;
    }

    .highlight_1 {
        background-color: orange;
    }

    .highlight_2 {
        background-color: red;
    }
</style>
