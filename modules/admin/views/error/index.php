<section>
    <div class="row">
        <div class="col-sm-3">
        </div>
        <div class="col-sm-3">
            <img id="photo" src="/image/gavin.jpeg" alt="placeholder" class="img-thumbnail">
        </div>

        <div class="col-sm-3"style="text-align: center"><br><br>
            <samp>It's just a </samp><div class='chip pink lighten-4'>404 Error !</div><samp><br><br>
                What you’re looking for may have been misplaced
                in Long Term Memory.
            </samp>
        </div>
    </div>
</section>