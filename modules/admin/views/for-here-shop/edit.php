<?php
/* @var $shop \app\models\what2eat\ForHereShop */

/* @var $this yii\web\View */
$this->registerJsFile("/js/seek-and-wrap.js");
$this->registerJsFile("/js/jquery-ui.min.js");
$this->registerJsFile("/js/jquery.tagsinput.min.js");
$this->registerCssFile('/css/jquery-ui.min.css');

use yii\web\View as View;

$script = <<< JS
$("document").ready(function () {
    var words = $("#category").text();
    $("body").seekAndWrap({
            "tag":"span",
            "search":words,
            "class":"highlight"
    });
    var shop_name = $("#shop_name").text();
    shop_name = shop_name.split("(")[0];
    var shop_link = $("#shop_link").attr('href');
    $('label[for=404]').append("<span>    (<a target='_blank' href='https://baike.baidu.com/item/"+shop_name+"'>百度百科</a>)</span>");
    $('label[for=404]').append("<span>    (<a target='_blank' href='"+shop_link+"'>大众点评</a>)</span>");
});
JS;

$this->registerJs($script, View::POS_END);

?>

<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="/admin/index">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="#">基础数据</a>
    </li>
    <li class="breadcrumb-item"><a href="/admin/for-here-shop?status=<?= $status ?>">ForHereShop</a></li>
    <li class="breadcrumb-item active">编辑 ForHereShop</li>
</ol>
<section style="margin-top: 50px">
    <form class="row" method="post"
          action="/admin/for-here-shop/edit?status=<?= $status ?>&id=<?= $shop->getId(); ?>&from=<?= $from ?>">
        <div class="col-sm-4">
            <div class="card">
                <div class="card-body">
                    <div class="form-header default-color">
                        <h5>
                            <i class="fa fa-pencil"></i><span id="shop_name"><?= $shop->getName() ?></span></h5>
                        <p class="dancing-script">Edited by: <?= $shop->getEditor() ?></p>
                        <p class="dancing-script">Created: <?= $shop->getDisplayCreateTime() ?>
                            Updated: <?= $shop->getDisplayUpdateTime() ?></p>
                    </div>
                    <div class="md-form">
                        <p style="font-size: small;color: grey">
                            <i class="fas fa-link"></i>&nbsp;
                            <a style="font-size: small" target="_blank" id="shop_link"
                               href="<?= $shop->getLink() ?>"><?= $shop->getLink() ?></a>
                        </p>
                    </div>
                    <div class="md-form">
                        <p style="font-size: small;color: grey">
                            <i class="fas fa-link"></i>&nbsp;爬虫原始菜系:
                            <span id="category"><?= $shop->getOriginalCategory() ?></span>
                        </p>
                    </div>
                    <div class="md-form">
                        <p style="font-size: small;color: grey">
                            <i class="far fa-star"></i>&nbsp;
                            口味: <?= $shop->getTasteRate(); ?> | 环境: <?= $shop->getEnvironmentRate(); ?> |
                            服务: <?= $shop->getServiceRate(); ?></p>
                    </div>
                    <div class="md-form">
                        <p style="font-size: small;color: grey">
                            <i class="fas fa-location-arrow"></i>&nbsp;
							<?= $shop->getFormattedAddress(); ?></p>
                    </div>
                    <div class="md-form">
                        <input type="text" id="name" name="name" class="form-control"
                               value="<?= $shop->getName(); ?>">
                        <label for="name">店铺名称</label>
                    </div>
                    <div class="md-form">
                        <textarea type="text" id="recommend_reason" name="recommend_reason" class="form-control md-textarea"
                        ><?= $shop->getRecommendReason(); ?></textarea>
                        <label for="recommend_reason">推荐理由</label>
                    </div>
                    <div class="text-center">
                        <button class="btn btn-default waves-effect waves-light">保存</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="card">
                <div class="card-body">
                    <div>
						<?= \app\models\Label::getShopCheckBoxOptions($shop->getLabels()) ?>
                    </div>
                    <div class="text-center">
                        <button class="btn btn-default waves-effect waves-light">保存</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>
<style>
    [type=checkbox] + label, [type=radio] + label {
        padding-left: 22px;
    }
    .highlight {
        background-color: orange;
    }
</style>