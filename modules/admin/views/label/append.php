<section>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/admin/index">Home</a>
        </li>
        <li class="breadcrumb-item">
            <a href="#">基础数据</a>
        </li>
        <li class="breadcrumb-item"><a href="/admin/label">标签系统</a></li>
        <li class="breadcrumb-item active">Append 新标签</li>
    </ol>
</section>
<section style="margin-top: 50px">
    <div class="row">
        <div class="col-sm-3">
        </div>
        <div class="col-sm-6">
            <form method="post" action="/admin/label/append?parent_id=<?= $parent_label->getId() ?>">
                <div class="card">
                    <div class="card-body">
                        <div class="form-header default-color">
                            <h3>
                                <i class="fa fa-plus"></i> Append to 标签 <?= $parent_label->getLabelName() ?></h3>
                        </div>
                        <div class="md-form">
                            <i class="fa fa-tag prefix grey-text"></i>
                            <input type="text" id="label_name" name="label_name" class="form-control">
                            <label for="label_name">标签名称</label>
                        </div>
                        <div class="text-center">
                            <button class="btn btn-default waves-effect waves-light">保存</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>


</section>