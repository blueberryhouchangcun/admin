<?php
/* @var $label \app\models\Label */
?>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="/admin/index">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="#">基础数据</a>
    </li>
    <li class="breadcrumb-item"><a href="/admin/label">标签系统</a></li>
    <li class="breadcrumb-item active">编辑标签</li>
</ol>
<section style="margin-top: 50px">
    <div class="row">
        <div class="col-sm-3" >
        </div>
        <div class="col-sm-6" >
            <form method="post" action="/admin/label/edit?id=<?= $label->getId(); ?>">
                <div class="card">
                    <div class="card-body">
                        <div class="form-header default-color">
                            <h3>
                                <i class="fa fa-pencil"></i> 编辑标签 <?= $label->getLabelName() ?></h3>
                            <p class="dancing-script">Edited by: <?= $label->getEditor() ?></p>
                            <p class="dancing-script">Created: <?= $label->getDisplayCreateTime() ?>
                                Updated: <?= $label->getDisplayUpdateTime() ?></p>                        </div>
                        <div class="md-form">
                            <input type="text" id="label_name" name="label_name" class="form-control"
                                   value="<?= $label->getName(); ?>">
                            <label for="label_name">标签名称</label>
                        </div>
                        <div class="text-center">
                            <button class="btn btn-default waves-effect waves-light">保存</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>

</section>
