<?php
/* @var $label \app\models\Label */
/* @var $this yii\web\View */
$this->registerCssFile('/css/jsmind.css');

use yii\web\View as View;

$script = <<< JS
$("document").ready(function () {
//    var mind = {
//        "meta": {
//            "name": "jsMind remote",
//            "author": "hizzgdev@163.com",
//            "version": "0.2"
//        },
//        "format": "node_array",
//        "data": $data
//    };
//    var options = {
//        container: 'jsmind_container',
//        editable: false,
//        theme: 'info'
//    };
//    var jm = new jsMind(options);
//    // show it
//    jm.show(mind);


    $("button.btn-delete").click(function () {
        var name = $(this).attr("data-name");
        $("#confirm-name").html(name);
    });
    $('#confirm-delete').on('show.bs.modal', function (e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });
});
JS;

$this->registerJs($script, View::POS_END);
?>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="/admin/index">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="#">基础数据</a>
    </li>
    <li class="breadcrumb-item active">标签系统 (<?= $type ?>)</li>
</ol>
<section>
    <div class="row">
        <div class="col-sm-12">
            <table class="table table-sm table-striped table-bordered">
                <thead class="thead-light">
                <tr>
                    <th scope="col" style="text-align: left">
                        <button type="button" class="btn btn-sm btn-outline-info waves-effect">标签名称</button>
                    </th>
                    <th scope="col" style="text-align: right">
                        <button type="button" class="btn btn-sm btn-outline-info waves-effect">Action</button>
                    </th>
                </tr>
                </thead>
                <tbody>
				<?php foreach ($labels as $label): ?>
					<?php if ($label instanceof \app\models\Label): ?>
                        <tr>
                            <td style="text-align: left"><?= $label->getTreeName() ?></td>
                            <td style="text-align: right">
								<?php if ($label->getLevel()): ?>
									<?php if (!$label->hasChildren()): ?>
                                        <button type="button"
                                                class="btn btn-sm btn-delete btn-danger waves-effect waves-light"
                                                href="#" data-toggle="modal"
                                                data-target="#confirm-delete"
                                                data-name="<?= $label->getName() ?>"
                                                data-href="/admin/label/delete?id=<?= $label->getId(); ?>"
                                        >Delete
                                        </button>
									<?php endif; ?>
                                    <button onclick="location.href='/admin/label/edit?id=<?= $label->getId() ?>'"
                                            type="button"
                                            class="btn btn-default btn-sm waves-effect waves-light">Edit
                                    </button>
                                    <button onclick="location.href='/admin/label/append?parent_id=<?= $label->getId() ?>'"
                                            type="button"
                                            class="btn btn-success btn-sm waves-effect waves-light">Append
                                    </button>
								<?php else: ?>
                                    <button onclick="location.href='/admin/label/append?parent_id=<?= $label->getId() ?>'"
                                            type="button"
                                            class="btn btn-success btn-sm waves-effect waves-light">Append
                                    </button>
								<?php endif; ?>
                            </td>
                        </tr>
					<?php endif; ?>
				<?php endforeach; ?>
                </tbody>
            </table>
        </div>
<!--        <div class="col-sm-6">-->
<!--            <div id="jsmind_container"></div>-->
<!--        </div>-->
    </div>
</section>
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <i class="fa fa-exclamation-triangle"></i>&nbsp;删除确认
            </div>
            <div class="modal-body">
                是否确认删除标签 <span id="confirm-name" style="color: red;"></span> ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <a class="btn btn-danger btn-ok">删除</a>
            </div>
        </div>
    </div>
</div>

<!--<script src="/js/jsmind.js"></script>-->
<!--<script src="/js/jsmind.draggable.js"></script>-->
<style>
    table.table-sm td, table.table-sm th {
        padding-top: .1rem;
        padding-bottom: .1rem
    }

    li {
        text-align: left;
    }
</style>