<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>What2eat</title>
    <!-- Font Awesome -->
    <!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css"
          integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <!-- What2eat -->
    <link href="/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="/css/font.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
	<?php $this->head() ?>
</head>

<body class="fixed-sn navy-blue-skin">
<?php $this->beginBody() ?>
<?php
$authManager = Yii::$app->getAuthManager();
$userRole = array_keys($authManager->getAssignments(Yii::$app->user->identity->id))[0];
?>
<!--Main Navigation-->
<header>

    <!--Navbar-->
    <nav class="navbar navbar-expand-lg navbar-dark double-nav  fixed-top scrolling-navbar">

        <!-- SideNav slide-out button -->
        <div class="float-left">
            <a href="#" data-activates="slide-out" class="button-collapse">
                <i class="fa fa-bars"></i>
            </a>
        </div>

        <!-- Breadcrumb-->
        <div class="breadcrumb-dn mr-auto">
            <p><a href="/admin/index"><span style="font-family: 'Stylish', sans-serif;font-size: larger">What2eat Everyday</span>
                    🥕</a></p>
        </div>

        <!-- Links -->
        <ul class="nav navbar-nav nav-flex-icons ml-auto">
            <li class="nav-item">
                <a class="nav-link">
                    <i class="fab fa-github"></i>
                    <span class="clearfix d-none d-sm-inline-block"
                          style="font-family: 'Dancing Script', cursive;font-size: large">Hi <?php echo Yii::$app->user->identity->username ?></span>
                </a>
            </li>
			<?php if ($userRole == 'admin'): ?>
                <li class="nav-item">
                    <a class="nav-link" href="/user/admin/index">
                        <i class="fas fa-cog"></i>
                        <span class="clearfix d-none d-sm-inline-block"
                              style="font-family: 'Dancing Script', cursive;font-size: large">Settings</span>
                    </a>
                </li>
			<?php endif; ?>
            <li class="nav-item dropdown">
                <form method="post" action="/user/security/logout">
                    <button class="btn btn-sm btn-outline-warning waves-effect"><span
                                class="clearfix d-none d-sm-inline-block"
                                style="font-family: 'Dancing Script', cursive;font-size: smaller">Sign Out</span>
                    </button>
                </form>
            </li>
        </ul>

    </nav>
    <!--/.Navbar-->

    <!-- Sidebar navigation -->
	<?php if ($userRole == 'admin'): ?>
        <div id="slide-out" class="side-nav fixed sn-bg-4">
            <ul class="custom-scrollbar list-unstyled">
                <!-- Logo -->
                <li class="logo-sn waves-effect">
                    <div class=" text-center">
                        <a href="/admin/index"><span
                                    style="font-family: 'Stylish', sans-serif;font-size: larger;color: white">What2eat Everyday</span>
                            🥕</a>
                    </div>
                </li>
                <!--/. Logo -->
                <!--Search Form-->
                <li>
                    <form class="search-form" role="search">
                        <div class="form-group waves-light">
                            <input type="text" class="form-control" placeholder="Search">
                        </div>
                    </form>
                </li>
                <!--/.Search Form-->
                <!-- Side navigation links -->
                <li>
                    <ul class="collapsible collapsible-accordion">
                        <li class="active">
                            <a class="collapsible-header waves-effect arrow-r active">
                                <i class="fas fa-table"></i>&nbsp;&nbsp;&nbsp;基础数据
                                <i class="fa fa-angle-down rotate-icon"></i>
                            </a>
                            <div class="collapsible-body">
                                <ul>
                                    <li>
                                        <a href="/admin/label?type=course" class="waves-effect">菜品标签系统</a>
                                    </li>
                                    <li>
                                        <a href="/admin/meal-combination" class="waves-effect">套餐搭配组合</a>
                                    </li>
                                    <li>
                                        <a href="/admin/label?type=shop" class="waves-effect">店铺标签系统</a>
                                    </li>
                                    <li>
                                        <a href="/admin/ingredient" class="waves-effect">食材列表</a>
                                    </li>
                                    <li>
                                        <a href="/admin/for-here-shop?status=manual_edited"
                                           class="waves-effect">已编辑店铺</a>
                                    </li>
                                    <li>
                                        <a href="/admin/course?status=manual_edited" class="waves-effect">已编辑核心菜品</a>
                                    </li>
                                    <li>
                                        <a href="/admin/course?status=locked" class="waves-effect">编辑中核心菜品</a>
                                    </li>
                                    <li>
                                        <a href="/admin/question" class="waves-effect">问题系统</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <a class="collapsible-header waves-effect arrow-r">
                                <i class="fas fa-bug"></i>&nbsp;&nbsp;&nbsp;爬虫人工审核
                                <i class="fa fa-angle-down rotate-icon"></i>
                            </a>
                            <div class="collapsible-body">
                                <ul>
                                    <!--                                <li>-->
                                    <!--                                    <a href="/admin/ingredient/check" class="waves-effect">Ingredients</a>-->
                                    <!--                                </li>-->
                                    <li>
                                        <a href="/admin/course?status=imported" class="waves-effect">待编辑核心菜品</a>
                                    </li>
                                    <!--                                <li>-->
                                    <!--                                    <a href="#" class="waves-effect">Take Away</a>-->
                                    <!--                                </li>-->
                                    <li>
                                        <a href="/admin/for-here-shop?status=imported" class="waves-effect">待编辑店铺</a>
                                    </li>
                                    <!--                                <li>-->
                                    <!--                                    <a href="#" class="waves-effect">DIY</a>-->
                                    <!--                                </li>-->
                                </ul>
                            </div>
                        </li>
                        <li>
                            <a class="collapsible-header waves-effect arrow-r">
                                <i class="fas fa-user-circle"></i>&nbsp;&nbsp;&nbsp;用户管理
                                <i class="fa fa-angle-down rotate-icon"></i>
                            </a>
                            <div class="collapsible-body">
                                <ul>
                                    <li>
                                        <a href="/admin/user/index" class="waves-effect">内测用户</a>
                                    </li>
                                    <li>
                                        <a href="/admin/analysis/label" class="waves-effect">标注统计</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </li>
                <!--/. Side navigation links -->
            </ul>

            <!-- Mask -->
            <div class="sidenav-bg mask-strong"></div>

        </div>
	<?php endif; ?>
    <!--/. Sidebar navigation -->

</header>
<!--Main Navigation-->

<!--Main layout-->
<main>
    <div class="container-fluid">
		<?= $content; ?>
    </div>
</main>
<!--Main layout-->

<!--Footer-->
<footer class="page-footer center-on-small-only">

    <!--Copyright-->
    <div class="footer-copyright">
        <div class="container-fluid">
            © <?= date('Y') ?> Copyright
            <a href="https://www.what2eateveryday.com"> What2eateveryday.com </a> | <span
                    class="clearfix d-none d-sm-inline-block"
                    style="font-family: 'Dancing Script', cursive;font-size: large">Powered by genius from Houchangcun</span>

        </div>
    </div>
    <!--/.Copyright-->

</footer>
<!--/.Footer-->

<!-- SCRIPTS -->
<!-- JQuery -->
<script type="text/javascript" src="/js/jquery-3.2.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="/js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="/js/mdb.min.js"></script>

<!-- Initializations -->
<script>
    // SideNav Initialization
    $(".button-collapse").sideNav();

    // Material Select Initialization
    $(document).ready(function () {
        $('.mdb-select').material_select();
    });

    // Data Picker Initialization
    $('.datepicker').pickadate();

    // Tooltip Initialization
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>

<?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>
