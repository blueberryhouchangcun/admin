<?php
/* @var $this yii\web\View */

use yii\web\View as View;

$script = <<< JS

JS;
$this->registerJs($script, View::POS_END);

?>

<section class="mb-12" style="text-align: center;margin-top: 50px">
<!--    <button onclick="location.href='/admin/meal-combination'" type="button"-->
<!--            class="btn peach-gradient btn-lg waves-effect waves-light">菜品搭配属性组合-->
<!--    </button>-->
    <button onclick="location.href='/admin/course/auto-loop-edit'" type="button"
            class="btn btn-success btn-lg waves-effect waves-light">开始编辑菜品
    </button>
    <button onclick="location.href='/admin/course/index?status=imported'" type="button"
            class="btn purple-gradient btn-lg waves-effect waves-light">待编辑菜品列表
    </button>

    <button onclick="location.href='/admin/course/index?status=manual_edited'" type="button"
            class="btn aqua-gradient btn-lg waves-effect waves-light">已编辑菜品列表
    </button>
    <button onclick="location.href='/admin/course/index?status=locked'" type="button"
            class="btn orange-gradient btn-lg waves-effect waves-light">编辑中菜品列表
    </button>
    <button onclick="location.href='/admin/course/index?status=review'" type="button"
            class="btn peach-gradient btn-lg waves-effect waves-light">待优化菜品列表
    </button>
    <hr>
    <button onclick="location.href='/admin/course/index?status=manual_edited&check=2'" type="button"
            class="btn btn-info btn-lg waves-effect waves-light">待重新编辑菜品列表
    </button>
	<?php if ($userRole == 'admin'):?>
        <button onclick="location.href='/admin/course/index?status=manual_edited&check=3'" type="button"
                class="btn btn-info btn-lg waves-effect waves-light">待重新审核菜品列表
        </button>
	<?php endif;?>

<!--    <button onclick="location.href='/admin/for-here-shop?status=imported'" type="button"-->
<!--            class="btn blue-gradient btn-lg waves-effect waves-light">待编辑店铺-->
<!--    </button>-->
<!--    <button onclick="location.href='/admin/label?type=course'" type="button"-->
<!--            class="btn btn-success btn-lg waves-effect waves-light">菜品标签-->
<!--    </button>-->
<!--    <button onclick="location.href='/admin/label?type=shop'" type="button"-->
<!--            class="btn btn-success btn-lg waves-effect waves-light">店铺标签-->
<!--    </button>-->
<!--    <button onclick="location.href='/admin/user/index'" type="button"-->
<!--            class="btn aqua-gradient btn-lg waves-effect waves-light">内测用户-->
<!--    </button>-->


</section>
<hr>
<div class="row" style="text-align: center">
    <div class="col-sm-4">
    </div>
    <div class="col-sm-4">
        <img src="/image/deadpool.png" alt="placeholder" class="img-thumbnail">
    </div>
</div>

