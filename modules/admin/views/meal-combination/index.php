<?php
/* @var $combination \app\models\MealCombination */

/* @var $this yii\web\View */

use yii\web\View as View;

$script = <<< JS
$("document").ready(function() {
     $("button.btn-delete").click(function() {
       var name = $(this).attr("data-name");
       $("#confirm-name").html(name);
     });
     $('#confirm-delete').on('show.bs.modal', function(e) {
         $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
     });
});
JS;

$this->registerJs($script, View::POS_END);
?>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="/admin/index">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="#">基础数据</a>
    </li>
    <li class="breadcrumb-item active">套餐搭配组成</li>
</ol>
<section>
    <div class="row">
        <div class="col-sm-12">
            <nav aria-label="pagination">
				<?php
				echo \yii\widgets\LinkPager::widget([
					'pagination' => $pages,
					'pageCssClass' => 'page-link waves-effect',
					'nextPageCssClass' => 'page-link waves-effect',
					'prevPageCssClass' => 'page-link waves-effect',
					'activePageCssClass' => 'page-link active waves-effect',
				]);
				?>
            </nav>
            <table class="table table-sm table-striped table-bordered">
                <thead class="thead-light">
                <tr>
                    <th scope="col" style="text-align: left">
                        <button type="button" class="btn btn-sm btn-outline-info waves-effect">Category</button>
                    </th>
                    <th scope="col" style="text-align: left">
                        <button type="button" class="btn btn-sm btn-outline-info waves-effect">JSON</button>
                    </th>
                    <th scope="col" style="text-align: left">
                        <button type="button" class="btn btn-sm btn-outline-info waves-effect">用餐人数</button>
                    </th>
                    <th scope="col" style="text-align: right">
                        <button onclick="location.href='/admin/meal-combination/edit'" type="button"
                                class="btn btn-success btn-sm waves-effect waves-light">新建
                        </button>
                    </th>
                </tr>
                </thead>
                <tbody>
				<?php foreach ($combinations as $combination): ?>
                    <tr>
                        <td style="text-align: left">
                            <?= $combination->getDisplayCategory() ?>
                        </td>
                        <td style="text-align: left">
                            <pre><?= $combination->getDisplayCombinationJson() ?></pre>
                        </td>
                        <td><?= $combination->getNumberOfDinners() ?>
                        </td>
                        <td style="text-align: right">
                            <button onclick="location.href='/admin/meal-combination/edit?id=<?= $combination->getId() ?>'"
                                    type="button"
                                    class="btn btn-default btn-sm waves-effect waves-light">Edit
                            </button>
                            <button type="button" class="btn btn-sm btn-delete btn-danger waves-effect waves-light"
                                    href="#" data-toggle="modal"
                                    data-target="#confirm-delete"
                                    data-name="<?= $combination->getCombinationJson() ?>"
                                    data-href="/admin/meal-combination/delete?id=<?= $combination->getId(); ?>"
                            >Delete
                            </button>
                        </td>
                    </tr>
				<?php endforeach; ?>
                </tbody>
            </table>
            <nav aria-label="pagination">
				<?php
				echo \yii\widgets\LinkPager::widget([
					'pagination' => $pages,
					'pageCssClass' => 'page-link waves-effect',
					'nextPageCssClass' => 'page-link waves-effect',
					'prevPageCssClass' => 'page-link waves-effect',
					'activePageCssClass' => 'page-link active waves-effect',
				]);
				?>
            </nav>
        </div>
    </div>

</section>
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <i class="fa fa-exclamation-triangle"></i>&nbsp;删除确认
            </div>
            <div class="modal-body">
                是否确认删除搭配组合 <span id="confirm-name" style="color: red;"></span> ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <a class="btn btn-danger btn-ok">删除</a>
            </div>
        </div>
    </div>
</div>

<style>
    table.table-sm td, table.table-sm th {
        padding-top: .1rem;
        padding-bottom: .1rem
    }

    li {
        text-align: left;
    }
</style>