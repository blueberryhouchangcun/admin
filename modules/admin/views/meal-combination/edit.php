<?php
/* @var $combination \app\models\MealCombination */

/* @var $this yii\web\View */
$this->registerJsFile("/js/autosize.min.js");

use yii\web\View as View;

$script = <<< JS
$(document).ajaxStart(function(){
    $('#wait').show();
 }).ajaxStop(function(){
    $('#wait').hide();
 });
$("document").ready(function () {
    var category = $("#category");
    $.ajax({
         url:"/admin/meal-combination/get-tips?id="+ category.val(),
         success:function(result) {
           $("#tips").html(result);
         }
       })
    autosize($('textarea'));
    category.change(function() {
       var categoryId = $(this).val();
       $.ajax({
         url:"/admin/meal-combination/get-tips?id="+categoryId,
         success:function(result) {
           $("#tips").html(result);
         }
       })
    });
});
JS;

$this->registerJs($script, View::POS_END);

?>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="/admin/index">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="#">基础数据</a>
    </li>
    <li class="breadcrumb-item"><a href="/admin/meal-combination">套餐搭配</a></li>
    <li class="breadcrumb-item active">编辑套餐搭配</li>
</ol>
<section style="margin-top: 50px">
    <form method="post" action="/admin/meal-combination/edit?id=<?= $combination->getId(); ?>" class="row">
        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <div class="form-header default-color">
                        <p class="dancing-script">Edited by: <?= $combination->getEditor() ?></p>
                        <p class="dancing-script">Updated: <?= $combination->getDisplayUpdateTime() ?></p>
                    </div>
                    <div class="text-center">
                        <button class="btn btn-default waves-effect waves-light">保存</button>
                    </div>
                    <div>
                        <select id="category" name="category" class="mdb-select colorful-select dropdown-primary"
                                searchable="搜索">
                            <option value="" disabled selected>请选择套餐属性</option>
							<?= \app\models\SubLabels::getCategoriesSelectOptions($combination->getCategory()) ?>
                        </select>
                    </div>
                    <div class="md-form">
                        <select name="number_of_dinners" id="number_of_dinners"
                                class="mdb-select colorful-select dropdown-primary">
							<?= $combination->getNumberOfDinnersSelectOptions($combination->getNumberOfDinners()) ?>
                        </select>
                        <label for="number_of_dinners">用餐人数</label>
                    </div>
                    <div class="text-center">
                        <button class="btn btn-default waves-effect waves-light">保存</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <div class="md-form">
                        <p>请按从以下属性中选择填写</p>
                        <p id="wait" style="display: none"><code>正在加载 ...........</code></p>
                        <p id="tips"></p>
                    </div>
                    <div class="md-form">
                        <textarea type="text" id="combination_json" name="combination_json"
                                  class="form-control md-textarea"
                        ><?= $combination->getDisplayCombinationJson(); ?></textarea>
                        <label for="combination_json">JSON</label>
                    </div>
                    <div class="text-center">
                        <button class="btn btn-default waves-effect waves-light">保存</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>
