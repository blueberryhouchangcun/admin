<?php
/* @var $question \app\models\Question */
/* @var $this yii\web\View */
$this->registerJsFile("/js/autosize.min.js");

use yii\web\View as View;

$script = <<< JS
$("document").ready(function() {
     $("button.btn-delete").click(function() {
       var name = $(this).attr("data-name");
       $("#confirm-name").html(name);
     });
     $('#confirm-delete').on('show.bs.modal', function(e) {
         $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
     });
});
JS;

$this->registerJs($script, View::POS_END);
?>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="/admin/index">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="#">基础数据</a>
    </li>
    <li class="breadcrumb-item"><a href="/admin/question">问题系统</a></li>
    <li class="breadcrumb-item active">编辑问题</li>
</ol>
<section style="margin-top: 50px">
    <form method="post" action="/admin/question/edit?id=<?= $question->getId(); ?>" class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="form-header default-color">
                        <h3>
                            <i class="fa fa-pencil"></i> 编辑问题#<?= $question->getOrder() ?>
                            <br><?= $question->getContent() ?>
                        </h3>
                        <p class="dancing-script">Edited by: <?= $question->getEditor() ?></p>
                        <p class="dancing-script">Created: <?= $question->getDisplayCreateTime() ?>
                            Updated: <?= $question->getDisplayUpdateTime() ?></p></div>
                    <div class="md-form">
                        <input type="text" id="content" name="content" class="form-control"
                               value="<?= $question->getContent(); ?>">
                        <label for="content">问题文本</label>
                    </div>
                    <div class="md-form">
                        <input type="text" id="order" name="order" class="form-control"
                               value="<?= $question->getOrder(); ?>">
                        <label for="order">问题顺序(请填写整数)</label>
                    </div>
                    <div class="md-form">
                        <select name="type" id="type"
                                class="mdb-select colorful-select dropdown-primary">
							<?= $question->getTypeSelectOptions($question->getType()) ?>
                        </select>
                        <label for="type">问题类型</label>
                    </div>
                    <div class="text-center">
                        <button class="btn btn-default waves-effect waves-light">保存</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>
<section>
    <div class="col-md-12">
        <div class="card card-cascade narrower mb-4">
            <div class="card-body card-body-cascade">
                <table class="table table-sm table-striped table-bordered">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col" style="text-align: left">
                            答案选项 顺序
                        </th>
                        <th scope="col" style="text-align: left">
                            答案选项 文本
                        </th>
                        <th scope="col" style="text-align: left">
                            答案选项 Shop Labels
                        </th>
                        <th scope="col" style="text-align: left">
                            答案选项 Course Labels
                        </th>
                        <th scope="col" style="text-align: right">
                            <button onclick="location.href='/admin/question/edit-answer-option?question_id=<?= $question->getId() ?>'"
                                    type="button"
                                    class="btn btn-success btn-sm waves-effect waves-light">新建答案选项
                            </button>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
		            <?php foreach ($question->getAnswerOptions() as $answerOption): ?>
			            <?php if ($answerOption instanceof \app\models\AnswerOption): ?>
                            <tr>
                                <td>
						            <?= $answerOption->getOrder() ?>
                                </td>
                                <td>
						            <?= $answerOption->getContent() ?>
                                </td>
                                <td>
						            <?= $answerOption->getDisplayLabels('shop') ?>
                                </td>
                                <td>
						            <?= $answerOption->getDisplayLabels('course') ?>
                                </td>
                                <td style="text-align: right">
                                    <button onclick="location.href='/admin/question/edit-answer-option?id=<?= $answerOption->getId() ?>&question_id=<?= $question->getId() ?>'"
                                            type="button"
                                            class="btn btn-default btn-sm waves-effect waves-light">编辑答案选项
                                    </button>
                                    <button type="button"
                                            class="btn btn-sm btn-delete btn-danger waves-effect waves-light"
                                            href="#" data-toggle="modal"
                                            data-target="#confirm-delete"
                                            data-name="<?= $answerOption->getContent() ?>"
                                            data-href="/admin/question/delete-answer-option?id=<?= $answerOption->getId(); ?>"
                                    >Delete
                                    </button>
                                </td>
                            </tr>
			            <?php endif; ?>
		            <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</section>

<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <i class="fa fa-exclamation-triangle"></i>&nbsp;删除确认
            </div>
            <div class="modal-body">
                是否确认删除问题 <span id="confirm-name" style="color: red;"></span> ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <a class="btn btn-danger btn-ok">删除</a>
            </div>
        </div>
    </div>
</div>

<style>
    table.table-sm td, table.table-sm th {
        padding-top: .1rem;
        padding-bottom: .1rem
    }

    li {
        text-align: left;
    }
</style>
