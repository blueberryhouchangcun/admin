<?php
/* @var $question \app\models\Question */

/* @var $this yii\web\View */

use yii\web\View as View;

$script = <<< JS

$("document").ready(function() {
     $("button.btn-delete").click(function() {
       var name = $(this).attr("data-name");
       $("#confirm-name").html(name);
     });
     $('#confirm-delete').on('show.bs.modal', function(e) {
         $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
     });
});

JS;

$this->registerJs($script, View::POS_END);
?>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="/admin/index">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="#">基础数据</a>
    </li>
    <li class="breadcrumb-item active">问题系统</li>
</ol>
<section>
    <div class="row">
        <div class="col-sm-12">
            <table class="table table-sm table-striped table-bordered">
                <thead class="thead-light">
                <tr>
                    <th scope="col" style="text-align: left">
                        <button type="button" class="btn btn-sm btn-outline-info waves-effect">问题顺序</button>
                    </th>
                    <th scope="col" style="text-align: left">
                        <button type="button" class="btn btn-sm btn-outline-info waves-effect">问题内容</button>
                    </th>
                    <th scope="col" style="text-align: left">
                        <button type="button" class="btn btn-sm btn-outline-info waves-effect">问题类型</button>
                    </th>
                    <th scope="col" style="text-align: right">
                            <button onclick="location.href='/admin/question/edit'" type="button"
                                    class="btn btn-success btn-sm waves-effect waves-light">新建问题
                            </button>
                    </th>
                </tr>
                </thead>
                <tbody>
				<?php foreach ($questions as $question): ?>
                    <tr class="table-success">
                        <td style="text-align: left">
							<?= $question->getOrder() ?>
                        </td>
                        <td style="text-align: left">
							<?= $question->getContent() ?>
                        </td>
                        <td style="text-align: left">
							<?= $question->getLabelType() ?>
                        </td>
                        <td style="text-align: right">
                            <button onclick="location.href='/admin/question/edit?id=<?= $question->getId() ?>'"
                                    type="button"
                                    class="btn btn-default btn-sm waves-effect waves-light">编辑问题文本
                            </button>
                            <button type="button" class="btn btn-sm btn-delete btn-danger waves-effect waves-light"
                                    href="#" data-toggle="modal"
                                    data-target="#confirm-delete"
                                    data-name="<?= $question->getContent() ?>"
                                    data-href="/admin/question/delete?id=<?= $question->getId(); ?>"
                            >删除问题
                            </button>
                        </td>
                    </tr>
                    <tr><td colspan="4">
                        <table class="table table-sm table-striped table-bordered">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col" style="text-align: left">
                                    答案选项 顺序
                                </th>
                                <th scope="col" style="text-align: left">
                                    文本
                                </th>
                                <th scope="col" style="text-align: left">
                                    Shop Labels
                                </th>
                                <th scope="col" style="text-align: left">
                                    Course Labels
                                </th>
                                <th scope="col" style="text-align: left">
                                    禁忌食材
                                </th>
                                <th scope="col" style="text-align: right">
                                    <button onclick="location.href='/admin/question/edit-answer-option?question_id=<?= $question->getId()?>'" type="button"
                                            class="btn btn-success btn-sm waves-effect waves-light">新建答案选项
                                    </button>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
							<?php foreach ($question->getAnswerOptions() as $answerOption): ?>
								<?php if ($answerOption instanceof \app\models\AnswerOption): ?>
                                    <tr>
                                        <td>
											<code><?= $answerOption->getOrder() ?></code>
                                        </td>
                                        <td>
		                                    <?= $answerOption->getContent() ?>
                                        </td>
                                        <td>
		                                    <?= $answerOption->getDisplayLabels('shop') ?>
                                        </td>
                                        <td>
		                                    <?= $answerOption->getDisplayLabels('course') ?>
                                        </td>
                                        <td>
		                                    <?= $answerOption->getDisplayAvoidIngredients() ?>
                                        </td>
                                        <td style="text-align: right">
                                            <button onclick="location.href='/admin/question/edit-answer-option?question_id=<?= $question->getId()?>&id=<?= $answerOption->getId() ?>'"
                                                    type="button"
                                                    class="btn btn-warning btn-sm waves-effect waves-light">编辑答案选项
                                            </button>
                                            <button type="button" class="btn btn-sm btn-delete btn-danger waves-effect waves-light"
                                                    href="#" data-toggle="modal"
                                                    data-target="#confirm-delete"
                                                    data-name="<?= $answerOption->getContent() ?>"
                                                    data-href="/admin/question/delete-answer-option?id=<?= $answerOption->getId(); ?>"
                                            >删除答案选项
                                            </button>
                                        </td>
                                    </tr>
								<?php endif; ?>
							<?php endforeach; ?>
                            </tbody>
                        </table></td>
                    </tr>
				<?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

</section>

<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <i class="fa fa-exclamation-triangle"></i>&nbsp;删除确认
            </div>
            <div class="modal-body">
                是否确认删除 <span id="confirm-name" style="color: red;"></span> ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <a class="btn btn-danger btn-ok">删除</a>
            </div>
        </div>
    </div>
</div>

<style>
    table.table-sm td, table.table-sm th {
        padding-top: .1rem;
        padding-bottom: .1rem
    }

    li {
        text-align: left;
    }
</style>
