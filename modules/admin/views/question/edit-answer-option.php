<?php
/* @var $answerOption \app\models\AnswerOption */
/* @var $question \app\models\Question */

/* @var $this yii\web\View */

use yii\web\View as View;
$this->registerJsFile("/js/jquery-ui.min.js");
$this->registerJsFile("/js/jquery.tagsinput.min.js");
$this->registerCssFile('/css/jquery-ui.min.css');
$this->registerCssFile('/css/jquery.tagsinput.min.css');

$script = <<< JS
$("document").ready(function () {
    $('#avoid_ingredients').tagsInput({
        autocomplete_url:'/admin/ingredient/api'
    });
});
JS;

$this->registerJs($script, View::POS_END);

?>

<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="/admin/index">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="#">基础数据</a>
    </li>
    <li class="breadcrumb-item"><a href="/admin/question/index">问题系统</a></li>
    <li class="breadcrumb-item active">编辑 答案选项</li>
</ol>
<section style="margin-top: 50px">
    <form class="row" method="post"
          action="/admin/question/edit-answer-option?question_id=<?= $question->getId()?>&id=<?= $answerOption->getId(); ?>">
        <div class="col-sm-4">
            <div class="card">
                <div class="card-body">
                    <div class="form-header default-color">
                        <h5>
                            <i class="fa fa-pencil"></i><span><?= $question->getContent() ?></span></h5>
                    </div>
                    <div class="md-form">
                        <p style="font-size: small;color: grey">
                            <i class="fas fa-link"></i>&nbsp;问题类型:
                            <span><?= $question->getLabelType() ?></span>
                        </p>
                    </div>
                    <div class="md-form">
                        <input type="text" id="content" name="content" class="form-control"
                               value="<?= $answerOption->getContent(); ?>">
                        <label for="content">答案选项文本</label>
                    </div>
                    <div class="md-form">
                        <input type="text" id="order" name="order" class="form-control"
                               value="<?= $answerOption->getOrder(); ?>">
                        <label for="order">答案选项顺序(请填写整数)</label>
                    </div>
                    <div class="md-form">
                        <p style="font-size: small;color: grey">禁忌食材</p>
                        <input type="text" id="avoid_ingredients" name="avoid_ingredients" class="form-control"
                               value="<?= $answerOption->getDisplayAvoidIngredients(); ?>">
                    </div>
                    <div class="text-center">
                        <button class="btn btn-success waves-effect waves-light">保存</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="card">
                <div class="card-body">
                    <div class="form-header info-color">
                        <h5>店铺标签</h5>
                    </div>
                    <div>
						<?= \app\models\Label::getShopCheckBoxOptions($answerOption->getShopLabels()) ?>
                    </div>
                    <div class="text-center">
                        <button class="btn btn-success waves-effect waves-light">保存</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12" style="margin-top: 50px">
            <div class="card">
                <div class="card-body">
                    <div class="form-header info-color">
                        <h5>菜品标签</h5>
                    </div>
                    <div>
					    <?= \app\models\Label::getCourseCheckBoxOptions($answerOption->getCourseLabels()) ?>
                    </div>
                    <div class="text-center">
                        <button class="btn btn-success waves-effect waves-light">保存</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>
<style>
    [type=checkbox] + label, [type=radio] + label {
        padding-left: 22px;
    }
</style>