<?php
/* @var $user \app\models\OauthUsers */

/* @var $this yii\web\View */

use yii\web\View as View;

$script = <<< JS

JS;

$this->registerJs($script, View::POS_END);
?>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="/admin/index">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="#">用户管理</a>
    </li>
    <li class="breadcrumb-item active">标注统计</li>
</ol>
<?php if ($progressTotal != 0): ?>
    <section>
        <div class="progress d-flex" style="height: 20px;background-color: #eee">
			<?php foreach ($progress as $status => $num):
				$percentage = (100 * $num / $progressTotal); ?>
                <div class="progress-bar <?= $progressColor[$status]?>" role="progressbar" style="width: <?= $percentage ?>%; height: 20px"
                     aria-valuenow="<?= $num ?>" aria-valuemin="0" aria-valuemax="100"></div>
			<?php endforeach; ?>
        </div>
    </section>
<?php endif; ?>
<section>
    <div class="row">
        <div class="col-sm-12">
            <table class="table table-striped table-bordered">
                <thead class="thead-light">
                <tr>
                    <th scope="col" style="text-align: center">
                        <button type="button" class="btn btn-sm btn-outline-info waves-effect">Username</button>
                    </th>
                    <th scope="col" style="text-align: center">
                        <button type="button" class="btn btn-sm btn-outline-info waves-effect">完成编辑(manual_edited)
                        </button>
                    </th>
                    <th scope="col" style="text-align: center">
                        <button type="button" class="btn btn-sm btn-outline-info waves-effect">编辑中(locked)</button>
                    </th>
                    <th scope="col" style="text-align: center">
                        <button type="button" class="btn btn-sm btn-outline-info waves-effect">待优化(review)</button>
                    </th>
                </tr>
                </thead>
                <tbody>
				<?php foreach ($editors as $editor => $cnt): ?>
                    <tr>
                        <td style="text-align: center">
							<?= $editor ?>
                        </td>
                        <td style="text-align: center">
                            <a target="_blank"
                               href="/admin/course/index?status=manual_edited&editor=<?= $editor ?>"><code
                                        class="text-success"><?= $cnt['manual_edited'] ?></code></a>
                        </td>
                        <td style="text-align: center">
                            <a target="_blank" href="/admin/course/index?status=locked&editor=<?= $editor ?>"><code
                                        class="text-danger"><?= $cnt['locked'] ?></code></a>
                        </td>
                        <td style="text-align: center">
                            <a target="_blank" href="/admin/course/index?status=review&editor=<?= $editor ?>"><code
                                        class="text-info"><?= $cnt['review'] ?></code></a>
                        </td>

                    </tr>
				<?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

</section>


<style>
    li {
        text-align: left;
    }
    .progress .progress-bar , .bg-custom-grey {
        background-color: lightgray; !important;
    }
</style>