<?php

namespace app\modules\admin\controllers;

use app\models\Ingredients;
use app\models\Courses;
use app\models\SubLabels;
use app\models\what2eat\ForHereCourse;
use app\models\what2eat\ForHereShop;
use Yii;
use yii\data\Pagination;
use yii\web\Response;

class ForHereShopController extends BaseController
{


	public function actionIndex()
	{
		$status = Yii::$app->request->get("status", "");
		$query = ForHereShop::find()->where(['status' => $status, 'region' => '清河'])->orderBy('comment_quantity desc');
		$countQuery = clone $query;
		$pages = new Pagination(['totalCount' => $countQuery->count()]);
		$shops = $query->offset($pages->offset)
			->limit($pages->limit)
			->all();
		return $this->render('/for-here-shop/index', [
			'shops' => $shops,
			'pages' => $pages,
			'status' => $status
		]);
	}

	public function actionEdit()
	{
		$request = Yii::$app->request;
		$id = $request->get('id');
		$status = $request->get('status');
		$name = $request->post('name', '');
		$recommend_reason = $request->post('recommend_reason', '');
		$labels = $request->post('shop_labels', []);
		if ($id) {
			$shop = ForHereShop::findOne(['id' => $id]);
		} else {
			$shop = new ForHereShop();
		}
		if ($name) {
			$shop->setName($name);
			$shop->setRecommendReason($recommend_reason);
			$shop->setEditor($this->userName);
			$shop->setLabels(implode("#", $labels));

			/*********   临时方案   *********/
			$categories = [];
			foreach (array_keys(SubLabels::$combination_attribution) as $categoryId) {
				if (in_array($categoryId, $labels)) {
					$categories[] = $categoryId;
					continue;
				}
			}
			$shop->setCategory(implode("#", $categories));

			if ($shop->getId()) {
				$shop->setUpdateTime(time());
			} else {
				$shop->setCreateTime(time());
			}
			$shop->setStatus(ForHereShop::STATUS_MANUAL_EDITED);
			$shop->save(false);
			return $this->redirect(['/admin/for-here-shop/index?status=' . $status]);
		}
		return $this->render('/for-here-shop/edit', [
			'shop' => $shop,
			'status' => $status
		]);
	}

	public function actionDelete()
	{
		$request = Yii::$app->request;
		$id = $request->get('id');
		$status = $request->get('status');
		$shop = ForHereShop::findOne(['id' => $id]);
		$courses = ForHereCourse::find()->where(['shop_id'=>$id])->all();
		foreach ($courses as $course){
			if ($course instanceof ForHereCourse){
				$course->setStatus(ForHereCourse::STATUS_DELETED);
				$course->save(false);
			}
		}
		$shop->setStatus(ForHereShop::STATUS_DELETED);
		$shop->setUpdateTime(time());
		$shop->setEditor($this->userName);
		$shop->save(false);
		return $this->redirect(['/admin/for-here-shop/index?status=' . $status]);
	}
}