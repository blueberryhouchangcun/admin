<?php

namespace app\modules\admin\controllers;

use app\models\OauthUsers;
use app\models\Question;
use Ramsey\Uuid\Uuid;
use Yii;

class AnalysisController extends BaseController
{
	public function actionLabel()
	{
		$sql = "
			select editor,count(1) as cnt,status from courses
			where status != 'imported' and editor is not null
			group by editor,status
		";
		$result = Yii::$app->db->createCommand($sql)->queryAll();
		$sqlProgress = "
			select count(1) as cnt,status from courses 
			where avg_recommend_count > 1 group by status
			ORDER BY FIELD(status,'manual_edited','locked','review','imported')
		";
		$resultProgress = Yii::$app->db->createCommand($sqlProgress)->queryAll();
		$editors = [];
		$progress = [];
		$progressTotal = 0;
		$progressColor = [
			'imported' => 'bg-custom-grey',
			'review' => 'bg-info progress-bar-striped progress-bar-animated',
			'manual_edited' => 'bg-success progress-bar-striped progress-bar-animated',
			'locked' => 'bg-danger progress-bar-striped progress-bar-animated',
		];
		foreach ($result as $item) {
			$editors[$item['editor']][$item['status']] = $item['cnt'];
		}
		foreach ($resultProgress as $item) {
			$progress[$item['status']] = $item['cnt'];
			$progressTotal += $item['cnt'];
		}
		return $this->render('/analysis/label', [
			'editors' => $editors,
			'progress' => $progress,
			'progressTotal' => $progressTotal,
			'progressColor' => $progressColor,
		]);
	}


}