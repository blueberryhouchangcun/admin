<?php

namespace app\modules\admin\controllers;


use app\models\OauthUsers;
use filsh\yii2\oauth2server\filters\auth\CompositeAuth;
use filsh\yii2\oauth2server\filters\ErrorToExceptionFilter;
use Yii;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\Response;

class BaseController extends Controller
{
	protected $userId;
	protected $userName;
	protected $userRole;

	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow' => true,
						'roles' => ['admin'],
					],
				],
			],
		];
	}

	public function init()
	{
		$this->userId = Yii::$app->user->identity->id;
		$this->userName = Yii::$app->user->identity->username;
		$authManager = Yii::$app->getAuthManager();
		$this->userRole = array_keys($authManager->getAssignments(Yii::$app->user->identity->id))[0];
		parent::init();
	}

}