<?php

namespace app\modules\admin\controllers;

use app\models\Courses;
use app\models\what2eat\ForHereShop;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;

class CourseController extends BaseController
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow' => true,
						'roles' => ['admin', 'label'],
					],
				],
			],
		];
	}

	public function actionIndex()
	{
		$status = Yii::$app->request->get("status", "");
		$check = Yii::$app->request->get("check", "");
		$editor = Yii::$app->request->get("editor", "");
		$query = Courses::find()
			->leftJoin('for_here_course', 'courses.id=for_here_course.course_id')
			->leftJoin('for_here_shop', 'for_here_course.shop_id=for_here_shop.id')
			->where(['courses.status' => $status])
			->andWhere(['not in', 'for_here_shop.original_category', ['水果生鲜', '自助餐', '面包甜点']])
			->andWhere(['>', 'avg_recommend_count', 1]);
		if ($this->userRole != 'admin' ) {
			if ($status != Courses::STATUS_IMPORTED){
				$query->andWhere(['courses.editor' => $this->userName]);
			}
		} else {
			if ($editor) {
				$query->andWhere(['courses.editor' => $editor]);
			}
		}
		if ($check) {
			$query->andWhere(['check' => $check]);
		}
		$query->orderBy('courses.update_time desc, for_here_shop.comment_quantity desc,avg_recommend_count desc');
		$countQuery = clone $query;
		$pages = new Pagination(['totalCount' => $countQuery->count()]);
		$courses = $query->offset($pages->offset)
			->limit($pages->limit)
			->all();
		return $this->render('/course/index', [
			'courses' => $courses,
			'pages' => $pages,
			'userRole' => $this->userRole,
			'status' => $status
		]);
	}

	public function actionEdit()
	{
		$request = Yii::$app->request;
		$id = $request->get('id');
		$status = $request->get('status');
		$course_name = $request->post('course_name', '');
		$p_ingredients = $request->post('principal_ingredients', '');
		$c_ingredients = $request->post('complementary_ingredients', '');
		$labels = $request->post('course_labels', []);
		if ($id) {
			$course = Courses::findOne(['id' => $id]);
		} else {
			$course = new Courses();
		}
		if ($course_name) {
			/******审核相关******/
			$check_invalid = $request->post("check_invalid");
			if (isset($check_invalid)) {
				$course->setCheck(Courses::CHECK_INVALID);
				$course->setCheckReason($request->post('check_reason'));
				$course->save(false);
				return $this->redirect(['/admin/course/index?status=' . $status . '&check=1']);
			}
			$check_valid = $request->post("check_valid");
			if (isset($check_valid)) {
				$course->setCheck(Courses::CHECK_VALID);
				$course->save(false);
				return $this->redirect(['/admin/course/index?status=' . $status . '&check=3']);
			}
			/****审核相关END*****/

			$course->setName($course_name);
			$course->setPrincipalIngredients($p_ingredients);
			$course->setComplementaryIngredients($c_ingredients);
			$course->setLabels(implode("#", $labels));
			$course->setEditor($this->userName);
			if ($course->getId()) {
				$course->setUpdateTime(time());
			} else {
				$course->setCreateTime(time());
			}
			$review = $request->post("review");
			if (isset($review)) {
				$course->setStatus(Courses::STATUS_REVIEW);
			} else {
				if ($course->getCheck() == Courses::CHECK_INVALID) {
					$course->setCheck(Courses::CHECK_REVIEW);
				}
				$course->setStatus(Courses::STATUS_MANUAL_EDITED);
			}
			$course->save(false);
			return $this->redirect(['/admin/course/index?status=' . $status]);
		}
		$categories = [];
		foreach ($course->getForHereShops() as $shop) {
			if ($shop instanceof ForHereShop) {
				if ($shop->getDisplayCategory()) {
					foreach (explode("#", $shop->getDisplayCategory()) as $item) {
						$categories[] = $item;
					}
				}
			}
		}
		return $this->render('/course/edit', [
			'course' => $course,
			'status' => $status,
			'categories' => $categories,
			'userRole' => $this->userRole,
		]);
	}


	public function actionAutoLoopEdit()
	{
		$request = Yii::$app->request;
		$id = $request->post('id');
		$course_name = $request->post('course_name', '');
		$p_ingredients = $request->post('principal_ingredients', '');
		$c_ingredients = $request->post('complementary_ingredients', '');
		$labels = $request->post('course_labels', []);
		$course = Courses::getNext();
		$categories = [];
		if ($course instanceof Courses) {
			if ($request->isGet) {
				$course->setStatus(Courses::STATUS_LOCKED);
				$course->setUpdateTime(time());
				$course->setEditor($this->userName);
				$course->save(false);
				foreach ($course->getForHereShops() as $shop) {
					if ($shop instanceof ForHereShop) {
						if ($shop->getDisplayCategory()) {
							foreach (explode("#", $shop->getDisplayCategory()) as $item) {
								$categories[] = $item;
							}
						}
					}
				}
			}
			if ($id && $course_name) {
				$coursePost = Courses::findOne(['id' => $id]);
				if ($coursePost instanceof Courses) {
					$coursePost->setName($course_name);
					$coursePost->setPrincipalIngredients($p_ingredients);
					$coursePost->setComplementaryIngredients($c_ingredients);
					$coursePost->setLabels(implode("#", $labels));
					$coursePost->setEditor($this->userName);
					$coursePost->setUpdateTime(time());
					$review = $request->post("review");
					if (isset($review)) {
						$coursePost->setStatus(Courses::STATUS_REVIEW);
					} else {
						$coursePost->setStatus(Courses::STATUS_MANUAL_EDITED);
					}
					$coursePost->save(false);
				}
				return $this->redirect(['/admin/course/auto-loop-edit']);
			}
		}
		return $this->render('/course/auto-loop-edit', [
			'course' => $course,
			'categories' => $categories,
		]);
	}

	public function actionDelete()
	{
		$request = Yii::$app->request;
		$id = $request->get('id');
		$course = Courses::findOne(['id' => $id]);
		$course->setStatus(Courses::STATUS_DELETED);
		$course->setEditor($this->userName);
		$course->setUpdateTime(time());
		$course->save(false);
		return $this->redirect(['/admin/course/index']);
	}

}