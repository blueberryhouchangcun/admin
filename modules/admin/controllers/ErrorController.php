<?php

namespace app\modules\admin\controllers;

use app\models\Courses;
use Yii;

class ErrorController extends BaseController
{

	public function actionIndex()
	{
		$exception = Yii::$app->errorHandler->exception;
		if ($exception !== null) {
			return $this->render('index', ['exception' => $exception]);
		}
	}
}