<?php

namespace app\modules\admin\controllers;

use app\models\Ingredients;
use app\models\Courses;
use app\models\IngredientsAlias;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\Response;

class IngredientController extends BaseController
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow' => true,
						'roles' => ['admin', 'label'],
					],
				],
			],
		];
	}

	public function actionIndex()
	{
		$query = Ingredients::find()->where(['status' => [Ingredients::STATUS_AVAILABLE]]);
		$countQuery = clone $query;
		$pages = new Pagination(['totalCount' => $countQuery->count()]);
		$ingredients = $query->offset($pages->offset)
			->limit($pages->limit)
			->all();
		return $this->render('/ingredient/index', [
			'ingredients' => $ingredients,
			'pages' => $pages
		]);
	}


	public function actionEdit()
	{
		$request = Yii::$app->request;
		$id = $request->get('id');
		$ingredient_name = $request->post('ingredient_name', '');
		$alias = $request->post('alias', '');
		$function = $request->post('function', '');
		if ($id) {
			$ingredient = Ingredients::findOne(['id' => $id]);
		} else {
			$ingredient = new Ingredients();
		}
		if ($ingredient_name) {
			$exist = Ingredients::findOne(['name' => $ingredient_name]);
			if ($exist instanceof Ingredients) {
				return $this->render('/ingredient/edit', [
					'ingredient' => $ingredient,
					'error' => "食材【" . $ingredient_name . "】已存在"
				]);
			}
			$existAlias = IngredientsAlias::findOne(['alias' => $ingredient_name]);
			if ($existAlias instanceof IngredientsAlias) {
				$ingredientExistName = Ingredients::findOne(['id' => $existAlias->getIngredientId()])->getName();
				return $this->render('/ingredient/edit', [
					'ingredient' => $ingredient,
					'error' => "该食材名称【" . $ingredient_name . "】为食材【" . $ingredientExistName . "】的别名"
				]);
			}
			$ingredient->setName($ingredient_name);
			$ingredient->setAlias($alias);
			$ingredient->setFunction($function);
			$ingredient->setEditor($this->userName);
			if ($ingredient->getId()) {
				$ingredient->setUpdateTime(time());
			} else {
				$ingredient->setCreateTime(time());
			}
			$ingredient->setStatus(Ingredients::STATUS_AVAILABLE);
			$ingredient->save(false);
			return $this->redirect(['/admin/ingredient/index']);
		}
		return $this->render('/ingredient/edit', [
			'ingredient' => $ingredient,
		]);
	}

	public function actionDelete()
	{
		$request = Yii::$app->request;
		$id = $request->get('id');
		$ingredient = Ingredients::findOne(['id' => $id]);
		if ($ingredient instanceof Ingredients) {
			$ingredient->setStatus(Ingredients::STATUS_DELETE);
			$ingredient->setUpdateTime(time());
			$ingredient->setEditor($this->userName);
			$ingredient->save(false);
		}
		return $this->redirect(['/admin/ingredient/index']);
	}

	public function actionApi()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;
		$request = Yii::$app->request;
		$term = $request->get('term');
		$return = [];
		$ingredients = Ingredients::find()->andFilterWhere(['like', 'name', $term])->all();
		foreach ($ingredients as $ingredient) {
			if ($ingredient instanceof Ingredients) {
				$return[] = $ingredient->getName();
			}
		}
		$alias = IngredientsAlias::find()->andFilterWhere(['like', 'alias', $term])->all();
		foreach ($alias as $a) {
			if ($a instanceof IngredientsAlias) {
				if (!in_array($a->getAlias(), $return)) {
					$return[] = $a->getAlias();
				}
			}
		}
		return $return;
	}

	public function actionGetFunction()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;
		$request = Yii::$app->request;
		$term = $request->get('term');
		$ingredientsString = explode(",", $term);
		$return = [];
		foreach ($ingredientsString as $item) {
			$ingredient = Ingredients::findOne(['name' => $item, 'status' => Ingredients::STATUS_AVAILABLE]);
			if (null === $ingredient) {
				$alias = IngredientsAlias::findOne(['alias' => $item]);
				if ($alias instanceof IngredientsAlias) {
					$ingredient = Ingredients::findOne(['id' => $alias->getIngredientId()]);
				}
			}
			if ($ingredient instanceof Ingredients) {
				$return[] = [
					'ingredient' => "<span class='badge badge-success'>" . $ingredient->getName() . "</span>",
					'function' => "<span class='badge badge-light-blue'>" . implode("</span>&nbsp;&nbsp;<span class='badge badge-light-blue'>", $ingredient->getFunctionArray()) . "</span>"
				];
			}
		}

		return $return;
	}
}