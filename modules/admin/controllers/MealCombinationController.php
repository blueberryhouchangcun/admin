<?php

namespace app\modules\admin\controllers;

use app\models\Courses;
use app\models\Label;
use app\models\MealCombination;
use app\models\SubLabels;
use Ramsey\Uuid\Uuid;
use Yii;
use yii\data\Pagination;
use yii\web\Response;

class MealCombinationController extends BaseController
{

	public function actionIndex()
	{
		$query = MealCombination::find()->where(['status' => MealCombination::STATUS_AVAILABLE]);
		$countQuery = clone $query;
		$pages = new Pagination(['totalCount' => $countQuery->count()]);
		$combinations = $query->offset($pages->offset)
			->limit($pages->limit)
			->all();
		return $this->render('/meal-combination/index', [
			'combinations' => $combinations,
			'pages' => $pages,
		]);
	}

	public function actionEdit()
	{
		$request = Yii::$app->request;
		$id = $request->get('id', '');
		$numberOfDinners = $request->post('number_of_dinners', '');
		$category = $request->post('category', '');
		$json = $request->post('combination_json', '');
		$array = json_decode($json, true);
		if ($id) {
			$combination = MealCombination::findOne(['id' => $id]);
		} else {
			$combination = new MealCombination();
		}
		if ($category && $numberOfDinners && $array) {
			$combination->setCategory($category);
			$combination->setNumberOfDinners(intval($numberOfDinners));
			foreach ($array as $index => $item) {
				$array[$index]['food_component'] = SubLabels::labelString2Id($item['food_component']);
			}
			$combination->setCombinationJson(json_encode($array));
			$combination->setEditor($this->userName);
			$combination->setUpdateTime(time());
			$combination->setStatus(MealCombination::STATUS_AVAILABLE);
			$combination->save(false);
			return $this->redirect(['/admin/meal-combination/index']);
		}
		return $this->render('/meal-combination/edit', [
			'combination' => $combination,
		]);

	}

	public function actionDelete()
	{
		$request = Yii::$app->request;
		$id = $request->get('id');
		$combination = MealCombination::findOne(['id' => $id]);
		$combination->setStatus(MealCombination::STATUS_DELETE);
		$combination->setEditor($this->userName);
		$combination->setUpdateTime(time());
		$combination->save(false);
		return $this->redirect(['/admin/meal-combination/index']);
	}

	public function actionGetTips()
	{
		$return = "";
		$request = Yii::$app->request;
		$id = $request->get('id');
		$map_id = intval(SubLabels::$combination_attribution[$id]);
		$children = Label::getLeafChildren($map_id);
		foreach ($children as $child) {
			if ($child instanceof Label) {
				$name = $child->getName();
				$return .= "<span class='badge badge-light-blue'>$name</span>&nbsp;&nbsp;";
			}
		}
		return $return;
	}
}