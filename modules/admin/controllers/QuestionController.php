<?php

namespace app\modules\admin\controllers;

use app\models\Answer;
use app\models\AnswerMap;
use app\models\AnswerOption;
use app\models\Question;
use Ramsey\Uuid\Uuid;
use Yii;
use yii\web\NotFoundHttpException;

class QuestionController extends BaseController
{

	public function actionIndex()
	{
		$questions = Question::find()
			->where(['status' => Question::STATUS_AVAILABLE])
			->orderBy('order')->all();
		return $this->render('/question/index', [
			'questions' => $questions,
		]);
	}

	public function actionEdit()
	{

		$request = Yii::$app->request;
		$id = $request->get('id', '');
		$content = $request->post('content', '');
		$order = $request->post('order', '');
		$type = $request->post('type', '');
		if ($id) {
			$question = Question::findOne(['id' => $id]);
		} else {
			$question = new Question();
		}
		if ($content && $order) {
			if (!$question->getId()) {
				$question->setId(Uuid::uuid4());
				$question->setCreateTime(time());
			} else {
				$question->setUpdateTime(time());
			}
			$question->setEditor($this->userName);
			$question->setStatus(Question::STATUS_AVAILABLE);
			$question->setOrder(intval($order));
			$question->setContent($content);
			$question->setType($type);
			$question->save(false);
			return $this->redirect(['/admin/question/index']);
		}
		return $this->render('/question/edit', [
			'question' => $question,
		]);
	}

	public function actionEditAnswerOption()
	{

		$request = Yii::$app->request;
		$question_id = $request->get('question_id', '');
		$question = Question::findOne(['id' => $question_id, 'status' => Question::STATUS_AVAILABLE]);
		if ($question === null) {
			return $this->redirect(['/admin/question/index']);
		}
		$id = $request->get('id', '');
		$content = $request->post('content', '');
		$course_labels = $request->post('course_labels', []);
		$shop_labels = $request->post('shop_labels', []);
		$order = $request->post('order', '');
		$avoid_ingredients = $request->post('avoid_ingredients', '');

		if ($id) {
			$answerOption = AnswerOption::findOne(['id' => $id]);
		} else {
			$answerOption = new AnswerOption();
		}
		if ($content && $order) {
			$answerOption->setId(Uuid::uuid4());
			$answerOption->setOrder(intval($order));
			$answerOption->setQuestionId($question->getId());
			$answerOption->setShopLabels(implode("#", $shop_labels));
			$answerOption->setCourseLabels(implode("#", $course_labels));
			$answerOption->setAvoidIngredients($avoid_ingredients);
			$answerOption->setStatus(AnswerOption::STATUS_AVAILABLE);
			$answerOption->setEditor($this->userName);
			$answerOption->setContent($content);
			if ($answerOption->getId()) {
				$answerOption->setCreateTime(time());
			} else {
				$answerOption->setUpdateTime(time());
			}
			$answerOption->save(false);
			return $this->redirect(['/admin/question/index']);
		}
		return $this->render('/question/edit-answer-option', [
			'question' => $question,
			'answerOption' => $answerOption,
		]);
	}


	public function actionDelete()
	{
		$request = Yii::$app->request;
		$id = $request->get('id');
		$question = Question::findOne(['id' => $id]);
		$question->setStatus(Question::STATUS_DELETE);
		$question->setEditor($this->userName);
		$question->setUpdateTime(time());
		$question->save(false);
		foreach ($question->getAnswerOptions() as $answerOption) {
			if ($answerOption instanceof AnswerOption) {
				$answerOption->setStatus(AnswerOption::STATUS_DELETE);
				$answerOption->setEditor($this->userName);
				$answerOption->setUpdateTime(time());
				$answerOption->save(false);
			}
		}
		return $this->redirect(['/admin/question/index']);
	}

	public function actionDeleteAnswerOption()
	{
		$request = Yii::$app->request;
		$id = $request->get('id');
		$option = AnswerOption::findOne(['id' => $id]);
		$option->setStatus(AnswerOption::STATUS_DELETE);
		$option->setEditor($this->userName);
		$option->setUpdateTime(time());
		$option->save(false);
		return $this->redirect(['/admin/question/index']);
	}


}