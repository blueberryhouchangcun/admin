<?php

namespace app\modules\admin\controllers;

use app\models\OauthUsers;
use app\models\Question;
use Ramsey\Uuid\Uuid;
use Yii;

class UserController extends BaseController
{
	public function actionIndex()
	{
		$users = OauthUsers::find()->orderBy('create_time')->all();
		return $this->render('/user/index', [
			'users' => $users,
		]);
	}

	public function actionEdit()
	{
		$request = Yii::$app->request;
		$id = $request->get('id');
		$username = $request->post('username', '');
		$password = $request->post('password', '');
		$gender = $request->post('gender', '');
		$birth_year = $request->post('birth_year', '');
		$height = $request->post('height', '');
		$weight = $request->post('weight', '');
		$first_name = $request->post('first_name', '');
		$last_name = $request->post('last_name', '');
		$tip = $request->post('tip', '');
		if ($id) {
			$user = OauthUsers::findOne(['id' => $id]);
		} else {
			$user = new OauthUsers();
		}
		if ($username && $password) {
			$user->setUsername($username);
			$user->setPassword($password);
			$user->setGender($gender);
			$user->setFirstName($first_name);
			$user->setLastName($last_name);
			$user->setTip($tip);
			$user->setStatus(OauthUsers::STATUS_REGISTERED);
			$user->setBirthYear(intval($birth_year));
			$user->setHeight(intval($height));
			$user->setWeight(intval($weight));
			if ($user->getId()) {
				$user->setUpdateTime(time());
			} else {
				$user->setId(Uuid::uuid4());
				$user->setCreateTime(time());
			}
			$user->setEditor($this->userName);
			$user->save(false);
			return $this->redirect(['/admin/user/index']);
		}
		$questions = Question::find()->all();
		return $this->render('/user/edit', [
			'user' => $user,
			'questions' => $questions
		]);
	}

	public function actionDelete()
	{
		$request = Yii::$app->request;
		$id = $request->get('id');
		$user = OauthUsers::findOne(['id' => $id]);
		$user->setStatus(OauthUsers::STATUS_DELETED);
		$user->setUpdateTime(time());
		$user->setEditor($this->userName);
		$user->save(false);
		return $this->redirect(['/admin/user/index']);
	}

}