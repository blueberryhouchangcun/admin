<?php

namespace app\modules\admin\controllers;

use app\models\Label;
use app\services\JsonConverter;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class LabelController extends BaseController
{

	public function actionIndex()
	{
		$request = Yii::$app->request;
		$type = $request->get("type");
		$labels = Label::find()->where(['status' => Label::STATUS_AVAILABLE, 'type' => $type])->all();
		$labelsArray = json_decode(JsonConverter::array2json(ArrayHelper::toArray($labels)), true);
		$order = [];
		$data = [];
		array_walk_recursive($labelsArray, function ($item, $key) use (&$order, &$data) {
			if ($key == 'id') {
				$order[] = $item;
			}
		});

//		foreach ($labels as $label) {
//			if ($label instanceof Label) {
//				if ($label->getLevel() == 0) {
//					$data[] = [
//						'id' => $label->getId(),
//						'topic' => $label->getName(),
//						'isroot' => true,
//					];
//				} else {
//					$data[] = [
//						'id' => $label->getId(),
//						'topic' => $label->getName(),
//						'parentid' => $label->getParentId(),
//					];
//				}
//			}
//
//		}
		$order = implode(",", $order);
		$labelsResult = Label::find()->where(['status' => Label::STATUS_AVAILABLE, 'type' => $type])->orderBy([new Expression('FIELD(id,' . $order . ')')])->all();
		return $this->render('/label/index', [
			'labels' => $labelsResult,
			'data' => json_encode($data),
			'type' => $type
		]);
	}

	public function actionAppend()
	{
		$request = Yii::$app->request;
		$parent_id = $request->get('parent_id');
		$label = new Label();
		$label_name = $request->post('label_name', '');
		$parent_label = Label::findOne(['id' => $parent_id]);
		if ($parent_id && $label_name) {
			$label->setName($label_name);
			$label->setParentId($parent_id);
			$label->setLevel($label->getLevel());
			$label->setStatus(Label::STATUS_AVAILABLE);
			$label->setCreateTime(time());
			$label->setType($parent_label->getType());
			$label->setEditor($this->userName);
			$label->save(false);
			return $this->redirect(['/admin/label/index?type=' . $parent_label->getType()]);
		}
		return $this->render('/label/append', [
			'parent_label' => $parent_label
		]);
	}

	public function actionEdit()
	{
		$request = Yii::$app->request;
		$id = $request->get('id');
		$label_name = $request->post('label_name', '');
		$label = Label::findOne(['id' => $id]);
		if ($label_name) {
			$label->setName($label_name);
			$label->setUpdateTime(time());
			$label->setEditor($this->userName);
			$label->save(false);
			return $this->redirect(['/admin/label/index?type=' . $label->getType()]);
		}
		return $this->render('/label/edit', [
			'label' => $label
		]);
	}

	public function actionDelete()
	{
		$request = Yii::$app->request;
		$id = $request->get('id');
		$label = Label::findOne(['id' => $id]);
		$label->setStatus(Label::STATUS_DELETE);
		$label->setUpdateTime(time());
		$label->setEditor($this->userName);
		$label->save(false);
		return $this->redirect(['/admin/label/index?type=course']);
	}

}