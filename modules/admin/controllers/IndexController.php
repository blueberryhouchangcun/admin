<?php

namespace app\modules\admin\controllers;


use yii\filters\AccessControl;

class IndexController extends BaseController
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow' => true,
						'roles' => ['admin', 'label'],
					],
				],
			],
		];
	}

	public function actionIndex()
	{
		return $this->render('/index/index', [
			'userRole' => $this->userRole
		]);
	}
}