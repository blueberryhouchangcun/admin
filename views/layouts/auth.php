<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>What2eat</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <!-- What2eat -->
    <link href="/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="/css/style.css" rel="stylesheet">
	<?php $this->head() ?>
</head>

<body class="fixed-sn light-blue-skin">
<?php $this->beginBody() ?>
<!--Main Navigation-->
<header>

    <!--Navbar-->
    <nav class="navbar navbar-expand-lg navbar-dark double-nav  fixed-top scrolling-navbar">


        <!-- Breadcrumb-->
        <div class="breadcrumb-dn mr-auto">
            <p><a href="/admin/index">What2eat Everyday 🥕</a></p>
        </div>

        <!-- Links -->
    </nav>
    <!--/.Navbar-->


</header>

<!--Main layout-->
<main>
    <div class="container-fluid">
		<?= $content;?>
    </div>
</main>
<!--Main layout-->

<!--Footer-->
<footer class="page-footer center-on-small-only">

    <!--Copyright-->
    <div class="footer-copyright">
        <div class="container-fluid">
            © <?= date('Y')?> Copyright
            <a href="https://www.what2eateveryday.com.com"> What2eateveryday.com </a> | Powered by genius from Houchangcun

        </div>
    </div>
    <!--/.Copyright-->

</footer>
<!--/.Footer-->

<!-- SCRIPTS -->
<!-- JQuery -->
<script type="text/javascript" src="/js/jquery-3.2.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="/js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="/js/mdb.min.js"></script>

<!-- Initializations -->
<script>
    // SideNav Initialization
    $(".button-collapse").sideNav();

    // Material Select Initialization
    $(document).ready(function () {
        $('.mdb-select').material_select();
    });

    // Data Picker Initialization
    $('.datepicker').pickadate();

    // Tooltip Initialization
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>

<?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>
