<?php
/* @var $this yii\web\View */

use yii\web\View as View;

$script = <<< JS
$("document").ready(function() {
});
JS;

$this->registerJs($script, View::POS_END);
?>
<section>
    <div class="row">
        <div class="col-sm-3"></div>
        <div class="col-md-6 mb-4">
            <!--Card-->
            <div class="card testimonial-card">

                <!--Bacground color-->
                <div class="card-up purple-gradient">
                </div>

                <!--Avatar-->
                <div class="avatar mx-auto white">
                    <img src="/image/timg.jpeg" class="rounded-circle">
                </div>

                <div class="card-body">
                    <!--Quotation-->
                    <form method="post" action="/user/login">
                        <div class="card-body">
                            <!--Body-->
                            <div class="md-form">
                                <input type="text" id="login-form-login" class="form-control" name="login-form[login]">
                                <label for="login-form-login">Your username</label>
                            </div>

                            <div class="md-form">
                                <input type="password" id="login-form-password" class="form-control"
                                       name="login-form[password]">
                                <label for="login-form-password">Your password</label>
                            </div>

                            <div class="text-center">
                                <button type="submit" class="btn btn-info waves-effect waves-light">Login</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</section>
