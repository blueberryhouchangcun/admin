<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;
/**
 * This is the model class for table "{{%ingredients_alias}}".
 *
 * @property integer $id
 * @property string  $alias
 * @property integer  $ingredient_id
 */
class IngredientsAlias extends ActiveRecord
{
	public static function getDb() {
		return Yii::$app->get('what2eat');
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getAlias()
	{
		return $this->alias;
	}

	/**
	 * @param string $alias
	 */
	public function setAlias($alias)
	{
		$this->alias = $alias;
	}

	/**
	 * @return int
	 */
	public function getIngredientId()
	{
		return $this->ingredient_id;
	}

	/**
	 * @param int $ingredient_id
	 */
	public function setIngredientId($ingredient_id)
	{
		$this->ingredient_id = $ingredient_id;
	}

}
