<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "{{%ingredients}}".
 *
 * @property integer $id
 * @property string  $name
 * @property string  $alias
 * @property string  $editor
 * @property string  $function
 * @property integer $create_time
 * @property integer $update_time
 * @property string  $status
 * @property string  $source
 */
class Ingredients extends ActiveRecord
{
	const STATUS_DELETE = 0;
	const STATUS_AVAILABLE = 1;

	public static function getDb()
	{
		return Yii::$app->get('what2eat');
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getAlias()
	{
		return $this->alias;
	}

	public function getDisplayAlias()
	{
		$return = [];
		if ($this->alias) {
			$array = explode("#", $this->alias);
			foreach ($array as $item) {
				$alias = IngredientsAlias::findOne(['id' => $item]);
				if ($alias instanceof IngredientsAlias) {
					$return[] = $alias->getAlias();
				}
			}
		}
		return implode("#", $return);
	}

	public function getDisplayFunction()
	{
		$return = [];
		if ($this->function) {
			$array = explode("#", $this->function);
			foreach ($array as $item) {
				$function = IngredientsFunction::findOne(['id' => $item]);
				if ($function instanceof IngredientsFunction) {
					$return[] = $function->getFunction();
				}
			}
		}
		return implode("#", $return);
	}

	/**
	 * @param string $alias
	 */
	public function setAlias($alias)
	{
		$array = explode("#", $alias);
		$return = [];
		if ($array) {
			foreach ($array as $item) {
				if (!is_int($item)) {
					$alias = IngredientsAlias::findOne(['alias' => $item]);
					if (!($alias instanceof IngredientsAlias)) {
						$alias = new IngredientsAlias();
						$alias->setIngredientId($this->id);
						$alias->setAlias($item);
						$alias->save(false);
					}
					$return[] = $alias->getId();
				} else {
					$return[] = $item;
				}
			}
		}
		$this->alias = implode("#", $return);
	}

	/**
	 * @return string
	 */
	public function getEditor()
	{
		return $this->editor;
	}

	/**
	 * @param string $editor
	 */
	public function setEditor($editor)
	{
		$this->editor = $editor;
	}

	/**
	 * @return string
	 */
	public function getFunction()
	{
		return $this->function;
	}

	/**
	 * @param string $function
	 */
	public function setFunction($function)
	{
		$array = explode("#", $function);
		$return = [];
		if ($array) {
			foreach ($array as $item) {
				if (!is_int($item)) {
					$function = IngredientsFunction::findOne(['function' => $item]);
					if (!($function instanceof IngredientsFunction)) {
						$function = new IngredientsFunction();
						$function->setFunction($item);
						$function->save(false);
					}
					$return[] = $function->getId();
				} else {
					$return[] = $item;
				}
			}
		}
		$this->function = implode("#", $return);
	}

	/**
	 * @return string
	 */
	public function getCreateTime()
	{
		return $this->create_time;
	}

	/**
	 * @param string $create_time
	 */
	public function setCreateTime($create_time)
	{
		$this->create_time = $create_time;
	}

	/**
	 * @return string
	 */
	public function getUpdateTime()
	{
		return $this->update_time;
	}

	/**
	 * @param string $update_time
	 */
	public function setUpdateTime($update_time)
	{
		$this->update_time = $update_time;
	}

	/**
	 * @return string
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @param string $status
	 */
	public function setStatus($status)
	{
		$this->status = $status;
	}

	/**
	 * @return string
	 */
	public function getSource()
	{
		return $this->source;
	}

	/**
	 * @param string $source
	 */
	public function setSource($source)
	{
		$this->source = $source;
	}

	public function getAliasArray()
	{
		$return = [];
		if ($this->alias) {
			$alias = explode("#", $this->alias);
			foreach ($alias as $id) {
				$ingredientAlias = IngredientsAlias::findOne(['id' => $id]);
				if ($ingredientAlias instanceof IngredientsAlias) {
					$return[] = $ingredientAlias->getAlias();
				}
			}
		}
		return $return;
	}

	public function getFunctionArray()
	{
		$return = [];
		if ($this->function) {
			$function = explode("#", $this->function);
			foreach ($function as $id) {
				$ingredientFunction = IngredientsFunction::findOne(['id' => $id]);
				if ($ingredientFunction instanceof IngredientsFunction) {
					$return[] = $ingredientFunction->getFunction();
				}
			}
		}
		return $return;
	}


	public function getLabelAlias()
	{
		if (!$this->getAliasArray()) {
			return "";
		}
		$return = "";
		foreach ($this->getAliasArray() as $item) {
			$return .= "<h6><span class='badge cyan'>$item</span></h6>";
		}
		return $return;
	}

	public function getLabelFunction()
	{
		if (!$this->getFunctionArray()) {
			return "";
		}
		$return = "";
		foreach ($this->getFunctionArray() as $item) {
			$return .= "<h6><span class='badge badge-success'>$item</span></h6>";
		}
		return $return;
	}

	public static function getSelectOptions($ingredientsString)
	{
		$ingredients = Ingredients::find()->all();
		if (!$ingredients) {
			return "";
		}
		$return = "";
		foreach ($ingredients as $ingredient) {
			$id = $ingredient["id"];
			$name = $ingredient["name"];
			$selected = (in_array($id, explode("#", $ingredientsString))) ? "selected" : "";
			$return .= "<option $selected value='$id'>$name</option>\n";
		}
		return $return;
	}

	public function getDisplayCreateTime()
	{
		return date('Y-m-d H:i:s', $this->create_time);
	}

	public function getDisplayUpdateTime()
	{
		return date('Y-m-d H:i:s', $this->update_time);
	}

	public function getIngredientAlias()
	{
		$return = IngredientsAlias::find()->where(['ingredient_id' => $this->id])->all();
		return $return;
	}

}
