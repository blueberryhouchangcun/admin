<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%question}}".
 *
 * @property string  $id
 * @property string  $content
 * @property string  $editor
 * @property integer $create_time
 * @property integer $update_time
 * @property integer $order
 * @property string  $type
 * @property string  $status
 */
class Question extends ActiveRecord
{
	const TYPE_MultipleChoiceSingleAnswer = 'MultipleChoiceSingleAnswer';
	const TYPE_MultipleChoiceMultipleAnswer = 'MultipleChoiceMultipleAnswer';
	const TYPE_ManyChoiceSingleAnswer = 'ManyChoiceSingleAnswer';
	const TYPE_ManyChoiceManyAnswer = 'ManyChoiceManyAnswer';

	const STATUS_DELETE = 0;
	const STATUS_AVAILABLE = 1;

	public static function getDb()
	{
		return Yii::$app->get('what2eat');
	}


	/**
	 * @return string
	 */
	public function getLabelType()
	{
		$class = "badge ";
		switch ($this->type) {
			case self::TYPE_ManyChoiceManyAnswer:
				$class .= "red";
				break;
			case self::TYPE_ManyChoiceSingleAnswer:
				$class .= "cyan";
				break;
			case self::TYPE_MultipleChoiceMultipleAnswer:
				$class .= "amber";
				break;
			case self::TYPE_MultipleChoiceSingleAnswer:
				$class .= "purple";
				break;
			default:
				$class .= "indigo";
				break;
		}
		return "<h6><span class='$class'>$this->type</span></h6>";
	}

	/**
	 * @param string $type
	 */
	public function setType($type)
	{
		$this->type = $type;
	}

	/**
	 */
	public function getType()
	{
		return $this->type;
	}




	public function getDisplayCreateTime()
	{
		return date('Y-m-d H:i:s', $this->create_time);
	}

	public function getDisplayUpdateTime()
	{
		return date('Y-m-d H:i:s', $this->update_time);
	}



	public function getTypeSelectOptions($typeSelected)
	{
		$selectedEmpty = ($this->type == '') ? "selected" : "";
		$types = [
			self::TYPE_MultipleChoiceSingleAnswer,
			self::TYPE_MultipleChoiceMultipleAnswer,
			self::TYPE_ManyChoiceSingleAnswer,
			self::TYPE_ManyChoiceManyAnswer
		];
		$return = "<option value='' disabled $selectedEmpty>请选择问题类型</option>";
		foreach ($types as $type) {
			$selected = ($type == $typeSelected) ? "selected" : "";
			$return .= "<option value='$type' $selected>$type</option>";
		}
		return $return;
	}

	/**
	 * @return string
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param string $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}



	/**
	 * @return string
	 */
	public function getContent()
	{
		return $this->content;
	}

	/**
	 * @param string $content
	 */
	public function setContent($content)
	{
		$this->content = $content;
	}

	/**
	 * @return string
	 */
	public function getEditor()
	{
		return $this->editor;
	}

	/**
	 * @param string $editor
	 */
	public function setEditor($editor)
	{
		$this->editor = $editor;
	}

	/**
	 * @return int
	 */
	public function getCreateTime()
	{
		return $this->create_time;
	}

	/**
	 * @param int $create_time
	 */
	public function setCreateTime($create_time)
	{
		$this->create_time = $create_time;
	}

	/**
	 * @return int
	 */
	public function getUpdateTime()
	{
		return $this->update_time;
	}

	/**
	 * @param int $update_time
	 */
	public function setUpdateTime($update_time)
	{
		$this->update_time = $update_time;
	}

	/**
	 * @return string
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @param string $status
	 */
	public function setStatus($status)
	{
		$this->status = $status;
	}

	/**
	 * @return int
	 */
	public function getOrder()
	{
		return $this->order;
	}

	/**
	 * @param int $order
	 */
	public function setOrder($order)
	{
		$this->order = $order;
	}

	public function getAnswerOptions()
	{
		$answerOptions = AnswerOption::find()
			->where(['status' => AnswerOption::STATUS_AVAILABLE, 'question_id' => $this->id])
			->orderBy('order')->all();
		return $answerOptions;
	}


}
