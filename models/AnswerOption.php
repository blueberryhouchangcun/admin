<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "{{%answer_option}}".
 *
 * @property string  $id
 * @property string  $question_id
 * @property integer $order
 * @property integer $create_time
 * @property integer $update_time
 * @property string  $content
 * @property string  $editor
 * @property string  $shop_labels
 * @property string  $course_labels
 * @property string  $avoid_ingredients
 * @property integer $status
 */
class AnswerOption extends ActiveRecord
{
	const STATUS_DELETE = 0;
	const STATUS_AVAILABLE = 1;

	public static function getDb()
	{
		return Yii::$app->get('what2eat');
	}

	/**
	 * @return string
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param string $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}


	/**
	 * @return int
	 */
	public function getOrder()
	{
		return $this->order;
	}

	/**
	 * @param int $order
	 */
	public function setOrder($order)
	{
		$this->order = $order;
	}

	/**
	 * @return string
	 */
	public function getContent()
	{
		return $this->content;
	}

	/**
	 * @param string $content
	 */
	public function setContent($content)
	{
		$this->content = $content;
	}

	/**
	 * @return string
	 */
	public function getShopLabels()
	{
		return $this->shop_labels;
	}

	/**
	 * @param string $shop_labels
	 */
	public function setShopLabels($shop_labels)
	{
		$this->shop_labels = $shop_labels;
	}

	/**
	 * @return string
	 */
	public function getCourseLabels()
	{
		return $this->course_labels;
	}

	/**
	 * @param string $course_labels
	 */
	public function setCourseLabels($course_labels)
	{
		$this->course_labels = $course_labels;
	}

	/**
	 * @return int
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @param int $status
	 */
	public function setStatus($status)
	{
		$this->status = $status;
	}

	/**
	 * @return string
	 */
	public function getQuestionId()
	{
		return $this->question_id;
	}

	/**
	 * @param string $question_id
	 */
	public function setQuestionId($question_id)
	{
		$this->question_id = $question_id;
	}

	/**
	 * @return int
	 */
	public function getCreateTime()
	{
		return $this->create_time;
	}

	/**
	 * @param int $create_time
	 */
	public function setCreateTime($create_time)
	{
		$this->create_time = $create_time;
	}

	/**
	 * @return int
	 */
	public function getUpdateTime()
	{
		return $this->update_time;
	}

	/**
	 * @param int $update_time
	 */
	public function setUpdateTime($update_time)
	{
		$this->update_time = $update_time;
	}

	/**
	 * @return string
	 */
	public function getEditor()
	{
		return $this->editor;
	}

	/**
	 * @param string $editor
	 */
	public function setEditor($editor)
	{
		$this->editor = $editor;
	}


	public function getDisplayCreateTime()
	{
		return date('Y-m-d H:i:s', $this->create_time);
	}

	public function getDisplayUpdateTime()
	{
		return date('Y-m-d H:i:s', $this->update_time);
	}

	public function getQuestion()
	{
		$question = Question::findOne(['status' => Question::STATUS_AVAILABLE, 'id' => $this->question_id]);
		return $question;
	}

	public function getDisplayLabels($type = 'course')
	{
		$name = [];
		if ($type == 'course') {
			$labels = $this->course_labels;
		} else {
			$labels = $this->shop_labels;
		}
		if ($labels) {
			$categories = explode("#", $labels);
			foreach ($categories as $labelId) {
				$label = Label::findOne(['id' => $labelId]);
				if ($label instanceof Label) {
					$name[] = $label->getName();
				}
			}
		}
		return implode("#", $name);
	}

	/**
	 * @return string
	 */
	public function getAvoidIngredients()
	{
		return $this->avoid_ingredients;
	}

	/**
	 * @param string $avoid_ingredients
	 */
	public function setAvoidIngredients($avoid_ingredients)
	{
		$return = [];
		if ($avoid_ingredients) {
			$array = explode(",", $avoid_ingredients);
			if ($array) {
				foreach ($array as $item) {
					if (!is_int($item)) {
						$a = Ingredients::findOne(['name' => $item]);
						if ($a === null) {
							$alias = IngredientsAlias::findOne(['alias' => $item]);
							if ($alias instanceof IngredientsAlias) {
								$a = Ingredients::findOne(['id' => $alias->getIngredientId()]);
							}
							if ($a === null) {
								$a = new Ingredients();
								$a->setName($item);
								$a->save(false);
							}
						}
						$return[] = $a->getId();
					} else {
						$return[] = $item;
					}
				}
			}
		}
		$this->avoid_ingredients = implode("#", array_unique($return));
	}

	public function getDisplayAvoidIngredients()
	{
		$return = [];
		if ($this->avoid_ingredients) {
			$array = explode("#", $this->avoid_ingredients);
			foreach ($array as $item) {
				$ingredient = Ingredients::findOne(['id' => $item]);
				if ($ingredient !== null) {
					$name = $ingredient->getName();
					$return[] = $name;
				}
			}
		}
		return implode(",", $return);
	}


}
