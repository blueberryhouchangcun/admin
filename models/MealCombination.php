<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "{{%courses}}".
 *
 * @property integer  $id
 * @property string  $category
 * @property integer $number_of_dinners
 * @property string  $combination_json
 * @property string  $editor
 * @property integer $update_time
 * @property integer $status
 */
class MealCombination extends ActiveRecord
{
	const STATUS_DELETE = 0;
	const STATUS_AVAILABLE = 1;
	const NUMBER_OF_DINNERS = [1, 2];

	public static function getDb()
	{
		return Yii::$app->get('what2eat');
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}



	/**
	 * @return string
	 */
	public function getCategory()
	{
		return $this->category;
	}

	/**
	 * @param string $category
	 */
	public function setCategory($category)
	{
		$this->category = $category;
	}

	/**
	 * @return int
	 */
	public function getNumberOfDinners()
	{
		return $this->number_of_dinners;
	}

	/**
	 * @param int $number_of_dinners
	 */
	public function setNumberOfDinners($number_of_dinners)
	{
		$this->number_of_dinners = $number_of_dinners;
	}


	/**
	 * @return string
	 */
	public function getCombinationJson()
	{
		return $this->combination_json;
	}

	/**
	 * @param string $combination_json
	 */
	public function setCombinationJson($combination_json)
	{
		$this->combination_json = $combination_json;
	}

	/**
	 * @return string
	 */
	public function getEditor()
	{
		return $this->editor;
	}

	/**
	 * @param string $editor
	 */
	public function setEditor($editor)
	{
		$this->editor = $editor;
	}

	/**
	 * @return int
	 */
	public function getUpdateTime()
	{
		return $this->update_time;
	}

	/**
	 * @param int $update_time
	 */
	public function setUpdateTime($update_time)
	{
		$this->update_time = $update_time;
	}

	/**
	 * @return int
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @param int $status
	 */
	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getDisplayUpdateTime()
	{
		return date('Y-m-d H:i:s', $this->update_time);
	}

	public function getNumberOfDinnersSelectOptions($number)
	{
		$selectedEmpty = ($this->number_of_dinners == '') ? "selected" : "";
		$return = "<option value='' disabled $selectedEmpty>请选择用餐人数</option>";
		foreach (self::NUMBER_OF_DINNERS as $num) {
			$selected = ($num == $number) ? "selected" : "";
			$return .= "<option value=$num $selected>$num</option>";
		}
		return $return;
	}

	public function getDisplayCategory()
	{
		$return = "";
		if ($this->category) {
			$array = explode("#", $this->category);
			foreach ($array as $item) {
				$label = Label::findOne(['id' => $item]);
				if ($label instanceof Label) {
					$name = $label->getName();
					$return .= "<span class='badge badge-success'>$name</span>&nbsp;";
				}
			}
		}
		return $return;
	}

	public function getDisplayCombinationJson()
	{
		$array = [];
		if ($this->combination_json) {
			$array = json_decode($this->combination_json, true);
			foreach ($array as $index => $item) {
				$array[$index]['food_component'] = SubLabels::labelId2String($item['food_component']);
			}
		}
		return json_encode($array, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
	}
}
