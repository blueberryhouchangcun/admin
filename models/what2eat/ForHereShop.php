<?php

namespace app\models\what2eat;

use app\models\Label;
use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "{{%for_here_shop}}".
 *
 * @property string  $id
 * @property string  $category // 菜式 & 菜系
 * @property string  $original_category // 原始爬虫分类
 * @property string  $name
 * @property string  $open_hours
 * @property string  $feature
 * @property string  $geo_hash
 * @property string  $taste_rate
 * @property string  $environment_rate
 * @property string  $service_rate
 * @property integer $reserve
 * @property integer $dianping_id
 * @property string  $link
 * @property string  $address
 * @property string  $region
 * @property string  $city
 * @property string  $district
 * @property string  $avg_price
 * @property string  $comment_quantity
 * @property string  $telephone
 * @property integer $tuangou
 * @property integer $waimai
 * @property string  $status
 * @property string  $editor
 * @property string  $labels
 * @property integer $create_time
 * @property integer $update_time
 * @property integer $shop_profile_update_time
 * @property string  $geo_type
 * @property string  $geo_latitude
 * @property string  $geo_longitude
 * @property string  $formatted_address
 * @property string  $geo_adcode
 * @property string  $street
 * @property string  $meal_combination
 * @property string  $recommend_reason
 * @property string  $shop_profile
 *
 */
class ForHereShop extends ActiveRecord
{
	const STATUS_IMPORTED = 'imported';
	const STATUS_MANUAL_EDITED = 'manual_edited';
	const STATUS_DELETED = 'deleted';

	public static function getDb()
	{
		return Yii::$app->get('what2eat');
	}

	/**
	 * @return string
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param string $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getCategory()
	{
		return $this->category;
	}

	/**
	 * @param string $category
	 */
	public function setCategory($category)
	{
		$this->category = $category;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getOpenHours()
	{
		return $this->open_hours;
	}

	/**
	 * @param string $open_hours
	 */
	public function setOpenHours($open_hours)
	{
		$this->open_hours = $open_hours;
	}

	/**
	 * @return string
	 */
	public function getFeature()
	{
		return $this->feature;
	}

	/**
	 * @param string $feature
	 */
	public function setFeature($feature)
	{
		$this->feature = $feature;
	}

	/**
	 * @return string
	 */
	public function getGeoHash()
	{
		return $this->geo_hash;
	}

	/**
	 * @param string $geo_hash
	 */
	public function setGeoHash($geo_hash)
	{
		$this->geo_hash = $geo_hash;
	}

	/**
	 * @return string
	 */
	public function getRate()
	{
		return $this->rate;
	}

	/**
	 * @param string $rate
	 */
	public function setRate($rate)
	{
		$this->rate = $rate;
	}

	/**
	 * @return string
	 */
	public function getReserve()
	{
		return $this->reserve;
	}

	/**
	 * @param string $reserve
	 */
	public function setReserve($reserve)
	{
		$this->reserve = $reserve;
	}

	/**
	 * @return string
	 */
	public function getLink()
	{
		return $this->link;
	}

	/**
	 * @param string $link
	 */
	public function setLink($link)
	{
		$this->link = $link;
	}

	/**
	 * @return string
	 */
	public function getAddress()
	{
		return $this->address;
	}

	/**
	 * @param string $address
	 */
	public function setAddress($address)
	{
		$this->address = $address;
	}

	/**
	 * @return string
	 */
	public function getRegion()
	{
		return $this->region;
	}

	/**
	 * @param string $region
	 */
	public function setRegion($region)
	{
		$this->region = $region;
	}

	/**
	 * @return string
	 */
	public function getTasteRate()
	{
		return $this->taste_rate;
	}

	/**
	 * @param string $taste_rate
	 */
	public function setTasteRate($taste_rate)
	{
		$this->taste_rate = $taste_rate;
	}

	/**
	 * @return string
	 */
	public function getEnvironmentRate()
	{
		return $this->environment_rate;
	}

	/**
	 * @param string $environment_rate
	 */
	public function setEnvironmentRate($environment_rate)
	{
		$this->environment_rate = $environment_rate;
	}

	/**
	 * @return string
	 */
	public function getServiceRate()
	{
		return $this->service_rate;
	}

	/**
	 * @param string $service_rate
	 */
	public function setServiceRate($service_rate)
	{
		$this->service_rate = $service_rate;
	}

	/**
	 * @return string
	 */
	public function getCity()
	{
		return $this->city;
	}

	/**
	 * @param string $city
	 */
	public function setCity($city)
	{
		$this->city = $city;
	}

	/**
	 * @return string
	 */
	public function getDistrict()
	{
		return $this->district;
	}

	/**
	 * @param string $district
	 */
	public function setDistrict($district)
	{
		$this->district = $district;
	}

	/**
	 * @return string
	 */
	public function getAvgPrice()
	{
		return $this->avg_price;
	}

	/**
	 * @param string $avg_price
	 */
	public function setAvgPrice($avg_price)
	{
		$this->avg_price = $avg_price;
	}

	/**
	 * @return string
	 */
	public function getTelephone()
	{
		return $this->telephone;
	}

	/**
	 * @param string $telephone
	 */
	public function setTelephone($telephone)
	{
		$this->telephone = $telephone;
	}

	/**
	 * @return int
	 */
	public function getDianpingId()
	{
		return $this->dianping_id;
	}

	/**
	 * @param int $dianping_id
	 */
	public function setDianpingId($dianping_id)
	{
		$this->dianping_id = $dianping_id;
	}

	/**
	 * @return int
	 */
	public function getTuangou()
	{
		return $this->tuangou;
	}

	/**
	 * @param int $tuangou
	 */
	public function setTuangou($tuangou)
	{
		$this->tuangou = $tuangou;
	}

	/**
	 * @return int
	 */
	public function getWaimai()
	{
		return $this->waimai;
	}

	/**
	 * @param int $waimai
	 */
	public function setWaimai($waimai)
	{
		$this->waimai = $waimai;
	}


	/**
	 * @return string
	 */
	public function getCommentQuantity()
	{
		return $this->comment_quantity;
	}

	/**
	 * @param string $comment_quantity
	 */
	public function setCommentQuantity($comment_quantity)
	{
		$this->comment_quantity = $comment_quantity;
	}

	/**
	 * @return string
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @param string $status
	 */
	public function setStatus($status)
	{
		$this->status = $status;
	}

	/**
	 * @return string
	 */
	public function getEditor()
	{
		return $this->editor;
	}

	/**
	 * @param string $editor
	 */
	public function setEditor($editor)
	{
		$this->editor = $editor;
	}

	/**
	 * @return string
	 */
	public function getLabels()
	{
		return $this->labels;
	}

	/**
	 * @param string $labels
	 */
	public function setLabels($labels)
	{
		$this->labels = $labels;
	}

	/**
	 * @return int
	 */
	public function getCreateTime()
	{
		return $this->create_time;
	}

	/**
	 * @param int $create_time
	 */
	public function setCreateTime($create_time)
	{
		$this->create_time = $create_time;
	}

	/**
	 * @return int
	 */
	public function getUpdateTime()
	{
		return $this->update_time;
	}

	/**
	 * @param int $update_time
	 */
	public function setUpdateTime($update_time)
	{
		$this->update_time = $update_time;
	}

	public function getDisplayCreateTime()
	{
		return date('Y-m-d H:i:s', $this->create_time);
	}

	public function getDisplayUpdateTime()
	{
		return date('Y-m-d H:i:s', $this->update_time);
	}

	public function getLabelsName()
	{
		$return = [];
		$labelsId = explode("#", $this->labels);
		if ($labelsId) {
			foreach ($labelsId as $id) {
				$label = Label::findOne(['id' => $id]);
				if ($label instanceof Label) {
					$return[] = $label->getName();
				}
			}
		}
		return implode("#", $return);
	}


	/**
	 * @return string
	 */
	public function getGeoType()
	{
		return $this->geo_type;
	}

	/**
	 * @param string $geo_type
	 */
	public function setGeoType($geo_type)
	{
		$this->geo_type = $geo_type;
	}

	/**
	 * @return string
	 */
	public function getGeoLatitude()
	{
		return $this->geo_latitude;
	}

	/**
	 * @param string $geo_latitude
	 */
	public function setGeoLatitude($geo_latitude)
	{
		$this->geo_latitude = $geo_latitude;
	}

	/**
	 * @return string
	 */
	public function getGeoLongitude()
	{
		return $this->geo_longitude;
	}

	/**
	 * @param string $geo_longitude
	 */
	public function setGeoLongitude($geo_longitude)
	{
		$this->geo_longitude = $geo_longitude;
	}

	/**
	 * @return string
	 */
	public function getFormattedAddress()
	{
		return $this->formatted_address;
	}

	/**
	 * @param string $formatted_address
	 */
	public function setFormattedAddress($formatted_address)
	{
		$this->formatted_address = $formatted_address;
	}

	/**
	 * @return string
	 */
	public function getGeoAdcode()
	{
		return $this->geo_adcode;
	}

	/**
	 * @param string $geo_adcode
	 */
	public function setGeoAdcode($geo_adcode)
	{
		$this->geo_adcode = $geo_adcode;
	}

	/**
	 * @return string
	 */
	public function getStreet()
	{
		return $this->street;
	}

	/**
	 * @param string $street
	 */
	public function setStreet($street)
	{
		$this->street = $street;
	}

	/**
	 * @return string
	 */
	public function getMealCombination()
	{
		return $this->meal_combination;
	}

	/**
	 * @param string $meal_combination
	 */
	public function setMealCombination($meal_combination)
	{
		$this->meal_combination = $meal_combination;
	}

	public function getDisplayCategory()
	{
		$name = [];
		if ($this->category) {
			$categories = explode("#", $this->category);
			foreach ($categories as $labelId) {
				$label = Label::findOne(['id' => $labelId]);
				if ($label instanceof Label) {
					$name[] = $label->getName();
				}
			}
		}
		return implode("#", $name);
	}

	/**
	 * @return string
	 */
	public function getRecommendReason()
	{
		return $this->recommend_reason;
	}

	/**
	 * @param string $recommend_reason
	 */
	public function setRecommendReason($recommend_reason)
	{
		$this->recommend_reason = $recommend_reason;
	}

	/**
	 * @return string
	 */
	public function getOriginalCategory()
	{
		return $this->original_category;
	}

	/**
	 * @param string $original_category
	 */
	public function setOriginalCategory($original_category)
	{
		$this->original_category = $original_category;
	}

	/**
	 * @return int
	 */
	public function getShopProfileUpdateTime()
	{
		return $this->shop_profile_update_time;
	}

	/**
	 * @param int $shop_profile_update_time
	 */
	public function setShopProfileUpdateTime($shop_profile_update_time)
	{
		$this->shop_profile_update_time = $shop_profile_update_time;
	}

	/**
	 * @return string
	 */
	public function getShopProfile()
	{
		return $this->shop_profile;
	}

	/**
	 * @param string $shop_profile
	 */
	public function setShopProfile($shop_profile)
	{
		$this->shop_profile = $shop_profile;
	}


}
