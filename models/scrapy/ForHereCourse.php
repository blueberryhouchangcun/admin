<?php

namespace app\models\scrapy;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "{{%for_here_course}}".
 *
 * @property string $id
 * @property string $name
 * @property string $price
 * @property string $image_url
 * @property string $link
 * @property integer $recommend_count
 * @property string $source
 * @property string $shop_id
 */
class ForHereCourse extends ActiveRecord
{

	public static function getDb()
	{
		return Yii::$app->get('scrapy');
	}

	/**
	 * @return string
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param string $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}


	/**
	 * @return string
	 */
	public function getPrice()
	{
		return $this->price;
	}

	/**
	 * @param string $price
	 */
	public function setPrice($price)
	{
		$this->price = $price;
	}


	/**
	 * @return string
	 */
	public function getImageUrl()
	{
		return $this->image_url;
	}

	/**
	 * @param string $image_url
	 */
	public function setImageUrl($image_url)
	{
		$this->image_url = $image_url;
	}

	/**
	 * @return string
	 */
	public function getLink()
	{
		return $this->link;
	}

	/**
	 * @param string $link
	 */
	public function setLink($link)
	{
		$this->link = $link;
	}



	/**
	 * @return string
	 */
	public function getSource()
	{
		return $this->source;
	}

	/**
	 * @param string $source
	 */
	public function setSource($source)
	{
		$this->source = $source;
	}

	/**
	 * @return string
	 */
	public function getShopId()
	{
		return $this->shop_id;
	}

	/**
	 * @param string $shop_id
	 */
	public function setShopId($shop_id)
	{
		$this->shop_id = $shop_id;
	}

	/**
	 * @return int
	 */
	public function getRecommendCount()
	{
		return $this->recommend_count;
	}

	/**
	 * @param int $recommend_count
	 */
	public function setRecommendCount($recommend_count)
	{
		$this->recommend_count = $recommend_count;
	}


}
