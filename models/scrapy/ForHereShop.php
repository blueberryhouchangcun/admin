<?php

namespace app\models\scrapy;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "{{%for_here_shop}}".
 *
 * @property string  $id
 * @property string  $category
 * @property string  $name
 * @property string  $open_hours
 * @property string  $feature
 * @property string  $geo_hash
 * @property string  $taste_rate
 * @property string  $environment_rate
 * @property string  $service_rate
 * @property integer $reserve
 * @property integer $dianping_id
 * @property string  $link
 * @property string  $address
 * @property string  $region
 * @property string  $city
 * @property string  $district
 * @property string  $avg_price
 * @property string  $comment_quantity
 * @property string  $telephone
 * @property integer $tuangou
 * @property integer $waimai
 */
class ForHereShop extends ActiveRecord
{

	public static function getDb()
	{
		return Yii::$app->get('scrapy');
	}

	/**
	 * @return string
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param string $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getCategory()
	{
		return $this->category;
	}

	/**
	 * @param string $category
	 */
	public function setCategory($category)
	{
		$this->category = $category;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getOpenHours()
	{
		return $this->open_hours;
	}

	/**
	 * @param string $open_hours
	 */
	public function setOpenHours($open_hours)
	{
		$this->open_hours = $open_hours;
	}

	/**
	 * @return string
	 */
	public function getFeature()
	{
		return $this->feature;
	}

	/**
	 * @param string $feature
	 */
	public function setFeature($feature)
	{
		$this->feature = $feature;
	}

	/**
	 * @return string
	 */
	public function getGeoHash()
	{
		return $this->geo_hash;
	}

	/**
	 * @param string $geo_hash
	 */
	public function setGeoHash($geo_hash)
	{
		$this->geo_hash = $geo_hash;
	}

	/**
	 * @return string
	 */
	public function getRate()
	{
		return $this->rate;
	}

	/**
	 * @param string $rate
	 */
	public function setRate($rate)
	{
		$this->rate = $rate;
	}

	/**
	 * @return string
	 */
	public function getReserve()
	{
		return $this->reserve;
	}

	/**
	 * @param string $reserve
	 */
	public function setReserve($reserve)
	{
		$this->reserve = $reserve;
	}

	/**
	 * @return string
	 */
	public function getLink()
	{
		return $this->link;
	}

	/**
	 * @param string $link
	 */
	public function setLink($link)
	{
		$this->link = $link;
	}

	/**
	 * @return string
	 */
	public function getAddress()
	{
		return $this->address;
	}

	/**
	 * @param string $address
	 */
	public function setAddress($address)
	{
		$this->address = $address;
	}

	/**
	 * @return string
	 */
	public function getRegion()
	{
		return $this->region;
	}

	/**
	 * @param string $region
	 */
	public function setRegion($region)
	{
		$this->region = $region;
	}

	/**
	 * @return string
	 */
	public function getTasteRate()
	{
		return $this->taste_rate;
	}

	/**
	 * @param string $taste_rate
	 */
	public function setTasteRate($taste_rate)
	{
		$this->taste_rate = $taste_rate;
	}

	/**
	 * @return string
	 */
	public function getEnvironmentRate()
	{
		return $this->environment_rate;
	}

	/**
	 * @param string $environment_rate
	 */
	public function setEnvironmentRate($environment_rate)
	{
		$this->environment_rate = $environment_rate;
	}

	/**
	 * @return string
	 */
	public function getServiceRate()
	{
		return $this->service_rate;
	}

	/**
	 * @param string $service_rate
	 */
	public function setServiceRate($service_rate)
	{
		$this->service_rate = $service_rate;
	}

	/**
	 * @return string
	 */
	public function getCity()
	{
		return $this->city;
	}

	/**
	 * @param string $city
	 */
	public function setCity($city)
	{
		$this->city = $city;
	}

	/**
	 * @return string
	 */
	public function getDistrict()
	{
		return $this->district;
	}

	/**
	 * @param string $district
	 */
	public function setDistrict($district)
	{
		$this->district = $district;
	}

	/**
	 * @return string
	 */
	public function getAvgPrice()
	{
		return $this->avg_price;
	}

	/**
	 * @param string $avg_price
	 */
	public function setAvgPrice($avg_price)
	{
		$this->avg_price = $avg_price;
	}

	/**
	 * @return string
	 */
	public function getTelephone()
	{
		return $this->telephone;
	}

	/**
	 * @param string $telephone
	 */
	public function setTelephone($telephone)
	{
		$this->telephone = $telephone;
	}

	/**
	 * @return int
	 */
	public function getDianpingId()
	{
		return $this->dianping_id;
	}

	/**
	 * @param int $dianping_id
	 */
	public function setDianpingId($dianping_id)
	{
		$this->dianping_id = $dianping_id;
	}

	/**
	 * @return int
	 */
	public function getTuangou()
	{
		return $this->tuangou;
	}

	/**
	 * @param int $tuangou
	 */
	public function setTuangou($tuangou)
	{
		$this->tuangou = $tuangou;
	}

	/**
	 * @return int
	 */
	public function getWaimai()
	{
		return $this->waimai;
	}

	/**
	 * @param int $waimai
	 */
	public function setWaimai($waimai)
	{
		$this->waimai = $waimai;
	}



	/**
	 * @return string
	 */
	public function getCommentQuantity()
	{
		return $this->comment_quantity;
	}

	/**
	 * @param string $comment_quantity
	 */
	public function setCommentQuantity($comment_quantity)
	{
		$this->comment_quantity = $comment_quantity;
	}


}
