<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "{{%user_answers}}".
 *
 * @property integer $id
 * @property string  $question_id
 * @property string  $user_id
 * @property string  $answer_option_ids
 * @property integer $create_time
 */
class UserAnswers extends ActiveRecord
{
	public static function getDb()
	{
		return Yii::$app->get('account');
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getQuestionId()
	{
		return $this->question_id;
	}

	/**
	 * @param string $question_id
	 */
	public function setQuestionId($question_id)
	{
		$this->question_id = $question_id;
	}

	/**
	 * @return string
	 */
	public function getUserId()
	{
		return $this->user_id;
	}

	/**
	 * @param string $user_id
	 */
	public function setUserId($user_id)
	{
		$this->user_id = $user_id;
	}

	/**
	 * @return string
	 */
	public function getAnswerOptionIds()
	{
		return $this->answer_option_ids;
	}

	/**
	 * @param string $answer_option_ids
	 */
	public function setAnswerOptionIds($answer_option_ids)
	{
		$this->answer_option_ids = $answer_option_ids;
	}

	/**
	 * @return int
	 */
	public function getCreateTime()
	{
		return $this->create_time;
	}

	/**
	 * @param int $create_time
	 */
	public function setCreateTime($create_time)
	{
		$this->create_time = $create_time;
	}


	public function getAnswers()
	{
		$return = [];
		$answerOptionIds = $this->getAnswerOptionIds();
		$answerOptionIds = (explode("#", $answerOptionIds));
		$answers = AnswerOption::find()->where(['id' => $answerOptionIds])->all();
		foreach ($answers as $answer) {
			$return[] = $answer;
		}
		return $return;
	}

	public function getAnswersStringArray()
	{
		$return = [];
		$answerOptionIds = $this->getAnswerOptionIds();
		$answerOptionIds = (explode("#", $answerOptionIds));
		$answerOptions = AnswerOption::find()->where(['id' => $answerOptionIds])->all();
		foreach ($answerOptions as $answerOption) {
			if ($answerOption instanceof AnswerOption) {
				$return[] = $answerOption->getContent();
			}
		}
		return $return;
	}

	public function getDisplayCreateTime()
	{
		return date('Y-m-d H:i:s', $this->create_time);
	}
}
