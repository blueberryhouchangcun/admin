<?php
/**
 * Created by PhpStorm.
 * User: manjinxin
 * Date: 2018/5/22
 * Time: 16:48
 */

namespace app\models;


class SubLabels
{
	static public $combination_attribution = [
		519 => 254,
		520 => 285,
		521 => 417,
		523 => 418,
		524 => 419,
		525 => 420,
		526 => 421
	];

	static public function getCategoriesSelectOptions($categoriesString)
	{
		$categoriesId = array_keys(self::$combination_attribution);
		$return = "";
		foreach ($categoriesId as $categoryId) {
			$category = Label::findOne(['id' => $categoryId]);
			if ($category instanceof Label) {
				$id = $category->getId();
				$name = $category->getName();
				$selected = (in_array($id, explode("#", $categoriesString))) ? "selected" : "";
				$return .= "<option $selected value='$id'>$name</option>\n";
			}
		}
		return $return;
	}


	static public function labelString2Id($stringArray)
	{
		$return = [];
		foreach ($stringArray as $item) {
			$label = Label::findOne(['name' => $item]);
			if ($label instanceof Label) {
				$return[] = $label->getId();
			}
		}
		return $return;
	}

	static public function labelId2String($idArray)
	{
		$return = [];
		foreach ($idArray as $item) {
			$label = Label::findOne(['id' => $item]);
			if ($label instanceof Label) {
				$return[] = $label->getName();
			}
		}
		return $return;
	}
}