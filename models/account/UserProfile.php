<?php

namespace app\models\account;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "{{%account}}".
 *
 * @property integer $id
 * @property string  $user_id
 * @property string  $user_profile
 * @property integer $create_time
 */
class UserProfile extends ActiveRecord
{


	public static function getDb()
	{
		return Yii::$app->get('account');
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getUserId()
	{
		return $this->user_id;
	}

	/**
	 * @param string $user_id
	 */
	public function setUserId($user_id)
	{
		$this->user_id = $user_id;
	}

	/**
	 * @return string
	 */
	public function getUserProfile()
	{
		return $this->user_profile;
	}

	/**
	 * @param string $user_profile
	 */
	public function setUserProfile($user_profile)
	{
		$this->user_profile = $user_profile;
	}

	/**
	 * @return int
	 */
	public function getCreateTime()
	{
		return $this->create_time;
	}

	/**
	 * @param int $create_time
	 */
	public function setCreateTime($create_time)
	{
		$this->create_time = $create_time;
	}



}
