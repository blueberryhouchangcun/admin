<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;
/**
 * This is the model class for table "{{%ingredients_alias}}".
 *
 * @property integer $id
 * @property string  $function
 */
class IngredientsFunction extends ActiveRecord
{
	public static function getDb() {
		return Yii::$app->get('what2eat');
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getFunction()
	{
		return $this->function;
	}

	/**
	 * @param string $function
	 */
	public function setFunction($function)
	{
		$this->function = $function;
	}


}
