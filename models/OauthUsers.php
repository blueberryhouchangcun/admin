<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "{{%oauth_users}}".
 *
 * @property string  $id
 * @property string  $username
 * @property string  $password
 * @property string  $first_name
 * @property string  $last_name
 * @property integer $gender
 * @property string  $tip
 * @property string  $status
 * @property integer $birth_year
 * @property integer $height
 * @property integer $weight
 * @property integer $create_time
 * @property integer $update_time
 * @property string  $editor
 */
class OauthUsers extends ActiveRecord
{

	const GENDER_MALE = 1;
	const GENDER_FEMALE = 2;
	const STATUS_REGISTERED = 'registered';
	const STATUS_NEW_USER = 'new_user';
	const STATUS_DELETED = 'deleted';

	public static function getDb()
	{
		return Yii::$app->get('account');
	}

	/**
	 * @return string
	 */
	public function getUsername()
	{
		return $this->username;
	}

	/**
	 * @param string $username
	 */
	public function setUsername($username)
	{
		$this->username = $username;
	}

	/**
	 * @return string
	 */
	public function getPassword()
	{
		return $this->password;
	}

	/**
	 * @param string $password
	 */
	public function setPassword($password)
	{
		$this->password = $password;
	}

	/**
	 * @return string
	 */
	public function getFirstName()
	{
		return $this->first_name;
	}

	/**
	 * @param string $first_name
	 */
	public function setFirstName($first_name)
	{
		$this->first_name = $first_name;
	}

	/**
	 * @return string
	 */
	public function getLastName()
	{
		return $this->last_name;
	}

	/**
	 * @param string $last_name
	 */
	public function setLastName($last_name)
	{
		$this->last_name = $last_name;
	}

	/**
	 * @return string
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param string $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getGender()
	{
		return $this->gender;
	}

	/**
	 * @param string $gender
	 */
	public function setGender($gender)
	{
		$this->gender = $gender;
	}

	/**
	 * @return int
	 */
	public function getBirthYear()
	{
		return $this->birth_year;
	}

	/**
	 * @param int $birth_year
	 */
	public function setBirthYear($birth_year)
	{
		$this->birth_year = $birth_year;
	}

	/**
	 * @return int
	 */
	public function getHeight()
	{
		return $this->height;
	}

	/**
	 * @param int $height
	 */
	public function setHeight($height)
	{
		$this->height = $height;
	}

	/**
	 * @return int
	 */
	public function getWeight()
	{
		return $this->weight;
	}

	/**
	 * @param int $weight
	 */
	public function setWeight($weight)
	{
		$this->weight = $weight;
	}

	/**
	 * @return string
	 */
	public function getTip()
	{
		return $this->tip;
	}

	/**
	 * @param string $tip
	 */
	public function setTip($tip)
	{
		$this->tip = $tip;
	}

	/**
	 * @return string
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @param string $status
	 */
	public function setStatus($status)
	{
		$this->status = $status;
	}


	public function getFullName()
	{
		return $this->last_name . $this->first_name;
	}

	public function getAge()
	{
		return date('Y') - $this->birth_year;
	}

	public function getLabelUsername()
	{
		if ($this->gender == self::GENDER_MALE) {
			$avatar = 'avatar-6.jpg';
		} else {
			$avatar = 'avatar-10.jpg';
		}
		return "<div class='chip'><img src='/image/$avatar' alt='Oooooops'>$this->username</div>";
	}

	public function getSelectOptions($gender)
	{
		$selectedEmpty = ($this->gender == '') ? "selected" : "";
		$male = self::GENDER_MALE;
		$female = self::GENDER_FEMALE;
		$selectedMale = ($gender == $male) ? "selected" : "";
		$selectedFemale = ($gender == $female) ? "selected" : "";
		$return = "<option value='' disabled $selectedEmpty>请选择性别</option>
                 <option value='$male' $selectedMale>小哥哥</option>
                 <option value='$female' $selectedFemale>小姐姐</option>";
		return $return;
	}

	/**
	 * @return int
	 */
	public function getCreateTime()
	{
		return $this->create_time;
	}

	public function getDisplayCreateTime()
	{
		return date('Y-m-d H:i:s', $this->create_time);
	}

	public function getDisplayUpdateTime()
	{
		return date('Y-m-d H:i:s', $this->update_time);
	}

	/**
	 * @param int $create_time
	 */
	public function setCreateTime($create_time)
	{
		$this->create_time = $create_time;
	}

	/**
	 * @return int
	 */
	public function getUpdateTime()
	{
		return $this->update_time;
	}

	/**
	 * @param int $update_time
	 */
	public function setUpdateTime($update_time)
	{
		$this->update_time = $update_time;
	}

	/**
	 * @return string
	 */
	public function getEditor()
	{
		return $this->editor;
	}

	/**
	 * @param string $editor
	 */
	public function setEditor($editor)
	{
		$this->editor = $editor;
	}

	public function getUserAnswerByQuestionId($questionId)
	{
		$userAnswers = UserAnswers::find()->where(['question_id' => $questionId, 'user_id' => $this->id])->all();

		return $userAnswers;
	}

	public function getLabelStatus()
	{
		switch ($this->status) {
			case self::STATUS_REGISTERED:
				$class = "badge-info";
				break;
			case self::STATUS_NEW_USER:
				$class = "badge-success";
				break;
			case self::STATUS_DELETED:
				$class = "badge-danger";
				break;
			default:
				$class = "badge-info";
				break;
		}
		return "<span class='badge $class'>$this->status</span>";
	}

}
