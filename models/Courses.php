<?php

namespace app\models;

use app\models\what2eat\ForHereCourse;
use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "{{%courses}}".
 *
 * @property string  $id
 * @property string  $name
 * @property string  $ingredients
 * @property string  $complementary_ingredients
 * @property string  $principal_ingredients
 * @property string  $editor
 * @property string  $labels
 * @property integer $create_time
 * @property integer $update_time
 * @property integer $course_profile_update_time
 * @property string  $status
 * @property string  $course_profile
 * @property string  $check_reason
 * @property integer $avg_recommend_count
 * @property integer $check
 */
class Courses extends ActiveRecord
{
	const STATUS_IMPORTED = 'imported';
	const STATUS_MANUAL_EDITED = 'manual_edited';
	const STATUS_DELETED = 'deleted';
	const STATUS_LOCKED = 'locked';
	const STATUS_REVIEW = 'review';
	const CHECK_VALID = 1;
	const CHECK_INVALID = 2;
	const CHECK_REVIEW = 3;


	public static function getDb()
	{
		return Yii::$app->get('what2eat');
	}

	/**
	 * @return string
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param string $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}


	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getIngredients()
	{
		return $this->ingredients;
	}

	/**
	 * @param string $ingredients
	 */
	public function setIngredients($ingredients)
	{
		$this->ingredients = $ingredients;
	}

	/**
	 * @return string
	 */
	public function getLabels()
	{
		return $this->labels;
	}

	/**
	 * @param string $labels
	 */
	public function setLabels($labels)
	{
		$this->labels = $labels;
	}

	/**
	 * @return string
	 */
	public function getCreateTime()
	{
		return $this->create_time;
	}

	/**
	 * @param string $create_time
	 */
	public function setCreateTime($create_time)
	{
		$this->create_time = $create_time;
	}

	/**
	 * @return string
	 */
	public function getUpdateTime()
	{
		return $this->update_time;
	}

	/**
	 * @param string $update_time
	 */
	public function setUpdateTime($update_time)
	{
		$this->update_time = $update_time;
	}

	/**
	 * @return int
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @param int $status
	 */
	public function setStatus($status)
	{
		$this->status = $status;
	}

	/**
	 * @return string
	 */
	public function getEditor()
	{
		return $this->editor;
	}

	/**
	 * @param string $editor
	 */
	public function setEditor($editor)
	{
		$this->editor = $editor;
	}

	public function getDisplayCreateTime()
	{
		return date('Y-m-d H:i:s', $this->create_time);
	}

	public function getDisplayUpdateTime()
	{
		return date('Y-m-d H:i:s', $this->update_time);
	}

	public function getIngredientsName($type = 'principal')
	{
		$return = [];
		if ($type == 'principal') {
			$ingredients = $this->principal_ingredients;
		} else {
			$ingredients = $this->complementary_ingredients;
		}
		$ingredientsId = explode("#", $ingredients);
		if ($ingredientsId) {
			foreach ($ingredientsId as $id) {
				$ingredient = Ingredients::findOne(['id' => $id]);
				if ($ingredient instanceof Ingredients) {
					$return[] = $ingredient->getName();
				}
			}
		}
		return implode("#", $return);
	}

	public function getLabelsName()
	{
		$return = [];
		$labelsId = explode("#", $this->labels);
		if ($labelsId) {
			foreach ($labelsId as $id) {
				$label = Label::findOne(['id' => $id]);
				if ($label instanceof Label) {
					$return[] = $label->getName();
				}
			}
		}
		return implode("#", $return);
	}

	/**
	 * @return string
	 */
	public function getComplementaryIngredients()
	{
		return $this->complementary_ingredients;
	}

	/**
	 * @param string $complementary_ingredients
	 */
	public function setComplementaryIngredients($complementary_ingredients)
	{
		$return = [];
		if ($complementary_ingredients) {
			$array = explode(",", $complementary_ingredients);
			if ($array) {
				foreach ($array as $item) {
					if (!is_int($item)) {
						$a = Ingredients::findOne(['name' => $item]);
						if ($a === null) {
							$alias = IngredientsAlias::findOne(['alias' => $item]);
							if ($alias instanceof IngredientsAlias) {
								$a = Ingredients::findOne(['id' => $alias->getIngredientId()]);
							}
							if ($a === null) {
								$a = new Ingredients();
								$a->setName($item);
								$a->save(false);
							}
						}
						$return[] = $a->getId();
					} else {
						$return[] = $item;
					}
				}
			}
		}
		$this->complementary_ingredients = implode("#", array_unique($return));
	}

	/**
	 * @return string
	 */
	public function getPrincipalIngredients()
	{
		return $this->principal_ingredients;
	}

	/**
	 * @param string $principal_ingredients
	 */
	public function setPrincipalIngredients($principal_ingredients)
	{
		$return = [];
		if ($principal_ingredients) {
			$array = explode(",", $principal_ingredients);
			if ($array) {
				foreach ($array as $item) {
					if (!is_int($item)) {
						$a = Ingredients::findOne(['name' => $item]);
						if ($a === null) {
							$alias = IngredientsAlias::findOne(['alias' => $item]);
							if ($alias instanceof IngredientsAlias) {
								$a = Ingredients::findOne(['id' => $alias->getIngredientId()]);
							}
							if ($a === null) {
								$a = new Ingredients();
								$a->setName($item);
								$a->save(false);
							}
						}
						$return[] = $a->getId();
					} else {
						$return[] = $item;
					}
				}
			}
		}

		$this->principal_ingredients = implode("#", array_unique($return));
	}

	public function getForHereCourses()
	{
		$courses = ForHereCourse::findAll(['course_id' => $this->id, 'status' => [ForHereCourse::STATUS_IMPORTED, ForHereCourse::STATUS_MANUAL_EDITED]]);
		return $courses;
	}

	public function getForHereShops()
	{
		$shops = [];
		$courses = $this->getForHereCourses();
		foreach ($courses as $course) {
			if ($course instanceof ForHereCourse) {
				$shops[] = $course->getShop();
			}
		}
		return $shops;
	}

	/**
	 * @return integer
	 */
	public function getAvgRecommendCount()
	{
		return $this->avg_recommend_count;
	}

	/**
	 * @param integer $avg_recommend_count
	 */
	public function setAvgRecommendCount($avg_recommend_count)
	{
		$this->avg_recommend_count = $avg_recommend_count;
	}

	public function getDisplayPrincipalIngredients()
	{
		$return = [];
		if ($this->principal_ingredients) {
			$array = explode("#", $this->principal_ingredients);
			foreach ($array as $item) {
				$ingredient = Ingredients::findOne(['id' => $item]);
				if ($ingredient !== null) {
					$name = $ingredient->getName();
					$return[] = $name;
				}
			}
		}
		return implode(",", $return);
	}

	public function getDisplayComplementaryIngredients()
	{
		$return = [];
		if ($this->complementary_ingredients) {
			$array = explode("#", $this->complementary_ingredients);
			foreach ($array as $item) {
				$ingredient = Ingredients::findOne(['id' => $item]);
				if ($ingredient !== null) {
					$name = $ingredient->getName();
					$return[] = $name;
				}
			}
		}
		return implode(",", $return);
	}

	static public function getNext()
	{
		$courses = Courses::find()
			->leftJoin('for_here_course', 'courses.id=for_here_course.course_id')
			->leftJoin('for_here_shop', 'for_here_course.shop_id=for_here_shop.id')
			->where(['courses.status' => [Courses::STATUS_IMPORTED]])
			->andWhere(['not in', 'for_here_shop.original_category', ['水果生鲜', '自助餐', '面包甜点']])
			->andWhere(['>', 'avg_recommend_count', 1])
			->orderBy('for_here_shop.comment_quantity desc,avg_recommend_count desc')
			->limit(1)->all();
		if (count($courses)) {
			return $courses[0];
		}
		return null;
	}

	/**
	 * @return string
	 */
	public function getCourseProfile()
	{
		return $this->course_profile;
	}

	/**
	 * @param string $course_profile
	 */
	public function setCourseProfile($course_profile)
	{
		$this->course_profile = $course_profile;
	}

	/**
	 * @return int
	 */
	public function getCourseProfileUpdateTime()
	{
		return $this->course_profile_update_time;
	}

	/**
	 * @param int $course_profile_update_time
	 */
	public function setCourseProfileUpdateTime($course_profile_update_time)
	{
		$this->course_profile_update_time = $course_profile_update_time;
	}

	/**
	 * @return int
	 */
	public function getCheck()
	{
		return $this->check;
	}

	/**
	 * @param int $check
	 */
	public function setCheck($check)
	{
		$this->check = $check;
	}

	/**
	 * @return string
	 */
	public function getCheckReason()
	{
		return $this->check_reason;
	}

	/**
	 * @param string $check_reason
	 */
	public function setCheckReason($check_reason)
	{
		$this->check_reason = $check_reason;
	}



}
