<?php

namespace app\models;

use app\services\JsonConverter;
use yii\db\ActiveRecord;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%label}}".
 *
 * @property integer $id
 * @property string  $name
 * @property integer $parent_id
 * @property integer $create_time
 * @property integer $update_time
 * @property string  $editor
 * @property string  $type
 * @property integer $status
 * @property integer $level
 */
class Label extends ActiveRecord
{
	const STATUS_DELETE = 0;
	const STATUS_AVAILABLE = 1;

	static public $leafChildren = [];

	public static function getDb()
	{
		return Yii::$app->get('what2eat');
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return int
	 */
	public function getParentId()
	{
		return $this->parent_id;
	}

	/**
	 * @param int $parent_id
	 */
	public function setParentId($parent_id)
	{
		$this->parent_id = $parent_id;
	}

	/**
	 * @return string
	 */
	public function getCreateTime()
	{
		return $this->create_time;
	}

	/**
	 * @param string $create_time
	 */
	public function setCreateTime($create_time)
	{
		$this->create_time = $create_time;
	}

	/**
	 * @return string
	 */
	public function getUpdateTime()
	{
		return $this->update_time;
	}

	/**
	 * @param string $update_time
	 */
	public function setUpdateTime($update_time)
	{
		$this->update_time = $update_time;
	}

	/**
	 * @return string
	 */
	public function getEditor()
	{
		return $this->editor;
	}

	/**
	 * @param string $editor
	 */
	public function setEditor($editor)
	{
		$this->editor = $editor;
	}

	/**
	 * @return int
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @param int $status
	 */
	public function setStatus($status)
	{
		$this->status = $status;
	}

	/**
	 * @return int
	 */
	public function getLevel()
	{
		if ($this->parent_id) {
			return Label::findOne(['id' => $this->parent_id])->getLevel() + 1;
		} else {
			return 0;
		}
	}

	/**
	 * @param int $level
	 */
	public function setLevel($level)
	{
		$this->level = $level;
	}

	public function getTreeName()
	{
		$color = "btn btn-sm btn-rounded btn-" . Yii::$app->params['color.level'][$this->getLevel()];
		return str_repeat("&nbsp;", 4 * $this->getLevel()) .
			"<button class='$color'>" . $this->name . "</button>";
	}

	public function getLabelName()
	{
		$color = "btn btn-rounded btn-" . Yii::$app->params['color.level'][$this->getLevel()];
		return "<button class='$color'>" . $this->name . "</button>";
	}

	public static function getShopCheckBoxOptions($labelsString)
	{
		$labelType = 'shop';
		$labels = Label::find()->where(['status' => [Label::STATUS_AVAILABLE], 'type' => $labelType])->all();
		$labelsArray = json_decode(JsonConverter::array2json(ArrayHelper::toArray($labels)), true);
		$order = [];
		array_walk_recursive($labelsArray, function ($item, $key) use (&$order) {
			if ($key == 'id') {
				$order[] = $item;
			}
		});
		$order = implode(",", $order);
		$labelsResult = Label::find()->where(['status' => Label::STATUS_AVAILABLE, 'type' => $labelType])->orderBy([new Expression('FIELD(id,' . $order . ')')])->all();

		if (!$labelsResult) {
			return "";
		}
		$return = "";
		foreach ($labelsResult as $label) {
			if ($label['level'] == 0) {
				continue;
			}
			$id = $label["id"];
			$name = $label["name"];
			$selected = (in_array($id, explode("#", $labelsString))) ? "checked" : "";
			if ($label->hasChildren()) {
				$item = "<br><label for='$id'>$name</label><br>";
			} else {
				$item = "<input type='checkbox' name='shop_labels[]' class='filled-in' id='$id'  $selected value='$id'>
						<label for='$id'>$name</label>&nbsp;&nbsp;&nbsp;";
			}
			if ($name == '水果生鲜' || $name == '餐饮') {
				$item = "<hr>" . $item;
			}
			$return .= $item;
		}
		$return = str_replace("<br><br>", "<br>", $return);
		$return = str_replace("<br><br>", "<br>", $return);
		$return = str_replace("<hr><br>", "<hr>", $return);
		return $return;
	}

	public static function getCourseCheckBoxOptions($labelsString)

	{
		$labelType = 'course';
		$labels = Label::find()->where(['status' => [Label::STATUS_AVAILABLE], 'type' => $labelType])->all();
		$labelsArray = json_decode(JsonConverter::array2json(ArrayHelper::toArray($labels)), true);
		$order = [];
		array_walk_recursive($labelsArray, function ($item, $key) use (&$order) {
			if ($key == 'id') {
				$order[] = $item;
			}
		});
		$order = implode(",", $order);
		$labelsResult = Label::find()->where(['status' => Label::STATUS_AVAILABLE, 'type' => $labelType])->orderBy([new Expression('FIELD(id,' . $order . ')')])->all();

		if (!$labelsResult) {
			return "";
		}
		$return = "";
		foreach ($labelsResult as $label) {
			if ($label['level'] == 0) {
				continue;
			}

			$id = $label["id"];
			$name = $label["name"];
			$selected = (in_array($id, explode("#", $labelsString))) ? "checked" : "";
			if ($label['level'] == 1) {
				if ($label['name'] != '味道') {
					$return .= "<hr>";
				}
				if ($label['name'] != '禁忌人群' && $label['name'] != '强功效') {
					continue;
				}
			}

			if ($label->hasChildren()) {
				$item = "<br><label for='$id'>$name</label><br>";
			} else {
				$item = "<input type='checkbox' name='course_labels[]' class='filled-in' id='$id'  $selected value='$id'>
						<label for='$id'>$name</label>&nbsp;&nbsp;&nbsp;";
			}

			if ($name == '炖炒类' || $name == '火锅' || $name == '烧烤' || $name == '西餐' || $name == '韩国料理' || $name == '日本料理' || $name == '东南亚菜' || $name == '自助'
			) {
				$return .= "<br>" . $item . "<br>";
			} else {
				$return .= $item;
			}
		}
		$return = str_replace("<br><br>", "<br>", $return);
		$return = str_replace("<br><br>", "<br>", $return);
		$return = str_replace("<hr><br>", "<hr>", $return);
		return $return;
	}

	public static function getSelectOptions($labelsString, $labelType)
	{
		$labels = Label::find()->where(['status' => [Label::STATUS_AVAILABLE], 'type' => $labelType])->all();
		$labelsArray = json_decode(JsonConverter::array2json(ArrayHelper::toArray($labels)), true);
		$order = [];
		array_walk_recursive($labelsArray, function ($item, $key) use (&$order) {
			if ($key == 'id') {
				$order[] = $item;
			}
		});
		$order = implode(",", $order);
		$labelsResult = Label::find()->where(['status' => Label::STATUS_AVAILABLE, 'type' => $labelType])->orderBy([new Expression('FIELD(id,' . $order . ')')])->all();

		if (!$labelsResult) {
			return "";
		}
		$return = "";
		foreach ($labelsResult as $label) {
			$id = $label["id"];
			$name = $label["name"];
			$selected = (in_array($id, explode("#", $labelsString))) ? "selected" : "";
			$return .= "<option $selected value='$id'>$name</option>\n";
		}
		return $return;
	}

	public function getDisplayCreateTime()
	{
		return date('Y-m-d H:i:s', $this->create_time);
	}

	public function getDisplayUpdateTime()
	{
		return date('Y-m-d H:i:s', $this->update_time);
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @param string $type
	 */
	public function setType($type)
	{
		$this->type = $type;
	}

	public function hasChildren()
	{
		$label = Label::findOne(['parent_id' => $this->id, 'status' => self::STATUS_AVAILABLE]);
		if ($label === null) {
			return false;
		}
		return true;
	}

	public function getParent()
	{
		$parent = Label::findOne(['id' => $this->parent_id]);
		if ($parent instanceof Label) {
			return $parent;
		}
		return null;
	}

	static public function getLeafChildren($id)
	{
		$children = Label::find()->where(['parent_id' => $id, 'status' => self::STATUS_AVAILABLE])->all();
		foreach ($children as $child) {
			if ($child instanceof Label) {
				if ($child->hasChildren()) {
					self::$leafChildren = self::getLeafChildren($child->getId());
				} else {
					self::$leafChildren[] = $child;
				}
			}
		}
		return self::$leafChildren;
	}

}
